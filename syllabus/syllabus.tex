%! TEX program = lualatex
\documentclass[11pt]{article}

% Misc packages
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{ifpdf}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{amssymb}

% Colours
\PassOptionsToPackage{dvipsnames,svgnames,x11names,table}{xcolor}
\usepackage{xcolor}
\definecolor{dku-blue}{HTML}{003A81}
\definecolor{dku-green}{HTML}{006F3F}

% Fonts
\usepackage[no-math]{fontspec}
%\usepackage{xunicode}
\usepackage{xltxtra}
\defaultfontfeatures{Mapping=tex-text,Numbers=Lining}
% \newfontfamily\cnfont{Hiragino Sans GB}
\AtBeginDocument{%
    \setmainfont{Carlito}%
    \setsansfont[
        BoldFont={Carlito}]{Carlito}%
    \raggedbottom%
}
% \usepackage[sf]{noto}

% Multi-language support
\usepackage{polyglossia}
\setmainlanguage{english}

% Links
\usepackage{url}
\renewcommand{\UrlBreaks}{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
    \do\)\do\,\do\?\do\'\do+\do\=\do\#\do\-}
\renewcommand{\UrlFont}{\normalfont}
\usepackage{hyperref} % Required for adding links	and customizing them
\hypersetup{colorlinks, breaklinks, urlcolor=Maroon, linkcolor=Maroon,
    citecolor=Green} % Set link colors
\hypersetup{%
    pdfauthor=Xing Shi Cai,%
    pdftitle=COMPSCI 203 Discrete Mathematics for Computer Science}

% Geometry
\usepackage[a4paper,
    includehead,tmargin=2cm,nohead,
    hmargin=2cm,
    includefoot,foot=1.5cm,bmargin=2cm]{geometry}

% Tabular
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{array}
\newcommand\gvrule{\color{lightgray}\vrule width 0.5pt}

% From pandoc
\renewcommand{\tabularxcolumn}[1]{m{#1}}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
    \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
    \usepackage{selnolig}  % disable illegal ligatures
\fi

% For including pictures from current directory
\graphicspath{.}

% Misc
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{hanging}

\newcommand{\push}{\hangpara{2em}{1}}

% Section titles
\usepackage{titlesec}
\titleformat{\section}{\raggedright\large\bfseries\color{dku-blue}}{}{0em}{}[{\titlerule[\heavyrulewidth]}]
\titleformat{\subsection}{\raggedright\itshape\color{dku-blue}}{}{0em}{}

% Emoji
\usepackage{emoji}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{tabularx}{\textwidth}{@{}Xm{6cm}}
    \arrayrulecolor{dku-blue}\toprule\vspace{6pt}
    {\bfseries\color{dku-blue}
        {\Large COMPSCI 203} \newline\newline
        {\Large Discrete Mathematics for Computer Science} \newline\newline
        {\Large Fall 2023, Session 1}}
     & \includegraphics[width=6cm]{dku-logo.pdf} \\
    \bottomrule
\end{tabularx}

\vspace{1em}

\textcolor{dku-blue}{\rule{\textwidth}{1pt}}

{
    \setlength{\parskip}{0.2em}
    \textbf{Class meeting time:}
    See DKU Hub.

    \textbf{Academic credit:} 4 DKU credits.

    \textbf{Course format:}
    Lectures.

    \textbf{Asynchronous study:}
    For students who cannot attend the lectures in person or remotely, slides and video
    recordings will be provided.

    \textbf{Office hour:}
    Two one-hour online sessions per week.
    In-person meetings can be arranged upon request.

    \textbf{Last update:} \today{}
}

\vspace{-1em}\textcolor{dku-blue}{\rule{\textwidth}{1pt}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Instructor's information}

Dr.\ \href{https://newptcai.gitlab.io/}{Xing Shi Cai}
Assistant Professor of Mathematics, Duke Kunshan University

Dr.\ Cai received his PhD in computer science at McGill University in Canada.
After that he worked as postdoc researcher at Uppsala University in Sweden.
His main research interest is in applying probability theory in the analysis of
algorithms and complex networks.

Website: \url{https://newptcai.gitlab.io/teaching/}

Email: \href{mailto:xingshi.cai@dukekunshan.edu.cn}{xingshi.cai@dukekunshan.edu.cn}

\section*{What is this course about?}

Discrete mathematics lays the foundation on which much of modern
computer science rests. This course is an introduction to discrete
mathematics, with topics selected based on their relevance to computer
science. These include combinatorics, recursive equations, complex
theory, graph theory, probability and mathematical logic. We will see
how discrete mathematics can help to solve problems such as estimate how
long it takes a hacker to guess a password, and to count the number of
ways to make a subway sandwich.

Mathematical logic, the first component of the course will be helpful
for students who want to study any advanced topics in mathematics.
Those who intend to take
COMPSCI 301 (Algorithms and Databases) will see that the mathematical
techniques taught in this course are useful for the analysis of
algorithms.

\section*{What background knowledge do I need before taking this course?}

Pre/co-requisites: COMPSCI 201 (Introduction to Programming and Data
Structures) or MATH 202 (Linear Algebra)

\section*{What will I learn in this course?}

By the end of this course, you will be able to:

\begin{itemize}
\item
  Give examples of basic objects and notions in discrete mathematics;
\item
  Present mathematical arguments in written and spoken form;
\item
  Apply techniques in discrete mathematics to solve concrete problems.
\end{itemize}

These notions and techniques include

\begin{itemize}
\item
  Mathematical statements and proofs
\item
  Sets and functions
\item
  Propositional and predicate logic
\item
  Strings, permutations, combinations
\item
  Combinatorial proofs
\item
  Binomial coefficients, Catalan numbers
\item
  Mathematical Induction
\item
  Big O notation
\item
  Algorithm complexity
\item
  The inclusion-exclusion formula
\item
  Recursion
\item
  Linear recurrence equations
\item
  Graphs
\item
  Eulerian graphs and Hamiltonian circles
\item
  Planar graph
\item
  Graph colouring
\item
  Cayley's formula
\item
  Ramsey number
\item
    Sequences (definition, convergent sequences, find the limit of a convergent sequence, properties of convergent sequences)
\item
    Series (partial sums, convergent series, geometric series, harmonic series, test for divergence)
\item
    Testing series (integral test, comparison tests, alternating series test, absolute convergence, ratio and root tests)
\item
    Power series
\item
    Taylor series
\end{itemize}

\section*{What will I do in this course?}

\subsection*{Lectures}

You will attend four lectures each week.

\subsection*{Assignment}

There are no assignment.
But at the end of each lecture,
there will be some recommended exercises.
Most of their solutions will be provided.
Problems in quizzes and the final exam will be similar to these exercises.

\subsection*{Quiz}

At the end of each week, there are going to be one 30 minutes in-person quiz.
There are going to be three questions in each quiz.

\subsection*{Presentation}
You will select a problem in discrete mathematics and present it to the
instructor in 15 minutes.
(This will be a group project if there are more than 20 students in class.)
Students who took the course before and have done the presentation will be
exempted.

\subsection*{Exam}

There are going to be one final exam in week 8.
The exam will last two hours.

\subsection*{In-class and Online discussions}

You will be given opportunity to answer questions either in-class or online to
win points for the 2\% Discussion Points.

\subsection*{Experiential activity}

We will invite a guest speaker to talk about doing
research in discrete mathematics and studying abroad for graduate
school. The talk will be held through zoom.


\section*{How can I prepare for the class sessions to be successful?}

To succeed, you should be prepared to devote several hours to this
course on a daily basis. You are strongly encouraged to use the tutoring
resources of ARC, to work with classmates, to seek information online,
and to contact instructors in a timely manner for additional help as
needed.

\section*{What required texts, materials, and equipment will I need?}

We will use free textbooks which you can download
from the links below:

\begin{itemize}
\item
  \href{http://discrete.openmathbooks.org/dmoi3.html}{Discrete
  Mathematics: An Open Introduction, 3rd Edition} by Oscar Levin.
\item
  \href{https://www.appliedcombinatorics.org/appcomb/}{Applied
  Combinatorics} by Mitchel T. Keller and William T. Trotter. (See also
  the
  \href{https://www.appliedcombinatorics.org/appcomb/errata/}{errata}.)
\item
    \href{http://www.mecmath.net/calculus/index.html}{Elementary Calculus}, by Michael Corral (Schoolcraft College).
\end{itemize}

\section*{How will my grade be determined?}

Course grades will be assigned according to a standard 10-pt scale:


\begin{longtable}[h]{@{}llll@{}}
\toprule
Grades & Percentage & C+ & [77\%, 80\%) \\
\midrule
\endhead
A+ & [98\%, 100\%] & C & [73\%, 77\%) \\
A & [93\%, 98\%) & C- & [70\%, 73\%) \\
A- & [90\%, 93\%) & D+ & [67\%, 70\%) \\
B+ & [87\%, 90\%) & D & [63\%, 67\%) \\
B & [83\%, 87\%) & D- & [60\%, 63\%) \\
B- & [80\%, 83\%) & F & [0\%, 60\%) \\
\bottomrule
\end{longtable}

\emoji{bomb} Points are rounded downwards, e.g., 92.99 will become to 92 (A-).
Petitions for increasing grades will \emph{not} get replies.
Regrade requests will be \emph{rejected} unless the grader misread an answer.

The course grade will be based on:

\begin{itemize}
    \item Quizzes: 60\%
    \item Final exam: 27\%
    \item Presentation: 10\%
    \item Discussion: 3\%
\end{itemize}

You are afforded the chance to retake any quiz during the office hours at the
start of the subsequent week. Bear in mind that the retake will contain
different questions, though of similar difficulty level. Your score for each
quiz will be recorded as the higher of your original attempt and the retake. 

To lighten your load, we will count only your top six quiz scores towards your
final grade. This means you could disregard one low score or even miss a quiz
without impacting your overall grade negatively.

\section*{What are the course policies?}

\subsection*{Q \& A}

Questions \emph{must} be posted on
\href{https://go.canvas.duke.edu/}{Ed}.
Emails will most likely \emph{not} get replied.

\subsection*{Remote learning}

If you are not able to come to classes or exams in-person, you will need
to send the instructor
\begin{itemize}
    \item the permission from the Dean of Undergraduate Studies,
    \item or proof of other special circumstances, such as illness.
\end{itemize}
Failing to do so will result in falling the class.

\subsection*{Classroom management}

Using laptops are \emph{not} allowed during lectures.
If you want to take notes,
use a paper notebook or a tablet computer.

Sit next to another student. We will have group activities in class.

Turn on your camera if you attend remotely.
Otherwise you will get kicked out of the meeting.

\subsection*{Exams and Quizzes}

All quizzes and the exams will be closed book.
But you are allowed to bring one A4-sized double-sided cheat sheet.
Books, collaborations, calculators, the Internet,
and other aids are \emph{not} allowed during exams.
Cheating for the first time will result in getting zero points.
Repeated infractions will cause failing the course.

The final exam must be taken during the assigned time.
Make-up final exam (if approved) will take place at a certain time in the following
semester/session arranged by Dean's office and the instructor.

\subsection*{Academic Integrity}

As a student, you should abide by the academic honesty standard of the
Duke Kunshan University. Its Community Standard states: ``Duke Kunshan
University is a community comprised of individuals from diverse cultures
and backgrounds. We are dedicated to scholarship, leadership, and
service and to the principles of honesty, fairness, respect, and
accountability. Members of this community commit to reflecting upon and
upholding these principles in all academic and non-academic endeavors,
and to protecting and promoting a culture of integrity and trust.'' For
all graded work, students should pledge that they have neither given nor
received any unacknowledged aid.

\subsection*{Academic Policy \& Procedures}
You are responsible for knowing and adhering to academic policy and
procedures as published in University Bulletin and Student Handbook.
Please note, an incident of behavioral infraction or academic dishonesty
(cheating on a test, plagiarizing, etc.) will result in immediate action
from me, in consultation with university administration (e.g., Dean of
Undergraduate Studies, Student Conduct, Academic Advising). Please visit
the Undergraduate Studies website for additional guidance related to
academic policy and procedures. Academic integrity is everyone's
responsibility.

Please refer to \href{https://undergrad.dukekunshan.edu.cn/en/undergraduate-bulletin}{Undergraduate Bulletin} and visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office of Undergraduate Advising website} for DKU course
policies and guidelines.


\subsection*{Academic Disruptive Behavior and Community Standard}
Please avoid all forms of disruptive behavior, including but not limited
to: verbal or physical threats, repeated obscenities, unreasonable
interference with class discussion, making/receiving personal phone
calls, text messages or pages during class, excessive tardiness, leaving
and entering class frequently without notice of illness or other
extenuating circumstances, and persisting in disruptive personal
conversations with other class members. Please turn off phones, pagers,
etc.\ during class unless instructed otherwise. Laptop computers may be
used for class activities allowed by the instructor during synchronous
sessions. If you choose not to adhere to these standards, I will take
action in consultation with university administration (e.g., Dean of
Undergraduate Studies, Student Conduct, Academic Advising).

\subsection*{Academic Accommodations}
If you need to request accommodation for a disability, you need a signed
accommodation plan from Campus Health Services, and you need to provide
a copy of that plan to me. Visit the Office of Student Affairs website
for additional information and instruction related to accommodations.

\section*{What campus resources can help me during this course?}

\subsection*{Academic Advising and Student Support}
Please consult with me about appropriate course preparation and
readiness strategies, as needed. Consult your academic advisors on
course performance (i.e., poor grades) and academic decisions (e.g.,
course changes, incompletes, withdrawals) to ensure you stay on track
with degree and graduation requirements. In addition to advisors, staff
in the Academic Resource Center can provide recommendations on academic
success strategies (e.g., tutoring, coaching, student learning
preferences). All ARC services will continue to be provided online.
Please visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office
of Undergraduate Advising website} for additional information related
to academic advising and student support services.

\subsection*{Writing and Language Studio}
For additional help with academic writing---and more generally with
language learning---you are welcome to make an appointment with the
Writing and Language Studio (WLS). To accommodate students who are
learning remotely as well as those who are on campus, writing and
language coaching appointments are available in person and online. You
can register for an account, make an appointment, and learn more about
WLS services, policies, and events on the
\href{https://dukekunshan.edu.cn/en/academics/language-and-culture-center/writing-and-language-studio}{WLS
website}. You can also find writing and language learning resources on
the \href{https://sakai.duke.edu/x/mQ6xqG}{Writing \&
Language Studio Sakai site}.

\subsection*{Online resources}
The authors of the textbook Applied Combinatorics have made some lecture
videos and slides available online
\href{https://sites.gatech.edu/math3012openresources/}{here}.

You may find the following websites helpful not only for this course:

\begin{itemize}
\item
  \href{https://math.stackexchange.com/}{Math Stack
  Exchange} --- Ask questions about mathematics and get answers from
  other users.
\item
  \href{https://mathworld.wolfram.com/}{Wolfram Mathworld}
  --- A good place to look up mathematical definitions.
\item
  \href{https://oeis.org/}{OEIS} --- Search for integer
  sequences by providing initial terms.
\end{itemize}

\subsection*{What is the expected course schedule?}

\emoji{bomb} Exact topics covered in each week may subject to change.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.20}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.80}}@{}}
\toprule
\endhead
Week 1 & \textbf{Module 1: Logic and Proof}

1. Statements DM (Discrete Mathematics) 0.1-0.2

2. Sets and Functions --- DM 0.3-0.4

3. Propositional Logic --- DM 3.1

4. Basic Proof Techniques --- DM 3.2 and some selected extra proofs.
\\
\midrule
Week 2 & \textbf{Module 2: Combinatorics}

5. Strings, Permutations, Combinations --- AC
(Applied Combinatorics) 2.1-2.3

6. Combinatorial Proofs, Binomial Coefficients --- AC
2.4-2.5

7. Binomial Coefficients, Binomial Theorem, Multinomial Coefficients --- AC 2.5-2.7

8. Sequences, Recursions, Induction --- AC 3.3, 3.5, 3.8
\\
\midrule
Week 3 & 9. Inclusion-exclusion (1) --- AC 7.1-7.3

10. Inclusion-exclusion (2) --- AC 7.4-7.6

11. Generating functions (1) --- AC 8.1-8.2

12. Generating functions (2) --- AC 8.2-8.4
\\
\midrule
Week 4 & 

\textbf{Module 3: Graph Theory}

13. Basic notions of graphs --- AC 5.1-5.2

14. Counting trees --- AC 5.6

15. Trees --- DM 4.2

16. Eulerian graphs and Hamiltonian circles --- AC 5.3
\\
\midrule
Week 5 & 
17. Graph colouring (1) --- AC 5.4

18. Graph colouring (2) --- AC 5.4

19. Graph colouring (3) --- AC 5.4

20. Planar graphs --- AC 5.5
\\
\midrule
Week 6 & 

21. Probability --- AC 10

22. Apply Probability to Combinatorics -- AC 11

23. Minimal spanning trees --- AC 12.1

24. Digraphs and Shortest Paths -- AC 12.2
\\
\midrule
Week 7 & \textbf{Module 4: Sequences and Series}

25-28. Elementary Calculus Chapter 9
\\
\bottomrule
\end{longtable}

\end{document}
