# COMPSCI203 -- Discrete Mathematics for Computer Science (2023-08)

This repository contains materials which [I (Xing Shi Cai)](https://newptcai.gitlab.io)
prepared for the course 
COMPSCI203 (Discrete Mathematics for Computer Science)
of [Duke Kunshan University](https://dukekunshan.edu.cn/).

You are free to use any material here.
