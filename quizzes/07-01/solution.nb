(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     18242,        553]
NotebookOptionsPosition[     13795,        464]
NotebookOutlinePosition[     14187,        480]
CellTagsIndexPosition[     14144,        477]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Problem 1", "Section",
 CellChangeTimes->{{3.9060745777663527`*^9, 
  3.906074578742193*^9}},ExpressionUUID->"f64089f9-ea5c-45e2-a0a3-\
1cfb961e1db3"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"4", "+", 
  RowBox[{"2", "*", 
   RowBox[{"Sum", "[", 
    RowBox[{
     RowBox[{"4", "*", 
      SuperscriptBox[
       RowBox[{"(", 
        FractionBox["65", "100"], ")"}], "n"]}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.906074580696587*^9, 3.906074607719097*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"98df5cc6-f1ce-4706-8c40-c174c3c44a1e"],

Cell[BoxData[
 FractionBox["132", "7"]], "Output",
 CellChangeTimes->{{3.906074592750369*^9, 3.906074608263625*^9}},
 CellLabel->"Out[4]=",ExpressionUUID->"3480279e-2b5e-4d5e-9b03-565d7b425b20"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Problem 2", "Section",
 CellChangeTimes->{{3.9060761270389023`*^9, 
  3.9060761377748747`*^9}},ExpressionUUID->"b7d1fba5-78f8-4b1c-8e00-\
226f8429e51f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ex", "=", 
  FractionBox["1", 
   RowBox[{
    RowBox[{"4", 
     SuperscriptBox["n", "2"]}], "-", "1"}]]}]], "Input",
 CellChangeTimes->{{3.906076235563377*^9, 3.9060762406547213`*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"bcef4bce-d28f-43c8-b16e-71cf5767c37c"],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{
   RowBox[{"-", "1"}], "+", 
   RowBox[{"4", " ", 
    SuperscriptBox["n", "2"]}]}]]], "Output",
 CellChangeTimes->{3.906076243774129*^9},
 CellLabel->"Out[8]=",ExpressionUUID->"af4dd469-aadb-4733-bccd-17f9374cafe6"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sum", "[", 
  RowBox[{"ex", ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.9060761391691113`*^9, 3.906076172958795*^9}, {
  3.906076247536911*^9, 3.906076247727004*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"e49bc812-f907-478d-9a0c-b73660133b25"],

Cell[BoxData[
 FractionBox["1", "2"]], "Output",
 CellChangeTimes->{{3.906076151049041*^9, 3.906076173511098*^9}, 
   3.9060762481085377`*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"c270584b-32da-4a7e-b937-93a9f578468c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ex1", "=", 
  RowBox[{"Apart", "[", "ex", "]"}]}]], "Input",
 CellChangeTimes->{{3.906076250400302*^9, 3.906076256078519*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"70ed11ad-a890-42f7-bc6b-3854ad3c37fe"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   RowBox[{"2", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      RowBox[{"2", " ", "n"}]}], ")"}]}]], "-", 
  FractionBox["1", 
   RowBox[{"2", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"2", " ", "n"}]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.906076256450479*^9},
 CellLabel->"Out[10]=",ExpressionUUID->"98be1b04-5619-4821-82cf-e71ae8ffc5ef"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Problem 4", "Section",
 CellChangeTimes->{{3.9060781370401087`*^9, 
  3.90607813834638*^9}},ExpressionUUID->"123fff5b-5ff6-409c-9d39-\
e4d5e6d568ba"],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"n_", ",", "x_"}], "]"}], ":=", 
  FractionBox[
   RowBox[{"n", " ", 
    SuperscriptBox["x", "n"]}], 
   RowBox[{"2", 
    RowBox[{"(", 
     RowBox[{"n", "+", "1"}], ")"}]}]]}]], "Input",
 CellChangeTimes->{{3.906078208226297*^9, 3.906078213303941*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"18547bce-32b3-4759-a497-1ed5e6203313"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sum", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"n", ",", "x"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.906078140939651*^9, 3.906078166307*^9}, {
  3.9060782186746902`*^9, 3.906078219936552*^9}},
 CellLabel->"In[15]:=",ExpressionUUID->"6dea5e18-3276-47f5-8549-a56b3710edde"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"-", "x"}], "-", 
   RowBox[{"Log", "[", 
    RowBox[{"1", "-", "x"}], "]"}], "+", 
   RowBox[{"x", " ", 
    RowBox[{"Log", "[", 
     RowBox[{"1", "-", "x"}], "]"}]}]}], 
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "x"}], ")"}], " ", "x"}]]], "Output",
 CellChangeTimes->{{3.9060781600841017`*^9, 3.9060781672087097`*^9}, 
   3.906078220239201*^9},
 CellLabel->"Out[15]=",ExpressionUUID->"32c8add1-f4c0-48f2-b2f6-82684036cb73"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{"Abs", "[", 
    FractionBox[
     RowBox[{"f", "[", 
      RowBox[{
       RowBox[{"n", "+", "1"}], ",", "x"}], "]"}], 
     RowBox[{"f", "[", 
      RowBox[{"n", ",", "x"}], "]"}]], "]"}], ",", 
   RowBox[{"n", "->", "\[Infinity]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.906078222385852*^9, 3.906078241009076*^9}},
 CellLabel->"In[17]:=",ExpressionUUID->"8996dd63-1f6d-446b-be8a-5c25c9c2baed"],

Cell[BoxData[
 RowBox[{"Abs", "[", "x", "]"}]], "Output",
 CellChangeTimes->{{3.906078228823616*^9, 3.9060782413880873`*^9}},
 CellLabel->"Out[17]=",ExpressionUUID->"d1a83dbe-62c6-4312-a307-f25b54b70986"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sum", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"n", ",", "1"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{3.906078252912765*^9},
 CellLabel->"In[18]:=",ExpressionUUID->"9452b3f2-587f-48b0-a911-544719c64533"],

Cell[BoxData[
 TemplateBox[{
  "Sum", "div", "\"Sum does not converge.\"", 2, 18, 2, 20672695862952267710, 
   "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{3.906078253325058*^9},
 CellLabel->
  "During evaluation of \
In[18]:=",ExpressionUUID->"0825fd86-23c9-44de-ab80-8af4a206c8ac"],

Cell[BoxData[
 RowBox[{
  UnderoverscriptBox["\[Sum]", 
   RowBox[{"n", "=", "1"}], "\[Infinity]"], 
  FractionBox["n", 
   RowBox[{"2", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "n"}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.906078253343912*^9},
 CellLabel->"Out[18]=",ExpressionUUID->"8c8aae0c-3f32-432f-8403-bd099c2ce6d9"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sum", "[", 
  RowBox[{
   RowBox[{"f", "[", 
    RowBox[{"n", ",", 
     RowBox[{"-", "1"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "1", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.906078260609208*^9, 3.906078262224619*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"1bc06fea-9a93-4efb-9902-41ff0ff9edc5"],

Cell[BoxData[
 TemplateBox[{
  "Sum", "div", "\"Sum does not converge.\"", 2, 19, 3, 20672695862952267710, 
   "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{3.9060782625758543`*^9},
 CellLabel->
  "During evaluation of \
In[19]:=",ExpressionUUID->"30041cf6-64bf-4309-a7db-2f2c5db116f1"],

Cell[BoxData[
 RowBox[{
  UnderoverscriptBox["\[Sum]", 
   RowBox[{"n", "=", "1"}], "\[Infinity]"], 
  FractionBox[
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"-", "1"}], ")"}], "n"], " ", "n"}], 
   RowBox[{"2", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", "n"}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.906078262592895*^9},
 CellLabel->"Out[19]=",ExpressionUUID->"687852de-0c4d-4e56-980a-e241c4181e1e"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Problem 5", "Section",
 CellChangeTimes->{{3.906152623796953*^9, 
  3.906152624804928*^9}},ExpressionUUID->"203c19b7-64b2-453e-8921-\
b6445d8b52e1"],

Cell[BoxData[
 RowBox[{"Clear", "[", "ex", "]"}]], "Input",
 CellChangeTimes->{{3.906152661654254*^9, 3.9061526630771723`*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"f164cf45-6516-4de9-8182-644f1c3a84eb"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "1", "]"}], "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{"1", "+", 
    RowBox[{"x", "^", "2"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.906152657350079*^9, 3.906152660169894*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"43322e2b-9d52-49bc-b164-d68aa4bcec8c"],

Cell[BoxData[
 SqrtBox[
  RowBox[{"1", "+", 
   SuperscriptBox["x", "2"]}]]], "Output",
 CellChangeTimes->{3.906152664481618*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"c62a6b6e-b395-4225-a13c-8d591cd06713"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Series", "[", 
  RowBox[{
   RowBox[{"ex", "[", "1", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "5"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.906152626887476*^9, 3.9061526685971413`*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"f2e8e8ae-affe-47c9-b737-fc99665c183c"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"1", "+", 
   FractionBox[
    SuperscriptBox["x", "2"], "2"], "-", 
   FractionBox[
    SuperscriptBox["x", "4"], "8"], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "x", "]"}], "6"],
    SeriesData[$CellContext`x, 0, {}, 0, 6, 1],
    Editable->False]}],
  SeriesData[$CellContext`x, 0, {1, 0, 
    Rational[1, 2], 0, 
    Rational[-1, 8]}, 0, 6, 1],
  Editable->False]], "Output",
 CellChangeTimes->{{3.906152638243519*^9, 3.90615266916448*^9}},
 CellLabel->"Out[5]=",ExpressionUUID->"f67cdef0-0f7b-4405-8260-6db8947ccfcd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "2", "]"}], "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{"1", "+", "y"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.906152676678617*^9, 3.906152686085609*^9}, {
  3.906152744215266*^9, 3.906152745030251*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"47bdf6ea-e086-4a8d-a784-991e73f6b3fe"],

Cell[BoxData[
 SqrtBox[
  RowBox[{"1", "+", "y"}]]], "Output",
 CellChangeTimes->{3.9061526868084717`*^9, 3.90615274590304*^9},
 CellLabel->"Out[11]=",ExpressionUUID->"79f15357-b072-476b-b2fe-e227dd838451"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "3", "]"}], "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"ex", "[", "2", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"y", ",", "n"}], "}"}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"n", ",", "0", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.906152687702632*^9, 3.906152718917902*^9}, {
  3.906152748935305*^9, 3.90615275501443*^9}},
 CellLabel->"In[13]:=",ExpressionUUID->"777f33b7-8100-40fa-8cd5-6e9495c091f1"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   SqrtBox[
    RowBox[{"1", "+", "y"}]], ",", 
   FractionBox["1", 
    RowBox[{"2", " ", 
     SqrtBox[
      RowBox[{"1", "+", "y"}]]}]], ",", 
   RowBox[{"-", 
    FractionBox["1", 
     RowBox[{"4", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", "+", "y"}], ")"}], 
       RowBox[{"3", "/", "2"}]]}]]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.9061527141229353`*^9, 3.906152719309819*^9}, {
  3.906152750445774*^9, 3.906152755427396*^9}},
 CellLabel->"Out[13]=",ExpressionUUID->"81cfcf72-6a58-423a-a8c0-ce53387a6ba6"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "3", "]"}], "/.", 
  RowBox[{"y", "->", "0"}]}]], "Input",
 CellChangeTimes->{{3.906152755895091*^9, 3.906152759365881*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"2d502c7b-be2f-4422-aebf-edb280e2f536"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", 
   FractionBox["1", "2"], ",", 
   RowBox[{"-", 
    FractionBox["1", "4"]}]}], "}"}]], "Output",
 CellChangeTimes->{3.906152759680949*^9},
 CellLabel->"Out[14]=",ExpressionUUID->"0dcba009-5d33-4240-8e74-01daf83a8ccc"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "4", "]"}], "=", 
  RowBox[{"Series", "[", 
   RowBox[{
    RowBox[{"ex", "[", "2", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"y", ",", "0", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.906152765991062*^9, 3.906152787749997*^9}},
 CellLabel->"In[17]:=",ExpressionUUID->"099a6fdb-1019-499d-8b53-ebeb8f436ef1"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"1", "+", 
   FractionBox["y", "2"], "-", 
   FractionBox[
    SuperscriptBox["y", "2"], "8"], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "y", "]"}], "3"],
    SeriesData[$CellContext`y, 0, {}, 0, 3, 1],
    Editable->False]}],
  SeriesData[$CellContext`y, 0, {1, 
    Rational[1, 2], 
    Rational[-1, 8]}, 0, 3, 1],
  Editable->False]], "Output",
 CellChangeTimes->{{3.906152779761022*^9, 3.906152788163001*^9}},
 CellLabel->"Out[17]=",ExpressionUUID->"9bc75d00-02b8-436c-9c74-433708e173da"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ex", "[", "4", "]"}], "//", "Normal"}], "//", 
  RowBox[{
   RowBox[{"#", "/.", 
    RowBox[{"y", "->", 
     RowBox[{"x", "^", "2"}]}]}], "&"}]}]], "Input",
 CellChangeTimes->{{3.90615278939919*^9, 3.9061528236961403`*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"a2e1e01b-6b05-40f4-ad86-5c22d9259158"],

Cell[BoxData[
 RowBox[{"1", "+", 
  FractionBox[
   SuperscriptBox["x", "2"], "2"], "-", 
  FractionBox[
   SuperscriptBox["x", "4"], "8"]}]], "Output",
 CellChangeTimes->{{3.906152796560584*^9, 3.906152824142537*^9}},
 CellLabel->"Out[19]=",ExpressionUUID->"0b02425e-10d6-41aa-ba95-9b43b68cfa4f"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{717, 807},
WindowMargins->{{Automatic, 1.5}, {Automatic, 1.5}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"7dd48b7d-6bf0-4cbe-8cb2-d445721055e7"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 156, 3, 68, "Section",ExpressionUUID->"f64089f9-ea5c-45e2-a0a3-1cfb961e1db3"],
Cell[CellGroupData[{
Cell[761, 29, 462, 13, 46, "Input",ExpressionUUID->"98df5cc6-f1ce-4706-8c40-c174c3c44a1e"],
Cell[1226, 44, 194, 3, 48, "Output",ExpressionUUID->"3480279e-2b5e-4d5e-9b03-565d7b425b20"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[1469, 53, 158, 3, 68, "Section",ExpressionUUID->"b7d1fba5-78f8-4b1c-8e00-226f8429e51f"],
Cell[CellGroupData[{
Cell[1652, 60, 288, 7, 51, "Input",ExpressionUUID->"bcef4bce-d28f-43c8-b16e-71cf5767c37c"],
Cell[1943, 69, 260, 7, 54, "Output",ExpressionUUID->"af4dd469-aadb-4733-bccd-17f9374cafe6"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2240, 81, 344, 7, 29, "Input",ExpressionUUID->"e49bc812-f907-478d-9a0c-b73660133b25"],
Cell[2587, 90, 220, 4, 47, "Output",ExpressionUUID->"c270584b-32da-4a7e-b937-93a9f578468c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2844, 99, 229, 4, 29, "Input",ExpressionUUID->"70ed11ad-a890-42f7-bc6b-3854ad3c37fe"],
Cell[3076, 105, 439, 14, 55, "Output",ExpressionUUID->"98be1b04-5619-4821-82cf-e71ae8ffc5ef"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3564, 125, 155, 3, 68, "Section",ExpressionUUID->"123fff5b-5ff6-409c-9d39-e4d5e6d568ba"],
Cell[3722, 130, 390, 11, 56, "Input",ExpressionUUID->"18547bce-32b3-4759-a497-1ed5e6203313"],
Cell[CellGroupData[{
Cell[4137, 145, 395, 9, 29, "Input",ExpressionUUID->"6dea5e18-3276-47f5-8549-a56b3710edde"],
Cell[4535, 156, 516, 15, 60, "Output",ExpressionUUID->"32c8add1-f4c0-48f2-b2f6-82684036cb73"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5088, 176, 459, 12, 49, "Input",ExpressionUUID->"8996dd63-1f6d-446b-be8a-5c25c9c2baed"],
Cell[5550, 190, 204, 3, 33, "Output",ExpressionUUID->"d1a83dbe-62c6-4312-a307-f25b54b70986"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5791, 198, 323, 8, 29, "Input",ExpressionUUID->"9452b3f2-587f-48b0-a911-544719c64533"],
Cell[6117, 208, 308, 8, 28, "Message",ExpressionUUID->"0825fd86-23c9-44de-ab80-8af4a206c8ac"],
Cell[6428, 218, 334, 9, 56, "Output",ExpressionUUID->"8c8aae0c-3f32-432f-8403-bd099c2ce6d9"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6799, 232, 368, 9, 29, "Input",ExpressionUUID->"1bc06fea-9a93-4efb-9902-41ff0ff9edc5"],
Cell[7170, 243, 310, 8, 28, "Message",ExpressionUUID->"30041cf6-64bf-4309-a7db-2f2c5db116f1"],
Cell[7483, 253, 432, 13, 60, "Output",ExpressionUUID->"687852de-0c4d-4e56-980a-e241c4181e1e"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7964, 272, 154, 3, 68, "Section",ExpressionUUID->"203c19b7-64b2-453e-8921-b6445d8b52e1"],
Cell[8121, 277, 205, 3, 29, "Input",ExpressionUUID->"f164cf45-6516-4de9-8182-644f1c3a84eb"],
Cell[CellGroupData[{
Cell[8351, 284, 302, 7, 29, "Input",ExpressionUUID->"43322e2b-9d52-49bc-b164-d68aa4bcec8c"],
Cell[8656, 293, 207, 5, 35, "Output",ExpressionUUID->"c62a6b6e-b395-4225-a13c-8d591cd06713"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8900, 303, 317, 7, 29, "Input",ExpressionUUID->"f2e8e8ae-affe-47c9-b737-fc99665c183c"],
Cell[9220, 312, 595, 17, 51, "Output",ExpressionUUID->"f67cdef0-0f7b-4405-8260-6db8947ccfcd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9852, 334, 327, 7, 29, "Input",ExpressionUUID->"47bdf6ea-e086-4a8d-a784-991e73f6b3fe"],
Cell[10182, 343, 206, 4, 33, "Output",ExpressionUUID->"79f15357-b072-476b-b2fe-e227dd838451"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10425, 352, 532, 14, 29, "Input",ExpressionUUID->"777f33b7-8100-40fa-8cd5-6e9495c091f1"],
Cell[10960, 368, 587, 18, 54, "Output",ExpressionUUID->"81cfcf72-6a58-423a-a8c0-ce53387a6ba6"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11584, 391, 248, 5, 29, "Input",ExpressionUUID->"2d502c7b-be2f-4422-aebf-edb280e2f536"],
Cell[11835, 398, 268, 7, 47, "Output",ExpressionUUID->"0dcba009-5d33-4240-8e74-01daf83a8ccc"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12140, 410, 372, 9, 29, "Input",ExpressionUUID->"099a6fdb-1019-499d-8b53-ebeb8f436ef1"],
Cell[12515, 421, 565, 16, 51, "Output",ExpressionUUID->"9bc75d00-02b8-436c-9c74-433708e173da"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13117, 442, 350, 9, 29, "Input",ExpressionUUID->"a2e1e01b-6b05-40f4-ad86-5c22d9259158"],
Cell[13470, 453, 297, 7, 51, "Output",ExpressionUUID->"0b02425e-10d6-41aa-ba95-9b43b68cfa4f"]
}, Open  ]]
}, Open  ]]
}
]
*)

