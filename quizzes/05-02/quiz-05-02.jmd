---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 05 (02)
author: "Instructor Xing Shi Cai"
date: 2023-09-28
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} \Large{} $\text{\emoji{bomb}}$ Exam Guidelines}]
\large{}
\begin{itemize}
    \item[\emoji{ab}] To write a proper proof, it is necessary to provide both equations and explanatory statements.
    \item[\emoji{robot}] When writing your name and short mathematical answers,
    use clear and distinct letters to facilitate AI recognition.
    \item[\emoji{pencil}] Avoid using pencil as its visibility diminishes upon scanning.
\end{itemize}
\end{tcolorbox}

\pagebreak{}

# Prüfer Code (1pt)

Convert the Prüfer code 2134 to a tree.

```julia

answer = L"""
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{tree.jpg}
    \caption{The tree with Prüfer code 1153}
\end{figure}
"""
long_answer(answer, vfill=true)

```

# Hamiltonian graphs (1pt)

## A hamiltonian graph (0.5pt)

We have discussed Proposition 5.18 (AC) in class, which states that a graph with
$n \ge 3$ vertices and a minimum degree $d \ge \lceil n/2 \rceil$ is
hamiltonian. Your task is to demonstrate that the converse of this statement is
not universally true by finding a graph with a degree sequence $(3, 3, 3, 3,
2, 2)$ that is hamiltonian, thereby showing that a graph can be hamiltonian
without meeting the $d \ge \lceil n/2 \rceil$ condition.

```julia

answer = L"""
One possibility is shown in Figure \ref{fig:hamiltonian:1}.
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{hamiltonian-1.jpg}
    \caption{A hamiltonian graph}
    \label{fig:hamiltonian:1}
\end{figure}
"""

long_answer(answer, vfill=true)
```
`j print_in_quiz(L"\pagebreak{}")`


## A non-hamiltonian graph (0.5pt)

A cut-edge is an edge whose removal disconnects the graph into two separate
components. For instance, the graph shown in Figure \ref{fig:hamiltonian:2} has
two cut-edges, $\{1,6\}$ and $\{2,3\}$.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth]{hamiltonian-2.jpg}
    \caption{A graph with a cut-edge}
    \label{fig:hamiltonian:2}
\end{figure}

Prove by contradiction that a graph with a cut-edge cannot be hamiltonian.

:bulb: You should start your proof by assuming that there is a hamiltonian cycle
$C$ in the graph, and consider the following cases:

1. the cut-edge is in $C$,
2. the cut-edge is not in $C$.

You should argue that in both cases, there is a contradiction.

```julia

answer = L"""
Assume that a graph $G$ has a hamiltonian cycle $C$ and has a cut-edge $e=uv$.

Case 1: $e$ is not in $C$: If $e$ is removed, the graph splits into two
disconnected components $G_1$ and $G_2$. This contradicts the existence of a
Hamiltonian cycle that should traverse all vertices in a connected graph $G$.

Case 2: $e$ is in the Hamiltonian cycle: If $e = (u, v)$ is part of the
Hamiltonian cycle and is removed, $u$ and $v$ should still be connected through
the remaining edges of the cycle. However, the removal of $e$ disconnects $u$
and $v$, contradicting the existence of a Hamiltonian cycle that should keep all
vertices connected.

In both cases, we arrive at a contradiction, proving that a graph with a
cut-edge cannot be Hamiltonian.
"""

long_answer(answer, vfill=true)
```

`j print_in_quiz(L"\pagebreak{}")`

# Clique number (1pt)

Consider the following graph.

\definecolor{juliaorange}{HTML}{FFA500}
\definecolor{jorange}{rgb}{1,0.647,0}
\definecolor{trolleygrey}{rgb}{0.5, 0.5, 0.5}
\definecolor{BrightRed}{HTML}{dd2e44}
\definecolor{MintGreen}{HTML}{78b159}
\definecolor{myBlue}{HTML}{55acee}
\definecolor{PastelYellow}{HTML}{eae559}
\definecolor{SeaGreen}{HTML}{2ca687}
\definecolor{MintWhisper}{rgb}{0.8,1,0.8}
\definecolor{GoldenWhisper}{HTML}{FDCB58}
\definecolor{LavenderDream}{HTML}{AA8ED6}

\newcommand{\n}{3}
\pgfmathtruncatemacro{\nminusone}{\n - 1}

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
    ultra thick,
    label distance=-5pt,
    every node/.style={circle, draw=black, fill=orange, ultra thick, minimum height=0.8cm},
    scale=2
    ]
    % xi vertices on a cycle
    \foreach \i in {0,1,...,\nminusone} {
        \pgfmathtruncatemacro\label{\i+1}
        \pgfmathsetmacro{\angle}{\i*360/\n}
        \node[label={\angle: $ x_{\label}$}] (x\i) at (\angle:1) {};
    }
    % yi vertices on another cycle
    \foreach \i in {0,1,...,\nminusone} {
        \pgfmathtruncatemacro\label{\i+1}
        \pgfmathsetmacro{\angle}{\i*360/\n}
        \node[label={\angle: $ y_{\label}$}] (y\i) at (\angle:2) {};
    }
    % Central vertex z
    \node[label=above: $ z $] (z) at (0,0) {};

    % Connect xi vertices to form a cycle
    \foreach \i in {0,1,...,\nminusone} {
        \pgfmathtruncatemacro{\j}{mod(\i+1, \n)}
        \draw[trolleygrey] (x\i) -- (x\j);
    }

    % Connect xi to yi and yi to z
    \foreach \i in {0,1,...,\nminusone} {
        \pgfmathtruncatemacro{\j}{mod(\i+1, \n)}
        \draw[trolleygrey] (x\i) -- (y\j);
        \pgfmathtruncatemacro{\j}{mod(\i-1+\n, \n)}
        \draw[trolleygrey] (x\i) -- (y\j);
        \pgfmathsetmacro{\angleout}{100+\i*360/\n}
        \pgfmathsetmacro{\angleint}{80+\i*360/\n}
        \draw[trolleygrey] (y\i) to[out=\angleout, in=\angleint] (z);
    }
    \end{tikzpicture}
    \caption{A graph $ G $}
    \label{fig:mycielski}
\end{figure}

## Clique number (0.5pt)

What is the clique number of $G$?

`j short_answer(L"\omega(G)", "3")`

`j print_in_quiz(L"\pagebreak{}")`

## Proof (0.5pt)

Prove your answer is correct.

:bulb: If your answer to the previous question is $\omega(G) = n$,
then you should 

1. find an induced subgraph $K_n$,
2. show that there is no induced $K_{n+1}$ by considering all the cases
   where it might exist and argue that in each case there is
   actually no induced $K_{n+1}$.

```julia

answer = L"""
There is a $K_3$ induced by $x_1, x_2, x_3$.
So $\omega(G) \ge 3$.
    
To show that $\omega(G) \le 3$, it suffices to show
there is no induced $K_4$ in the graph.
We consider four different cases:
\begin{itemize}
    \item A $K_4$ including $z$
    \item A $K_4$ including more than one $y_i$'s
    \item A $K_4$ including only $x_i$'s
    \item A $K_4$ including one $y_i$'s and $x_i$'s
\end{itemize}
None of these are possible because:
\begin{itemize}
    \item $z$ is only connected to $y_i$'s and there is no edge among $y_i$'s;
    \item there is no edge among $y_i$'s;
    \item $x_1, x_2, x_3$ cannot induce a $K_4$;
    \item none of the $y_i$'s are connected to all three $x_i$'s.
\end{itemize}
"""

long_answer(answer, vfill=true)
```

`j print_in_quiz(L"\pagebreak{}")`

\begin{center}
--- This is the end of the quiz. ---
\end{center}

![The general :rabbit:-hole principle. Created by `https://bing.com/create`](./bunny.jpg){width=400px}
