import networkx as nx
import matplotlib.pyplot as plt

# Construct the tree from the Prufer code
code = [2, 1, 3, 4]
code_nx = [i-1 for i in code]
tree = nx.from_prufer_sequence(code_nx)

# Define drawing options
options = {
    "font_size": 20,
    "node_size": 1500,
    "node_color": "orange",
    "edgecolors": "grey",  # Add black borders
    "linewidths": 1,
    "width": 3,
}

# Generate layout and draw the tree
pos = nx.spring_layout(tree, seed=42)  # Added seed for reproducibility

# Create a label mapping to add 1 to each label
label_mapping = {node: node + 1 for node in tree.nodes()}

# Draw the tree with updated labels
nx.draw_networkx(tree, pos, labels=label_mapping, edge_color="grey", **options)

# Remove the frame
plt.axis('off')

# Save the figure
plt.savefig("./tree.jpg")
