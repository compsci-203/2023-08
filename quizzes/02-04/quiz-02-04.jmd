---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 02 (04)
author: "Instructor Xing Shi Cai"
date: 2023-09-19
numbersections: true
weave_options:
    echo: false
    results: "hidden"
---

```julia

using Latexify
using LaTeXStrings
using StatsBase
using WeaveHelper

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}
\large{}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
\end{tcolorbox}

\pagebreak{}

# Permutations (0.5pt)

Ten :rabbit: compete in a gardening competition. Among them, there are three
of siblings.

How many different outcomes are there for the top five places given the
following restirctions?

(Consider the two following problems separately.)

## (0.25pt)

Not included.


## (0.25pt)

```julia

answer = 2 * binomial(9, 4)*factorial(4)

```
At most two of the three siblings **can** be placed in the top five.

```julia; results="tex"

answer = convert(Int, binomial(7,5)*factorial(5) 
    + binomial(3,1)*binomial(7,4)*factorial(5)
    + binomial(3,2)*binomial(7,3)*factorial(5))
short_answer(L"P(10,5) - \binom{7}{2}*5! = \binom{7}{5} 5! + \binom{3}{1} \binom{7}{4} 5! + \binom{3}{2} \binom{7}{3} 5! = %$answer")

```

# Lattice Paths (1pt)

Not included.

# Strings (0.5pt)

For each word below, determine the number of rearrangements of the word in which
all letters **except three** must be used.

## (0.25pt)

Not included.

## (0.25pt)

VERTEX

```julia; results="tex"

answer = convert(Int, 5*4*3 + 1/2*factorial(3)*binomial(4, 1))
short_answer(L"P(5, 3) + \frac{1}{2} P(3, 3) \binom{4}{1} = %$answer")

```

# Proof (1pt)

Prove by induction that for all $n \in \mathbb{N}$,
$9^n - 4^n$ is divisible by $5$.

## The Base Case (0.5pt)

Finish the base case in your proof.

```julia; results="tex"

long_answer(L"""
Since for $n=0$, we have $9^0 - 4^0 = 1 - 1 = 0$ which is clearly divisible by
$5$, the statement holds in this case.
""")

```

## Inductive Step (0.5pt)

Finish the inductive step in your proof.

```julia; results="tex"

answer = L"""
Now assume that the stament is true for $n = m$ for some $m \in \mathbb{N}$,
we will show that it is also true for $n = m+1$.
Note that
\begin{equation*}
    9^{m+1} - 4^{m+1} 
    = 
    9^{m+1} - 9 \times 4^m + 9 \times 4^m - 4^{m+1}
    =
    9(9^{m} - 4^m) + 5 \times 4^{m}.
\end{equation*}
The first term is divisible by $5$ by the induction hypothesis,
and the second term is divisible by $5$ since it has a factor $5$.
Thus $9^{m+1} - 4^{m+1}$ is divisible by $5$.
"""

long_answer(answer; vfill=true)

```

\pagebreak{}

\begin{center}
--- This is the end of the quiz. ---
\end{center}
