(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12093,        368]
NotebookOptionsPosition[     10079,        323]
NotebookOutlinePosition[     10468,        339]
CellTagsIndexPosition[     10425,        336]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"expr", "=", 
  RowBox[{
   FractionBox["1", "2"], 
   SuperscriptBox["E", 
    RowBox[{"2", "x"}]], 
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox["E", "x"], "-", 
     SuperscriptBox["E", 
      RowBox[{"-", "x"}]]}], ")"}], 
   RowBox[{"(", 
    RowBox[{"x", "+", "1"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.90331266097218*^9, 3.903312679164249*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"76e0b534-ff67-49a1-b8b2-d1ba4b1317bf"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"2", " ", "x"}]], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"-", "x"}]]}], "+", 
    SuperscriptBox["\[ExponentialE]", "x"]}], ")"}], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", "x"}], ")"}]}]], "Output",
 CellChangeTimes->{3.9033126805915613`*^9, 3.9033139262417583`*^9},
 CellLabel->"Out[2]=",ExpressionUUID->"e5fba20d-c248-4c71-a5f3-47d0a132816e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expr1", "=", 
  RowBox[{
   RowBox[{"SeriesCoefficient", "[", 
    RowBox[{"expr", ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "n"}], "}"}]}], "]"}], "//", 
   "ExpandAll"}]}]], "Input",
 CellChangeTimes->{{3.903312682289839*^9, 3.903312709598263*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"de0849f1-a77c-424b-9f2b-24c5b7fcefd0"],

Cell[BoxData[
 TagBox[GridBox[{
    {"\[Piecewise]", GridBox[{
       {
        RowBox[{
         RowBox[{"-", 
          FractionBox["1", 
           RowBox[{"2", " ", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{
               RowBox[{"-", "1"}], "+", "n"}], ")"}], "!"}]}]]}], "+", 
         FractionBox[
          SuperscriptBox["3", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "n"}]], 
          RowBox[{"2", " ", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{
              RowBox[{"-", "1"}], "+", "n"}], ")"}], "!"}]}]], "-", 
         FractionBox["1", 
          RowBox[{"2", " ", 
           RowBox[{"n", "!"}]}]], "+", 
         FractionBox[
          SuperscriptBox["3", "n"], 
          RowBox[{"2", " ", 
           RowBox[{"n", "!"}]}]]}], 
        RowBox[{"n", ">", "1"}]},
       {"1", 
        RowBox[{"n", "\[Equal]", "1"}]},
       {"0", 
        TagBox["True",
         "PiecewiseDefault",
         AutoDelete->True]}
      },
      AllowedDimensions->{2, Automatic},
      Editable->True,
      GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.84]}, 
          Offset[0.27999999999999997`]}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}},
      Selectable->True]}
   },
   GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[0.35]}, 
       Offset[0.27999999999999997`]}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}}],
  "Piecewise",
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True,
  Selectable->False,
  StripWrapperBoxes->True]], "Output",
 CellChangeTimes->{{3.903312687053053*^9, 3.903312710485169*^9}, 
   3.903313927103528*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"8f1b7529-4c71-448e-b205-7f7cfa25af45"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expr2", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"FullSimplify", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"n", "!"}], "*", "expr1"}], ",", 
      RowBox[{
       RowBox[{"n", ">=", "0"}], "&&", 
       RowBox[{"n", "\[Element]", 
        TemplateBox[{},
         "Integers"]}]}]}], "]"}], "//", "ExpandAll"}], "//", 
   "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.9033127371494627`*^9, 3.903312772253099*^9}, {
  3.9033130614022207`*^9, 3.903313077754004*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"4b76d590-3d8e-41f2-b3ac-c15a94768e37"],

Cell[BoxData[
 TagBox[GridBox[{
    {"\[Piecewise]", GridBox[{
       {
        RowBox[{
         FractionBox["1", "6"], " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "3"}], "+", 
           SuperscriptBox["3", 
            RowBox[{"1", "+", "n"}]], "-", 
           RowBox[{"3", " ", "n"}], "+", 
           RowBox[{
            SuperscriptBox["3", "n"], " ", "n"}]}], ")"}]}], 
        RowBox[{"n", "\[GreaterEqual]", "1"}]},
       {"0", 
        TagBox["True",
         "PiecewiseDefault",
         AutoDelete->True]}
      },
      AllowedDimensions->{2, Automatic},
      Editable->True,
      GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.84]}, 
          Offset[0.27999999999999997`]}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}},
      Selectable->True]}
   },
   GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{1.}}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[0.35]}, 
       Offset[0.27999999999999997`]}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}}],
  "Piecewise",
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True,
  Selectable->False,
  StripWrapperBoxes->True]], "Output",
 CellChangeTimes->{{3.903312743858885*^9, 3.9033127727772007`*^9}, {
   3.9033130465337353`*^9, 3.903313078187215*^9}, 3.90331392728223*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"b99f43b3-f439-448d-a2e2-f8533fe0b8cd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expr3", "=", 
  RowBox[{"expr2", "[", 
   RowBox[{"[", 
    RowBox[{"1", ",", "1", ",", "1"}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.9033130870183573`*^9, 3.903313092774428*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"4758e7ea-cb46-4d7a-8888-a7b8a6699298"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "6"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "3"}], "+", 
    SuperscriptBox["3", 
     RowBox[{"1", "+", "n"}]], "-", 
    RowBox[{"3", " ", "n"}], "+", 
    RowBox[{
     SuperscriptBox["3", "n"], " ", "n"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.9033130968270283`*^9, 3.90331392740186*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"f5f9b830-13bd-4fb6-b223-457c44811824"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expr3", "/.", 
  RowBox[{"n", "->", "0"}]}]], "Input",
 CellChangeTimes->{{3.903313099020893*^9, 3.903313103610117*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"622b27c9-691d-4fbe-b5f9-cd3bc0f5109b"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.903313104080167*^9, 3.903313927418544*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"5d839a62-9dbe-4903-b819-ec7b724e172d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expr4", "=", 
  RowBox[{"ToExpression", "[", 
   RowBox[{
   "\"\<\\\\frac{1}{2}\\\\left(3^n+3^{n-1} n-n-1\\\\right)\>\"", ",", 
    "TeXForm"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.9033131452933683`*^9, 3.9033131681630363`*^9}, {
  3.9033132219177923`*^9, 3.903313222764591*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"22c6541d-ffcd-45ad-a27b-b050d9613668"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    SuperscriptBox["3", "n"], "-", "n", "+", 
    RowBox[{
     SuperscriptBox["3", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "n"}]], " ", "n"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.9033131686934032`*^9, 3.903313223392371*^9, 
  3.9033139275693493`*^9},
 CellLabel->"Out[7]=",ExpressionUUID->"a9da70a9-01b4-45e0-bf28-82d7406c46f2"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"expr3", "==", "expr4"}], "//", 
  RowBox[{
   RowBox[{"FullSimplify", "[", 
    RowBox[{"#", ",", 
     RowBox[{
      RowBox[{"n", ">=", "0"}], "&&", 
      RowBox[{"n", "\[Element]", 
       TemplateBox[{},
        "Integers"]}]}]}], "]"}], "&"}]}]], "Input",
 CellChangeTimes->{{3.90331317096283*^9, 3.903313211370194*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"cbc32fe7-c843-4f03-9626-8952bc423fe2"],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{{3.903313179309245*^9, 3.903313224398814*^9}, 
   3.903313927586916*^9},
 CellLabel->"Out[8]=",ExpressionUUID->"43de57e8-1c70-42d7-987b-6f5cafb7919d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Expand", "[", "expr3", "]"}], "//", "Factor"}]], "Input",
 CellChangeTimes->{{3.903313455600051*^9, 3.903313552739942*^9}, {
  3.903313921435657*^9, 3.903313922710824*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"706d011e-a62a-4b25-b3ae-368d75f232e2"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "6"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "3"}], "+", 
    SuperscriptBox["3", 
     RowBox[{"1", "+", "n"}]], "-", 
    RowBox[{"3", " ", "n"}], "+", 
    RowBox[{
     SuperscriptBox["3", "n"], " ", "n"}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.903313460578404*^9, 3.903313553032168*^9}, {
  3.903313923098283*^9, 3.903313927604789*^9}},
 CellLabel->"Out[9]=",ExpressionUUID->"3ad634d3-626c-43c4-8068-8ad3f83e92cd"]
}, Open  ]]
},
WindowSize->{1152, 648},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"8596275d-040d-4ca9-96fe-b96f5cbaa4fa"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 464, 14, 43, "Input",ExpressionUUID->"76e0b534-ff67-49a1-b8b2-d1ba4b1317bf"],
Cell[1047, 38, 516, 14, 47, "Output",ExpressionUUID->"e5fba20d-c248-4c71-a5f3-47d0a132816e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1600, 57, 369, 9, 29, "Input",ExpressionUUID->"de0849f1-a77c-424b-9f2b-24c5b7fcefd0"],
Cell[1972, 68, 2160, 66, 81, "Output",ExpressionUUID->"8f1b7529-4c71-448e-b205-7f7cfa25af45"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4169, 139, 575, 16, 29, "Input",ExpressionUUID->"4b76d590-3d8e-41f2-b3ac-c15a94768e37"],
Cell[4747, 157, 1755, 50, 58, "Output",ExpressionUUID->"b99f43b3-f439-448d-a2e2-f8533fe0b8cd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6539, 212, 290, 6, 29, "Input",ExpressionUUID->"4758e7ea-cb46-4d7a-8888-a7b8a6699298"],
Cell[6832, 220, 430, 12, 47, "Output",ExpressionUUID->"f5f9b830-13bd-4fb6-b223-457c44811824"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7299, 237, 222, 4, 29, "Input",ExpressionUUID->"622b27c9-691d-4fbe-b5f9-cd3bc0f5109b"],
Cell[7524, 243, 170, 2, 33, "Output",ExpressionUUID->"5d839a62-9dbe-4903-b819-ec7b724e172d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7731, 250, 387, 8, 33, "Input",ExpressionUUID->"22c6541d-ffcd-45ad-a27b-b050d9613668"],
Cell[8121, 260, 457, 13, 47, "Output",ExpressionUUID->"a9da70a9-01b4-45e0-bf28-82d7406c46f2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8615, 278, 439, 12, 29, "Input",ExpressionUUID->"cbc32fe7-c843-4f03-9626-8952bc423fe2"],
Cell[9057, 292, 201, 3, 33, "Output",ExpressionUUID->"43de57e8-1c70-42d7-987b-6f5cafb7919d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9295, 300, 285, 5, 29, "Input",ExpressionUUID->"706d011e-a62a-4b25-b3ae-368d75f232e2"],
Cell[9583, 307, 480, 13, 47, "Output",ExpressionUUID->"3ad634d3-626c-43c4-8068-8ad3f83e92cd"]
}, Open  ]]
}
]
*)

