---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 05 (01)
author: "Instructor Xing Shi Cai"
date: 2023-09-21
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} \Large{} $\text{\emoji{bomb}}$ Exam Guidelines}]
\large{}
\begin{itemize}
    \item[\emoji{ab}] To write a proper proof, it is necessary to provide both equations and explanatory statements.
    \item[\emoji{robot}] When writing your name and short mathematical answers,
    use clear and distinct letters to facilitate AI recognition.
    \item[\emoji{pencil}] Avoid using pencil as its visibility diminishes upon scanning.
\end{itemize}
\end{tcolorbox}

\pagebreak{}

# Prüfer Code (0.5pt)

Convert the Prüfer code 1153 to a tree.

```julia

answer = L"""
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{tree.jpg}
    \caption{The tree with Prüfer code 1153}
\end{figure}
"""
long_answer(answer, vfill=true)

```

# Hamiltonian graphs (1pt)

## A hamiltonian graph (0.5pt)

Prove the graph in Figure \ref{fig:hamiltonian} is hamiltonian 
using a result from the class **without** acutally finding a hamiltonian cycle.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{hamiltonian-1.jpg}
    \caption{A hamiltonian graph}
    \label{fig:hamiltonian}
\end{figure}

`j print_in_quiz(L"\pagebreak{}")`


```julia

answer = L"""
There are $10$ vertices in the graph and the minium degree is $5 \ge 10/2$.
Thus by Proposition 5.18 (AC), the graph is hamiltonian.

Of course it is very easy to show the graph is hamiltonian by finding a
hamiltonian cycle such as the one in Figure \ref{fig:hamiltonian:2}.
But this is **not** what the question asks for.
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{hamiltonian-2.jpg}
    \caption{A hamiltonian graph}
    \label{fig:hamiltonian:2}
\end{figure}
"""

long_answer(answer, vfill=true)
```

## A non-hamiltonian graph (0.5pt)

Prove the graph in Figure \ref{fig:hamiltonian:3} is not hamiltonian
using a result from the class.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{hamiltonian-3.jpg}
    \caption{A non-hamiltonian graph}
    \label{fig:hamiltonian:3}
\end{figure}

```julia

answer = L"""
It is not difficult to see the graph is bipartite by redrawing it
as in Figure \ref{fig:hamiltonian:4}.
This the complete bipartite graph $K_{4,3}$.
As we have discussed in the class,
since the size both parts are not the same, 
the graph is not hamiltonian.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{hamiltonian-4.jpg}
    \caption{A non-hamiltonian graph?}
    \label{fig:hamiltonian:4}
\end{figure}
"""

long_answer(answer, vfill=true)
```

`j print_in_quiz(L"\pagebreak{}")`

# Clique number and chromatic number (1pt)

Consider the following graph.

\definecolor{juliaorange}{HTML}{FFA500}
\definecolor{jorange}{rgb}{1,0.647,0}
\definecolor{trolleygrey}{rgb}{0.5, 0.5, 0.5}
\definecolor{BrightRed}{HTML}{dd2e44}
\definecolor{MintGreen}{HTML}{78b159}
\definecolor{myBlue}{HTML}{55acee}
\definecolor{PastelYellow}{HTML}{eae559}
\definecolor{SeaGreen}{HTML}{2ca687}
\definecolor{MintWhisper}{rgb}{0.8,1,0.8}
\definecolor{GoldenWhisper}{HTML}{FDCB58}
\definecolor{LavenderDream}{HTML}{AA8ED6}

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
    label distance=-5pt,  % Global setting for all labels
    style={draw=trolleygrey, thick},
    every node/.style={circle, draw=trolleygrey, fill=jorange, ultra thick, minimum height=0.8cm},
    scale=3
    ]
    % xi vertices on a cycle
    \foreach \i in {0,1,...,5} {
        \pgfmathtruncatemacro\label{\i+1}
        \node[label={\i*60: $ x_{\label}$}] (x\i) at (\i*60:1) {};
    }
    % yi vertices on another cycle
    \foreach \i in {0,1,...,5} {
        \pgfmathtruncatemacro\label{\i+1}
        \node[label={\i*60+30: $ y_{\label}$}] (y\i) at (\i*60+20:2) {};
    }
    % Central vertex z
    \node[label=above: $ z $] (z) at (0,0) {};

    % Connect xi vertices to form a cycle
    \foreach \i in {0,1,...,4} {
        \pgfmathtruncatemacro\j{\i+1}
        \draw (x\i) -- (x\j);
    }
    \draw (x5) -- (x0);

    % Connect xi to yi and yi to z
    \foreach \i in {0,1,...,5} {
        \pgfmathtruncatemacro\j{mod(\i+1, 6)}
        \draw (x\i) -- (y\j);
        \pgfmathtruncatemacro\j{mod(\i-1+6, 6)}
        \draw (x\i) -- (y\j);
        \draw (y\i) -- (z);
    }
    \end{tikzpicture}
    \caption{A graph $ G $}
    \label{fig:mycielski}
\end{figure}

## Clique number (1pt)

What are the clique number and chromatic number of $G$?

`j short_answer(L"\omega(G), \chi(G)", "2, 2")`

`j print_in_quiz(L"\pagebreak{}")`

## Proof (0.5pt)

Prove your answers are correct.

:bulb: We have seen a graph which is very similar to this.

```julia

answer = L"""
In a recent lecture, we discussed the proof of Proposition 5.25 by J. Mycielski,
which gives a method for constructing a new graph $G_{t+1}$ from an
existing graph $G_t$. Importantly, if $G_t$ is triangle-free and has a chromatic
number $c$, then $G_{t+1}$ will also be triangle-free and have a chromatic
number $c+1$.

For this problem, the graph depicted in Figure \ref{fig:mycielski} represents
$G_{t+1}$, which is constructed from $G_t = C_6$ using Mycielski's construction.
Since $\chi(C_6) = 2$ and $C_6$ is triangle-free, it follows that $G_{t+1}$
has a chromatic number of $\chi(G_{t+1}) = 3$ and also remains triangle-free.
"""

long_answer(answer, vfill=true)
```

`j print_in_quiz(L"\pagebreak{}")`

\begin{center}
--- This is the end of the quiz. ---
\end{center}

![The general :rabbit:-hole principle. Created by `https://bing.com/create`](./bunny.jpg){width=400px}
