(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5499,        151]
NotebookOptionsPosition[      4459,        124]
NotebookOutlinePosition[      4890,        141]
CellTagsIndexPosition[      4847,        138]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["4", "Section",
 CellChangeTimes->{
  3.903514827440255*^9},ExpressionUUID->"ff1603e5-feab-4cbd-a450-\
6edde6f1f6d6"],

Cell["\<\
I want to random graphs with two component, one is a tree, the other is a \
cycle.\
\>", "Text",
 CellChangeTimes->{{3.90360764506089*^9, 
  3.903607659079578*^9}},ExpressionUUID->"e1e8159f-ff2e-4c13-bc4c-\
d9cf8437119a"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SeedRandom", "[", "1234", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"While", "[", 
  RowBox[{"True", ",", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"g", "=", 
     RowBox[{"RandomGraph", "[", 
      RowBox[{"BernoulliGraphDistribution", "[", 
       RowBox[{"8", ",", "0.2"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
    
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"Length", "[", 
         RowBox[{"ConnectedComponents", "[", "g", "]"}], "]"}], "==", "2"}], "&&", 
       RowBox[{
        RowBox[{"EdgeCount", "[", "g", "]"}], "==", "9"}]}], ",", 
      RowBox[{"Break", "[", "]"}]}], "]"}]}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.903608396044381*^9, 3.903608491496523*^9}, {
  3.90360858137121*^9, 3.9036086293235197`*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"683f7827-8a7c-4ea0-a38b-716331755642"],

Cell[CellGroupData[{

Cell[BoxData["g"], "Input",
 CellChangeTimes->{3.903608605501314*^9},
 CellLabel->"In[25]:=",ExpressionUUID->"94b3d431-9110-4344-96f0-afe0f2befa10"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8}, {Null, 
       SparseArray[
       Automatic, {8, 8}, 0, {
        1, {{0, 1, 2, 4, 5, 9, 12, 15, 18}, {{5}, {4}, {6}, {7}, {2}, {1}, {
          6}, {7}, {8}, {3}, {5}, {8}, {3}, {5}, {8}, {5}, {6}, {7}}}, 
         Pattern}]}]]}, 
    TagBox[
     GraphicsGroupBox[
      GraphicsComplexBox[{{3.373918395722327, -0.5107275862279222}, {
       1.31388810619992, -1.9036837282760728`}, {
       0.3138881061999199, -0.3917083576816349}, {
       0.3138881061999199, -1.9036837282760728`}, {
       2.196068883650255, -0.6923516978822299}, {
       1.3232282348198159`, -0.31388810619992014`}, {
       1.0177113971417915`, -1.0719008960401375`}, {
       1.6981836189309334`, -1.275907515876233}}, {
        {Hue[0.6, 0.7, 0.5], Opacity[0.7], Arrowheads[0.], 
         ArrowBox[{{1, 5}, {2, 4}, {3, 6}, {3, 7}, {5, 6}, {5, 7}, {5, 8}, {6,
           8}, {7, 8}}, 0.03066847341855297]}, 
        {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[0.7]}], 
         DiskBox[1, 0.03066847341855297], DiskBox[2, 0.03066847341855297], 
         DiskBox[3, 0.03066847341855297], DiskBox[4, 0.03066847341855297], 
         DiskBox[5, 0.03066847341855297], DiskBox[6, 0.03066847341855297], 
         DiskBox[7, 0.03066847341855297], DiskBox[8, 0.03066847341855297]}}]],
     
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FormatType->TraditionalForm,
  FrameTicks->None]], "Output",
 CellChangeTimes->{{3.9036086059543867`*^9, 3.903608631527712*^9}},
 CellLabel->"Out[25]=",ExpressionUUID->"63692526-b6d5-4102-a4fa-710723f64097"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"degSeq", "=", 
  RowBox[{
   RowBox[{"VertexDegree", "[", "g", "]"}], "//", "ReverseSort"}]}]], "Input",\

 CellChangeTimes->{{3.903608636635366*^9, 3.903608649802127*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"0eea4363-3045-4c36-b373-9fe8b37cc4c8"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "4", ",", "3", ",", "3", ",", "3", ",", "2", ",", "1", ",", "1", ",", "1"}],
   "}"}]], "Output",
 CellChangeTimes->{{3.9036086402164917`*^9, 3.903608650286964*^9}},
 CellLabel->"Out[28]=",ExpressionUUID->"0fd7acac-51a5-4bdb-a02f-0b563a88e47c"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{572.4, 644.4},
WindowMargins->{{1.7999999999999998`, Automatic}, {
  Automatic, 1.7999999999999998`}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"fac728f0-23b8-471c-936e-8ec906b94405"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 122, 3, 67, "Section",ExpressionUUID->"ff1603e5-feab-4cbd-a450-6edde6f1f6d6"],
Cell[705, 27, 231, 6, 58, "Text",ExpressionUUID->"e1e8159f-ff2e-4c13-bc4c-d9cf8437119a"],
Cell[939, 35, 902, 22, 111, "Input",ExpressionUUID->"683f7827-8a7c-4ea0-a38b-716331755642"],
Cell[CellGroupData[{
Cell[1866, 61, 148, 2, 29, "Input",ExpressionUUID->"94b3d431-9110-4344-96f0-afe0f2befa10"],
Cell[2017, 65, 1813, 36, 213, "Output",ExpressionUUID->"63692526-b6d5-4102-a4fa-710723f64097"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3867, 106, 275, 6, 29, "Input",ExpressionUUID->"0eea4363-3045-4c36-b373-9fe8b37cc4c8"],
Cell[4145, 114, 286, 6, 33, "Output",ExpressionUUID->"0fd7acac-51a5-4bdb-a02f-0b563a88e47c"]
}, Open  ]]
}, Open  ]]
}
]
*)

