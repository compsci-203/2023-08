\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\input{../graphs/interval-1.tex}
\input{../graphs/interval-2.tex}
\input{../graphs/k3-3.tex}

\title{Lecture \lecturenum{} -- Graph Colouring (3)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 5.4.3 Can We Determine Chromatic Number?}

\subsection{First-Fit Algorithm}

%\begin{frame}
%    \frametitle{\think{} How to find a proper colouring?}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{bunny-paint.jpg}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Computational Complexity}

    Deciding if $\chi(G) \le k$ is
    \href{https://en.wikipedia.org/wiki/NP-complete}{NP-complete}
    i.e., very hard, except for the cases $k \in \{0,1,2\}$.

    \cake{} Why it is easy to check if $\chi(G) = 0$?
    \vfill{}
    \cake{} Why it is easy to check if $\chi(G) = 1$?
    \vfill{}
    \cake{} Why it is easy to check if $\chi(G) = 2$?
    \vfill{}
\end{frame}

\begin{frame}
    \frametitle{First-fit algorithm}

    A \emph{greedy} algorithm to find a proper colouring $\phi$:
    \begin{enumerate}
        \item Fix an ordering of the vertices $(v_1 , v_2 , \dots, v_n)$.
        \item Let $\phi(v_{1}) = 1$.
        \item Given of $\phi(v_1), \phi(v_2), \ldots, \phi(v_i)$, let
            $\phi(v_{i+1})$
            be the smallest integer (colour) \emph{not} have been used
            on $v_{i+1}$'s neighbours.
        \item Repeat step 3 until all vertices have a colour.
    \end{enumerate}
    This is called the \alert{first-fit} algorithm.
\end{frame}

\begin{frame}
    \frametitle{Example: First-fit algorithm}

    Let's find a \emph{proper} colouring of the following graph via first-fit.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\textwidth]{graph-to-colour-1.pdf}
        \includegraphics<2>[width=0.6\textwidth]{graph-to-colour-2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    How many colours does the first-fit algorithm need for
    the same graph but different ordering of vertices?

    \begin{figure}
        \hfill
        \begin{subfigure}{0.48\textwidth}
            \begin{center}
                \begin{tikzpicture}
                    \draw (0,0)
                        pic[
                        style={graphedge},
                        every node/.style={graphnodesmall},
                        xscale=2,
                        yscale=1.5,
                        ]
                        {interval-1};
                \end{tikzpicture}
            \end{center}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.48\textwidth}
            \begin{center}
                \begin{tikzpicture}
                    \draw (0,0)
                        pic[
                        style={graphedge},
                        every node/.style={graphnodesmall},
                        xscale=2,
                        yscale=1.5,
                        ]
                        {interval-2};
                \end{tikzpicture}
            \end{center}
        \end{subfigure}
        \hfill
    \end{figure}
\end{frame}

\subsection{Maximum Degrees and Chromatic Numbers}

\begin{frame}
    \frametitle{The maximum degree}

    Let $\Delta(G)$ be the maximum degree in $G = (V, E)$, i.e.,
    \begin{equation*}
        \Delta(G) = \max_{v \in V} \deg_G(v)
    \end{equation*}

    \cake{} What is $\Delta(G)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{lollipop-5.pdf}
        \caption*{A ``lollipop'' graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 4.4.5 (\acs{dm}): Brook's Theorem}

    Any graph $G$ satisfies $χ(G) \le ∆(G)$,
    unless $G$ is a complete graph or an odd cycle,
    in which case $χ(G) = ∆(G) + 1$.

    \begin{figure}
        \centering
        \includegraphics<1>[width=0.5\textwidth]{c7.pdf}
        \includegraphics<2>[width=0.5\textwidth]{k5.pdf}
        \includegraphics<3>[width=0.7\textwidth]{lollipop-5.pdf}
        \only<1>{\caption*{An odd cycle}}
        \only<2>{\caption*{A complete graph}}
        \only<3>{\caption*{Not an odd cycle, not a complete graph.}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A simple upper bound}

    For any graph $G$, $\chi(G) \le \Delta(G) + 1$.

    The proof is to show that the first-fit algorithm can find a proper
    colouring with at most $\Delta(G) + 1$ colours.
\end{frame}

\subsection{Interval Graphs}

\begin{frame}
    \frametitle{Motivational example: A film festival}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-cinema.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{How many film can you watch?}

    The screening schedule of $6$ films are shown below.

    What is the max number of films which the \emoji{rabbit-face} 
    can watch in their entirety?

    \begin{figure}[htpb]
        \footnotesize
        \begin{tikzpicture}[
            every node/.style={midway},
            style={graphedge, draw=black},
            xscale=1.3,
            yscale=1.2,
            auto
            ]
            \input{../graphs/interval-4.tex}
        \end{tikzpicture}
        \caption*{The screening schedule}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Interval graphs}

    Given a list of intervals $(S_{v})_{v \in V}$, the \emph{interval graph} is the
    graph with vertex set $V$ and edge set
    \begin{equation*}
        E = \{ uv : S_{u} \cap S_{v} \ne \emptyset\}
    \end{equation*}

    \vspace{-2em}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \footnotesize
                \begin{tikzpicture}[
                    every node/.style={midway},
                    style={graphedge, draw=black},
                    yscale=0.6,
                    xscale=0.9,
                    auto
                    ]
                    \input{../graphs/interval-4.tex}
                \end{tikzpicture}
                \caption*{Intervals}
            \end{figure}
            \cake{} What are $\chi(G)$ and $\omega(G)$?
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{film-festival-1.pdf}
                \caption*{An interval graph $G$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.28 (\acs{ac})}

    If $G = (V, E)$ is an interval graph, then $\chi(G) = \omega(G)$.

    Proof: We have seen before that $\chi(G) \ge \omega(G)$.

    On the other hand, if $\chi(G) = m$, 
    we show that $G$ must have $K_{m}$ a subgraph, i.e., $\omega(G) \ge
    m = \chi(G)$.
\end{frame}

\subsection{Independent number}

\begin{frame}
    \frametitle{Review: Independent Graphs}
    Recall that an independent graph \( I_n \) is a graph with \( n \) vertices and no edges.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode, minimum size=1em},
            style={graphedge},
            scale=0.8]
            % Vertices
            \foreach \i in {1,...,8}
            {
                \node[label={\i*45-90: \( v_{\i} \)}] (\i) at
                    (\i*45-90:2cm) {};
            }
        \end{tikzpicture}
        \caption*{Example: \( I_{8} \)}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Review: Induced subgraphs}

    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is called \alert{an induced subgraph}
    if $xy \in E$ and $x, y \in W$ implies that $xy \in F$.

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph.pdf}}
            \caption*{A subgraph which is \emph{not} an induced subgraph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph-induced.pdf}}
            \caption*{A subgraph which is also an induced subgraph}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Independence number}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \setlength{\parskip}{1em}  % Enhance paragraph spacing

            The \emoji{rabbit-face}'s problem is the same as:

            What is the size of the largest induced $I_{k}$ in of $G$?

            This number, denoted by $\alpha(G)$, 
            is called the \alert{independent number} of $G$.

            What is $\alpha(G)$ for the interval graph $G$?
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=0.7\textwidth]{film-festival-2.pdf}
                \includegraphics<2>[width=0.7\textwidth]{film-festival-3.pdf}
                \includegraphics<3>[width=0.7\textwidth]{film-festival-4.pdf}
                \caption*{The interval $G$ has an induced $\only<1>{I_2}\only<2>{I_3}\only<3>{I_4}$}
            \end{figure}  % This was missing
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Independence number and chromatic number}

    If graph $G$ has $n$ vertices, then
    \begin{equation*}
        \alpha(G) \ge \frac{n}{\chi(G)}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Can You Draw the Interval Graph $G$?

    \think{} What are, $\alpha(G)$,  $\omega(G)$ and $\chi(G)$?

    \bigskip

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Interval_graph.svg.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{The clique number of an interval graph}
%
%    Try colouring this graph with first-hit.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.51\linewidth]{interval-2.pdf}
%        \includegraphics[width=0.51\linewidth]{interval-1.pdf}
%    \end{figure}
%\end{frame}

\subsection{Perfect Graphs}

%\begin{frame}
%    \frametitle{What is a perfect sunset?}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{bunny-sunset.png}
%    \end{figure}
%\end{frame}
%
\begin{frame}
    \frametitle{What is a perfect graph?}

    A graph $G$ is said to be \alert{perfect} if $\chi(H) = \omega(H)$ for every
    \emph{induced} subgraph $H$.

    Let's check if the following graph is perfect.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{graph-perfect-example-1.pdf}
        \caption*{Example: A perfect graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Interval graphs}
    
    Why is every interval graph perfect?
    
    \vspace{-1em}

    \input{../graphs/interval-3.tex}
\end{frame}

\begin{frame}
    \frametitle{Complete graphs are prefect}

    Why is every complete graph perfect?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{k6.pdf}
        \caption*{$K_{6}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complement graphs}

    % Introduction to the concept of complement graphs
    Given a graph \( G \), its \alert{complement graph} is obtained by removing
    all existing edges and adding edges where none existed.

    \begin{figure}[htpb]
        \centering
        % Use subfigures to label each graph
        \hfill
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\linewidth]{graph-complement-example-1.pdf}
            \caption*{A graph \( G \)}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\linewidth]{graph-complement-example-2.pdf}
            \caption*{The complement of \( G \)}
        \end{subfigure}
        \hfill
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{hole}

    A \alert{hole} is a cycle of length at least $5$.

    An \alert{anti-hole} is the complement of a hole.

    \begin{figure}[htpb]
        \centering
        \hfill
        \includegraphics<1>[width=0.4\textwidth]{c5.pdf}
        \includegraphics<2>[width=0.4\textwidth]{c7.pdf}
        \hfill
        \includegraphics<1>[width=0.4\textwidth]{c5-complement.pdf}
        \includegraphics<2>[width=0.4\textwidth]{c7-complement.pdf}
        \hfill
        \caption*{A hole and an anti-hole of \only<1>{$5$}\only<2>{$7$} vertices}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Strong Perfect Graph Theorem}

    \begin{block}{Strong Perfect Graph Theorem}
        A graph is \emph{perfect} if and only if it has neither odd holes nor odd anti-holes
        as \emph{induced} subgraphs.

        In other words, a graph is \emph{perfect} if and only if itself and its complement
        do not contain odd holes as induced subgraphs.
    \end{block}

    \pause{}

    \astonished{} The proof is more than 100 pages long! This is one of the most
    celebrated results in graph theory.

    The theorem was proved by
    \href{https://en.wikipedia.org/wiki/Maria\_Chudnovsky}{Maria Chudnovsky},
    Neil Robertson, Paul Seymour, and
    Robin Thomas (2006) is the following.
\end{frame}

\begin{frame}
    \frametitle{Example: Non-perfect graphs}
    
    \cake{} Can you see why the following graph contains an induced odd
    \only<2>{anti-}hole?

    \hint{} It follows the Strong Perfect Graph Theorem that it is not perfect.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{clique-number-02.pdf}
        \caption*{A graph with an inducted odd \only<2>{anti-}hole}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Complete graphs}

    \cake{} Can you see why neither $K_{n}$ nor its complement contains an
    induced odd hole.

    \hint{} It follows the Strong Perfect Graph Theorem that $K_{n}$ is perfect.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\linewidth]{k6.pdf}
        \caption*{Example: $K_{6}$ has neither a odd hole nor an odd anti-hole}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Bipartite graphs}
    
    \cake{} Can you see why neither $K_{m, n}$ nor its complement contains an
    induced odd hole.

    \hint{} It follows the Strong Perfect Graph Theorem that $K_{m, n}$ is perfect.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{k3-4.pdf}
        \caption*{Example: $K_{3,4}$ has neither an odd hole nor an odd anti-hole}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Maria Chudnovsky}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Maria Chudnovsky is one of the coauthors who proved The Strong Perfect Graph
            Theorem.

            \medskip{}

            She won a MacArthur Fellowship (2012), which comes
            with 500,000 \emoji{dollar}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{Chudnovsky.jpg}
                \caption*{Maria Chudnovsky}%
            \end{figure}

        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-22.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \emoji{closed-book} \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 25, 27.
            \end{itemize}

            \emoji{green-book}
            \href{http://discrete.openmathbooks.org/dmoi3}{\acf{dm}},
            \begin{itemize}
                \item[\emoji{pencil}]
                    Section 4.4: 1, 2, 3, 5, 7, 9, 10, 12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
