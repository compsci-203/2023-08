\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Recurrence Equations (2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 9.4.2 Solving Advancement Operator Equations -- Nonhomogeneous Case}

\begin{frame}
    \frametitle{Nonhomogeneous equations}
    Consider the homogeneous equation
    \begin{equation}
        \label{eq:hom:1}
        (A+2)(A-6) f = 0.
    \end{equation}
    \cake{} What is the general solution, let's call it $f_1(n)$, for this equation?
    \[
        f_1(n) = \blankshort{}
    \]

    \think{} What is the general solution for
    \[
        (A+2)(A-6) f = 3^{n}
    \]
\end{frame}

\begin{frame}{Finding a particular solution}
    Unfortunately, no known algorithm guarantees a solution for
    \begin{equation}
        \label{eq:nonhom:1}
        (A + 2)(A - 6) f = 3^n
    \end{equation}
    Nevertheless, experience suggests that a solution could closely resemble the right-hand side (RHS):
    \[
        d \cdot 3^n,
    \]
    where \(d\) is a constant, independent of \(n\).

    To validate this, plug \(d \cdot 3^n\) into equation \eqref{eq:nonhom:1} to determine the value of \(d\) that satisfies the equation.

    \emoji{pencil} Let's try it!
\end{frame}

\begin{frame}[c]{Particular solution vs general solution}
    \begin{columns}[c]
        \begin{column}{.5\textwidth}
            A \alert{particular} solution satisfies the equation but doesn't capture all possible solutions.

            \bigskip{}

            A \alert{general} solution, on the other hand, encompasses all solutions to the equation.
        \end{column}
        
        \begin{column}{.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{bunny-particular.jpg}
                \caption*{A general \bunny{} surrounded by particular \bunny{}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{What we have now}
    We have found a \alert{general} solution
    \begin{equation*}
        f_1(n) = c_1 \cdot (-2)^{n} + c_2 \cdot 6^{n}
    \end{equation*}
    for the homogeneous linear recurrence
    \begin{equation*}
        (A + 2)(A - 6) f = 0. \tag{\ref{eq:hom:1}}
    \end{equation*}
    We've also found one \alert{particular} solution:
    \[
        f_{2}(n) = -\frac{1}{15} \cdot 3^{n}
    \]
    for the nonhomogeneous linear recurrence
    \begin{equation*}
        (A + 2)(A - 6) f = 3^n. \tag{\ref{eq:nonhom:1}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The general solution of \eqref{eq:nonhom:1}}
    Therefore, the general solutions of \eqref{eq:nonhom:1} is:
    \[
        f(n) = f_{1}(n) + f_{2}(n) = c_{1} \cdot (-2)^{n} + c_{2} \cdot 6^{n} -
        \frac{1}{15} \cdot 3^{n}
    \]

    \think{} Why is this the case?
\end{frame}

\begin{frame}{\emoji{ramen} A recipe for nonhomogeneous linear recurrences}
    To solve equations of the form 
    \[
        p(A) f = g
    \]
    we take the following steps:

    \begin{enumerate}
        \item Find the \emph{general} solution \( f_{1} \) for
        \[
            p(A) f = 0
        \]
        \item Find any \emph{particular} solution \( f_{2} \) (typically by guessing) for
        \[
            p(A) f = g
        \]
    \end{enumerate}

    The general solution combines both:
    \[
        f(n) = f_{1}(n) + f_{2}(n)
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 9.15}
    
    Find the solutions to the equation
    \begin{equation*}
        (A + 2)(A − 6) f = 6^n,
    \end{equation*}
    if $f(0) = 1$ and $f(1) = 5$.

\end{frame}

\begin{frame}[fragile]{Automated solution with Wolfram Alpha \emoji{robot}}
    Use this
    \href{https://www.wolframalpha.com/input?i=solve+f\%5Bn\%2B2\%5D-4*f\%5Bn\%2B1\%5D-12f\%5Bn\%5D\%3D\%3D6\%5En\%2C+f\%5B1\%5D\%3D\%3D5\%2C+f\%5B0\%5D\%3D\%3D1}{query} to get a solution from Wolfram Alpha.
    \begin{small}
        \begin{verbatim}
solve f[n+2]-4*f[n+1]-12f[n]==6^n, f[1]==5, f[0]==1
        \end{verbatim}
    \end{small}
    The resulting solution is:
    \[ f(n) = \frac{1}{3} \cdot 2^{n - 6} \left( 4 \cdot 3^n \cdot n + 27 \cdot (-1)^n + 55 \cdot 3^{n + 1} \right) \]
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Can you guess the next problem?}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Prompt ---
            Highly detailed digital painting of a mouse,  a cow, a tiger, a rabbit, a dragon, a snake and a horse on the African prairie by makoto shinkai and krenz cushart.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=\textwidth]{prairie-01.jpg}
                \includegraphics<2>[width=\textwidth]{prairie-02.jpg}
                \caption*{Made with \only<1>{DALL\cdot E in spring 2023}\only<2>{Bing in autumn 2023}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 9.16}

    Find \emph{all} solutions to the equation
    \begin{equation*}
        r_{n+1} = r_{n} + n + 1.
    \end{equation*}
\end{frame}

\begin{frame}{Example 9.16 with initial conditions}
    Find \emph{the} solution to the equation
    \[
        r_{n+1} = r_n + n + 1
    \]
    given \( r(0) = 1 \) and \( r(1) = 2 \).
\end{frame}

\begin{frame}{\zany{} Unique solutions for linear recurrences}
    Consider a linear recurrence of order \( k \) of the form
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \ldots + c_{k} a_{n} = g(n),
        \quad \text{for all } n \ge 0,
    \]
    with initial conditions \( a_{0}, a_{1}, \ldots, a_{k-1} \).

    \cake{} How many unique solutions exist for this linear recurrence?
\end{frame}

%\begin{frame}
%    \frametitle{Towers of Hanoi puzzle}
%
%    These disks are to be transferred, one at a time, onto another peg, 
%    such that at no time is a larger disk is on top of a smaller one. 
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\textwidth]{Tower_of_Hanoi.jpg}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{The recurrent equation for towers of Hanoi}
%    
%    Let $h_n$ be the minimal number of moves needed.
%
%    Find a recursions for $h_{n}$.
%
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you solve the recursion
    \begin{equation*}
        h_{n+1} = 2 h_{n} + 1, \qquad \text{for all } n \ge 0
    \end{equation*}
    with the initial condition $h_1 = 1$.

    \hint{} This recursion comes from
    \href{https://en.wikipedia.org/wiki/Tower\_of\_Hanoi}{Towers of Hanoi}
    problem.
\end{frame}

\section{\zany{} \acs{ac} 9.5 Formalizing our approach to recurrence equations}

%\begin{frame}
%    \frametitle{Linear Algebra in 5 minutes}
%    
%    This section contains what you should remember after a 7-week 
%    Linear Algebra course.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.5\textwidth]{vector-space.png}
%    \end{figure}
%\end{frame}
%
%\begin{frame}[c]
%    \frametitle{Vector space}
%
%    \begin{columns}[c]
%        \begin{column}{0.5\textwidth}
%            A \alert{vector space}  is a set whose elements, 
%            often called \alert{vectors}, 
%            may be added together and multiplied by numbers.
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=0.8\textwidth]{vector.png}
%                \caption*{Vectors}
%            \end{figure}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Linear combination}
%    
%    Given $k$ vectors $v_{1}, \dots, v_{k}$, the expression
%    \begin{equation*}
%        c_1 v_{1} + c_2 v_{2} + \ldots + c_k v_{k}
%    \end{equation*}
%    is called a \alert{linear combination} of these vector,
%    and $c_{1}, \dots, c_{k}$ are the \alert{weights}.
%
%    If the linear combination of 
%    \begin{equation*}
%        c_1 v_{1} + c_2 v_{2} + \ldots + c_k v_{k}
%        =
%        \mathbf{0}
%    \end{equation*}
%    \emph{only} when the weights are all $0$,
%    they are called \alert{linearly independent}.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Dimensions}
%
%    A vector space $W$ has \alert{dimension} $k$
%    if we can find at most $k$ \emph{linearly independent}
%    vectors in $W$.
%
%    In such a case, any vector in $W$ is the \emph{linear combination}
%    of these $k$ vectors.
%\end{frame}

\begin{frame}{Theorem 9.18}
    The set of all solutions to an \emph{order-\(k\)} homogeneous linear equation
    \begin{equation*}
        \label{eq:9.5.1}
        (c_0 A^k + c_1 A^{k-1} + c_2 A^{k-2} + \ldots + c_k) f = 0 
        \tag{9.5.1}
    \end{equation*}
    forms a \(k\)-dimensional vector space \(V\).

    \begin{block}{\hint{} Implication}
        Every solution of \eqref{eq:9.5.1} is a linear combination of \(k\)
        linearly independent solutions to \eqref{eq:9.5.1}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Review: Theorem 9.21 (\ac{ac})}

    Let \( p(A) \) be a polynomial with \emph{distinct} roots \( r_{1}, \dots,
    r_{n} \). Then, for any constants \( \temoji{cucumber}_1, \dots, \temoji{cucumber}_k \) in \( \mathbb{R} \),
    the expression
    \begin{equation*}
        \temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}
    \end{equation*}
    is a solution to \( p(A) f = 0 \).

    In other words, 
    \begin{equation*}
        p(A) (\temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}) = 0, 
        \qquad (\text{for all } n \in \mathbb{N})
        .
    \end{equation*}

    Moreover, \alert{every solution} to \( p(A) f = 0 \) can be written in this form.
\end{frame}

\begin{frame}
    \frametitle{Proof of theorem 9.21 for distinct roots}

    First, note that $r_{1}^n, \dots, r_{k}^n$ are solutions to \( p(A) f = 0 \).

    \begin{block}{Linear independence}
        When $r_1, \dots, r_k$ are distinct roots, the functions \( r_{1}^n, \dots, r_{k}^n \) are linearly independent.
    \end{block}

    \begin{block}{Forming a basis}
        By Theorem 9.18,
        these functions form a basis for the $k$-dimensional vector space of all solutions.
    \end{block}

    \begin{block}{General solution}
        Any solution in this space can be represented as a linear combination:
        \[
            \temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}
        \]
        where \( \temoji{cucumber}_{1}, \dots, \temoji{cucumber}_{k} \) are arbitrary constants.
    \end{block}

\end{frame}

%\begin{frame}[t]{Example 9.9 again}
%    Consider
%    \begin{equation}
%        \label{eq:homogeneous:1}
%        p(A) f 
%        = (A^{2}+A-6) f 
%        = (A+3)(A-2)f 
%        = 0
%    \end{equation}
%
%    Its solution space has dimension $\underline{\hspace{1cm}}$.
%
%    Note that $2^{n}$ and $(-3)^{n}$ are two linearly independent solutions.
%
%    Thus
%    \begin{itemize}
%        \item Then \(c_{1} 2^{n} + c_{2} (-3)^{n}\) are also solutions of \cref{eq:homogeneous:1}.
%        \item \alert{All} solutions are of this form.
%    \end{itemize}
%\end{frame}

\section{\acs{ac} 9.6 Using Generating Functions to Solve Recurrences}

\begin{frame}
    \frametitle{Example 9.24: Revisiting an old problem}

    Recall our previous study of solving the recursion:
    \begin{equation*}
        (A^{2} + A - 6) r = 0
    \end{equation*}
    subject to the initial conditions \( r_{0} = 1 \) and \( r_{1} = 3 \).

    This translates to:
    \begin{equation*}
        r_{n+2} + r_{n+1} - 6 r_{n} = 0
    \end{equation*}

    We will show that the Generating Function (GF) of \( r_{n} \) is:
    \begin{equation*}
        f(x) = \frac{1 + 4x}{1 + x - 6x^2}
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{Extract the coefficients}
    
    By performing a partial fraction decomposition, we can rewrite \( f(x) \) as:
    \begin{equation*}
        f(x) = \frac{1 + 4x}{1 + x - 6x^2}
        = \frac{1 + 4x}{(1 + 3x)(1 - 2x)}
        = \frac{6}{5(1 - 2x)} - \frac{1}{5(1 + 3x)}.
    \end{equation*}
    
    \cake{} So, how do we extract \( r_n \) from this?
\end{frame}

\begin{frame}
    \frametitle{Example 9.25}

    Assume that $r_{0}=2$, $r_{1}=1$ and for $n \ge 2$
    \begin{equation*}
        r_{n} − r_{n−1} − 2 r_{n−2} = 2^n.
    \end{equation*}
    Let us show that the generating function of $(r_{n})_{n \ge 0}$ is
    \begin{equation*}
        R(x) = \frac{6x^2 − 5x + 2}{(1 − 2x)(1 − x − 2x^2)}
        .
    \end{equation*}
\end{frame}

\section{\zany{} \acs{ac} 9.7 Solving a nonlinear recurrence}

\begin{frame}
    \frametitle{Review — Catalan numbers}
    
    Recall that the Catalan numbers satisfy
    \begin{equation}
        \label{eq:catalan}
        C_{n+1} = \sum_{k=0}^{n} C_{k} C_{n-k}, \qquad (n \ge 0).
    \end{equation}

    \bomb{} Note that this is \emph{not} a linear recurrence equation, and so
    there's no straightforward method to solve it.

    Nevertheless, for this special case, we can solve it and get
    \begin{equation*}
        C_n = \frac{1}{n+1} \binom{2n}{n}, \qquad (n \ge 0).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Step 1: Shift the index by \(1\)}
    
    To simplify computations, let's define \( c_n \) as:
    \begin{equation*}
        c_n = 
        \begin{cases}
            C_{n-1}, & (n \ge 1), \\
            0, & (n = 0).
        \end{cases}
    \end{equation*}
    
    With this new definition, we can rewrite the recursion
    \begin{equation}
        C_{n+1} = \sum_{k=0}^{n} C_{k} C_{n-k}, \qquad (n \ge 0)
    \end{equation}
    as
    \begin{equation}
        c_{n} = \sum_{k=0}^{n} c_k c_{n-k}, \qquad (n \ge 0).
    \end{equation}
    \puzzle{} Check that the two equations are equivalent after the class.
\end{frame}

\begin{frame}
    \frametitle{Step 2: Find the generating function}
    
    Let $C(x) = \sum_{n \ge 0} c_{n} x^{n}$.
    Then we can show that
    \begin{equation*}
        c_{n} = \sum_{k=0}^{n} c_k c_{n-k}, \qquad (n \ge 0),
    \end{equation*}
    implies
    \begin{equation*}
        C(x) = \frac{1 \pm (1-4 x)^{1/2}}{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Step 3: Extract $c_{n}$}
    
    Applying Newton's binomial theorem to
    \begin{equation*}
        C(x) = \frac{1 \pm (1-4 x)^{1/2}}{2}
    \end{equation*}
    and
    \begin{equation}
        \label{eq:binom:half:choose:k}
        \binom{1/2}{k}
        =
        \frac{(-1)^{k-1}}{k}    
        \frac{\binom{2k-2}{k-1}}{2^{2k-1}}
    \end{equation}
    we have
    \begin{equation*}
        C(x) = 
        \frac{1}{2}
        \pm
        \frac{1}{2}
        \mp
        \sum_{n=1}^{\infty} \frac{\binom{2n-2}{n-1}}{n} x^{n}.
    \end{equation*}

    \puzzle{} Check \eqref{eq:binom:half:choose:k} after the class.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 9.9: 9, 11, 13, 15, 17.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
