\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Power Series and Taylor Series}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acf{ec} 9.4 Power Series}

\begin{frame}
    \frametitle{Power Series}

    A \alert{power series} is an infinite series whose terms involve constants
    $a_n$ and power of $x-c$ where  $x$ is a variable  and $c$ is a constant.

    \pause{}

    As shown in \ac{ec} 9.1 (geometric progression),
    \begin{equation*}
        \sum_{n=0} (x-0)^{n}
        =
        \sum_{n=0} x^{n}
        =
        1 + x + x^{2} + x^{3} + \cdots
        =
        \frac{1}{1-x}
    \end{equation*}
    where the last equality is valid when $-1 < x < 1$.

    \pause{}

    \cake{} Can you see why the series is divergent if $\abs{x} \ge 1$?
\end{frame}

\begin{frame}
    \frametitle{Interval of Convergence}
    
    In general, a power series $\sum_{n=1}^{\infty} f_n(x)$ has an
    \alert{interval of convergence} which
    is an interval on which the series converges.

    \cake{} What is the \emph{interval of convergence} of 
    $\sum_{n=1}^{\infty} x^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Radius of Convergence}

    The \alert{radius of convergence} of a power series 
    $\sum_{n=1}^{\infty} f_n(x)$ 
    is half of the length
    of its \emph{interval of convergence}.

    \cake{} What is the \emph{radius of convergence} of 
    $\sum_{n=1}^{\infty} x^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Power Series and Generating Functions}

    When we consider a power series
    \[
        \sum_{n=0}^{\infty} a_{n} x^{n},
    \]
    we view it as a function of \(x\), 
    which has an associated interval of convergence.

    \pause{}

    However, when we view the same series as a generating function
    \[
        \sum_{n=0}^{\infty} a_{n} x^{n},
    \]
    it is treated as a formal sum to encode the sequence \( \{ a_{n} \} \), with no concern for convergence.
\end{frame}

\begin{frame}
    \frametitle{Determining the Interval of Convergence}

    Consider a power series $\sum_{n=1}^{\infty} f_n(x)$. Define:
    $$r(x) = \lim_{n \to \infty} \left| \frac{f_{n+1}(x)}{f_n(x)} \right|.$$

    Apply the Ratio Test:
    \begin{itemize}
        \item $r(x) < 1$: Series converges.
        \item $r(x) > 1$: Series diverges.
        \item $r(x) = 1$: Test is inconclusive.
    \end{itemize}

    Identify the values of $x$ for which the series converges to determine its interval of convergence.
\end{frame}

\begin{frame}
    \frametitle{Example 9.16}

    Find the interval of convergence of $\sum_{n=1}^{\infty} \frac{x^n}{n!}$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.17}

    Find the interval of convergence of $\sum_{n=1}^{\infty} \frac{x^n}{n}$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.18}

    Find the interval of convergence of $\sum_{n=1}^{\infty} n! x^n$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Find the interval of convergence of
    \[
        \sum_{n=0}^{\infty} \frac{(-1)^n}{(2n+1)!} x^{2n+1} = x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \cdots
    \]
\end{frame}

\begin{frame}
    \frametitle{Differentiation and Integration}

    For a power series
    \begin{equation*}
        f(x) = \sum_{n=1}^{\infty} a_n (x-c)^{n}
    \end{equation*}
    that converges for
    $\abs{x-c} < R$,
    both
    \begin{equation*}
        f'(x)
        =
        \sum_{n=1}^{\infty} n a_n (x-c)^{n-1}
        ,
        \quad \text{and} \quad
        \int f(x) \dd{x}
        =
        C
        +
        \sum_{n=0}^{\infty} \frac{a_n}{n+1} (x-c)^{n+1}
    \end{equation*}
    converge for $\abs{x-c} < R$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.19}

    Let $f(x) = \sum_{n=0} x^{n}$.
    Find the interval of convergence of $f'(x)$.
\end{frame}

%\begin{frame}
%    \frametitle{Bessel Functions}
%
%    Many applications in engineering and physics requires solving differential equations
%    of the form
%    \[
%        x^2 y'' + x y' + (x^2 - m^2) y = 0.
%    \]
%    
%    The solution called Bessel Functions of order \(m\), denoted by \(J_m(x)\),
%    can also be expressed as a power series:
%    \[
%        J_m(x) = \sum_{n=0}^{\infty} \frac{(-1)^n}{n! (n + m)!} \left( \frac{x}{2} \right)^{2n + m}.
%    \]
%
%    \cake{} What is the interval of convergence for \(J_{m}(x)\)?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Bessel Functions in Picture}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{bessel-functions.pdf}
%        \caption*{Bessel Functions}
%    \end{figure}
%\end{frame}

\section{\acf{ec} 9.5 Taylor Series}

\begin{frame}
    \frametitle{The Derivatives of a Power Series}
    
    Consider the function
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} a_n (x-c)^{n}.
    \end{equation*}
    What are $f(c), f'(c), f''(c), f'''(c), \ldots f^{(k)}(c)$?
\end{frame}

\begin{frame}
    \frametitle{Taylor's formula}
    
    Given a function
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} a_n (x-c)^{n},
    \end{equation*}
    we have
    \begin{equation*}
        a_{n} = \frac{f^{(n)}(c)}{n!},
        \quad \text{for} \quad n \ge 0.
    \end{equation*}
    In other words,
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} \frac{f^{(n)}(c)}{n!} (x-c)^{n}.
    \end{equation*}

    This is known as \emph{Taylor series} for $f(x)$ about $x=c$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.20}
    
    Find the Taylor series for $f(x) = e^x$ about $x=0$.
\end{frame}

\begin{frame}
    \frametitle{Why do we both with Taylor series?}
    
    Isn't $e^{x}$ much nicer than
    \begin{equation*}
        \sum_{n=0}^{\infty} \frac{1}{n!} x^n
    \end{equation*}

    The reason is that we can approximate $e^{x}$ by its truncated Taylor series,
    i.e.,
    \begin{equation*}
        e^{x} 
        \approx 1 + x 
        \approx 1 + x + \frac{x^2}{2!}
        \approx 1 + x + \frac{x^2}{2!} + \frac{x^{3}}{3!}
        \approx \cdots
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.22}
    
    Find the Taylor series for $f(x) = \sin(x)$ about $x=0$.
\end{frame}

\begin{frame}
    \frametitle{$O(x^n)$ approximation}
    
    We define the $n$-th degree Taylor polynomial $P_n(x)$ for a function $f(x)$
    about $x = c$ by
    \begin{equation*}
        P_n(x) = \sum_{k=0}^{n} \frac{f^{(k)}(c)}{k!} (x-c)^{k}.
    \end{equation*}

    Thus $P_n(x) = O(x^n)$ is called the $O(x^n)$ approximation of $f(x)$.
\end{frame}

\begin{frame}
    \frametitle{$O(x^n)$ approximation for $\sin(x)$}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{sin-approximation.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Reminder Theorem}
    
    Let $P_n(x)$ be the $n$-th degree Taylor polynomial of $f(x)$ about $x = c$.
    Let
    \begin{equation*}
        R_{n}(x) = f(x) - P_n(x).
    \end{equation*}
    Then
    \begin{equation*}
        R_{n}(x) = \frac{f^{(n+1)}(c + \theta (c-x))}{(n+1)!} (x-c)^{n+1}.
    \end{equation*}
    for some $\theta \in [0, 1]$.

    \hint{} The exact value of $\theta$ is not important. We usually just want
    an upper bound of $|R_{n}(x)|$.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \acf{ec}
            \begin{itemize}
                \item Exercise 9.4: 1-7, 10.
                \item Exercise 9.5: 1-9, 11, 13, 15, 18, 19.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[standout]

    The End!
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{bunny-classroom.png}
    \end{figure}
\end{frame}

\end{document}
