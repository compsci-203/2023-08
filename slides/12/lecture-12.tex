\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 8.5 Partition of an Integer}

\subsection{Paying for Your Sandwich with Cash}

\begin{frame}
    \frametitle{Paying for Lunch with Cash}

    You've left your \emoji{iphone} at home, and now you must pay for your \emoji{sandwich} using cash.

    How many ways can you pay \$100 using unlimited \$1, \$2, and \$5 bills?

    You may use any number of each type of bill.

    But you must pay exactly \$100.

    We will solve this with \ac{gf}.
\end{frame}

\begin{frame}
    \frametitle{Review --- Partitions of an integer (\acs{ac} 2.5)}
    For a fixed integer \(n\),
    an equation in the form of
    \[
        n = a_{1}+a_{2}+\dots + a_{k}
    \]
    such that \(a_{1} \ge a_{2} \ge \dots a_{k} \ge 1\) are integers
    is called a \alert{partition} of $m$.

    The \emoji{sandwich} example is asking, if we restrict these $a_{i}$ to be
    in $\{1, 2, 5\}$,
    in how many ways can we partition $100$?
\end{frame}

\begin{frame}
    \frametitle{Paying with \$1 bills}
    
    Let $a_{1, n}$ be the number of ways to pay \$$n$ with \$1 bills.
    What is $A_{1}(x)$, the \ac{gf} of $(a_{1, n})_{n \ge 0}$?
\end{frame}

\begin{frame}
    \frametitle{Paying with \$2 bills}

    Let $a_{2, n}$ be the number of ways to pay \$$n$ with \$2 bills.
    What is $A_{2}(x)$, the \ac{gf} of $(a_{2, n})_{n \ge 0}$?
\end{frame}

\begin{frame}
    \frametitle{Paying with \$5 bills}

    Let $a_{5, n}$ be the number of ways to pay \$$n$ with \$5 bills.
    What is $A_{5}(x)$, the \ac{gf} of $(a_{5, n})_{n \ge 0}$?
\end{frame}

\begin{frame}
    \frametitle{Paying lunch with cash}

    Let $a_{n}$ be the number of ways to pay \$$n$ with \$1, \$2, \$5 bills.

    The \ac{gf} of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            A(x) 
            &
            =
            A_{1}(x)
            A_{2}(x)
            A_{5}(x)
            \\
            &
            =
            (1+x+x^{2} + \cdots)
            (1+x^{2}+(x^{2})^{2} + \cdots)
            (1+x^{5}+(x^{5})^{2} + \cdots)
            \\
            &
            =
            \frac{1}{1-x}
            \frac{1}{1-x^{2}}
            \frac{1}{1-x^{5}}
        \end{aligned}
    \end{equation*}

    \bomb{} There is no closed form for $a_{n}$ in general.

    But it is easy to get $a_{100} = 541$ by \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5B1\%2F\%28\%281-x\%29\%281-x\%5E2\%29\%281-x\%5E5\%29\%29\%2C+\%7Bx\%2C+0\%2C+100\%7D\%5D}{WolframAlpha}.
\end{frame}

\begin{frame}
    \frametitle{Why does this \ac{gf} work?}
    
    Let's compute the coefficient of, say $x^{4}$, in the \ac{gf}
    \begin{equation*}
        \begin{aligned}
            A(x) 
            &
            =
            A_{1}(x)
            A_{2}(x)
            A_{5}(x)
            \\
            &
            =
            {\color{red} (1+x+x^{2} + \cdots)}
            {\color{blue} (1+x^{2}+(x^{2})^{2} + \cdots)}
            {\color{orange} (1+x^{5}+(x^{5})^{2} + \cdots)}
            \\
            &
            =
            \dots 
            + 
            \sum_{\substack{k_{1}, k_2, k_{5} \in \dsN:\\ k_{1} + 2 k_2 + 5 k_{5} = 4}}
            {\color{red} x^{k_{1}}}
            {\color{blue} x^{2 k_{2}}}
            {\color{orange} x^{5 k_{5}}}
            + 
            \dots 
        \end{aligned}
    \end{equation*}
    We can interpret 
    \begin{equation*}
        {\color{red} x^{k_{1}}}
        {\color{blue} x^{2 k_{2}}}
        {\color{orange} x^{5 k_{5}}}
    \end{equation*}
    as paying \$4 with $k_{1}$ \$1 bills, $k_{2}$ \$2 bills, and $k_{5}$ \$5 bills.
\end{frame}

\begin{frame}
    \frametitle{Paying with \$$m$ bills}
    
    Let $a_{m, n}$ be the number of ways to pay \$$n$ with \$$m$ bills for some
    integer $m > 0$.

    The \ac{gf} $A_{m}(x)$ of $(a_{m, n})_{n \ge 0}$ is
    \begin{equation*}
        1 + x^{m} + x^{2 m} + x^{3 m} + \cdots = \frac{1}{1-x^{m}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Paying lunch with more cash}

    Let \( P_{n} \) be the number of ways to pay \$\(n\) using bills with
    positive integer denominations, i.e., \$1, \$2, \$3, \dots.

    The \ac{gf} of $(P_{n})_{n \ge 0}$ is
    \begin{equation*}
        P(x) 
        =
        \sum_{n=1}^{\infty} P_{n} x^{n}
        =
        \prod_{m=1}^{\infty}
        A_{m}(x)
        =
        \prod_{m=1}^{\infty}
        \frac{1}{1-x^{m}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
Suppose we aim to find the exact value of \( P_{10} \).

One approach is to extract the coefficient of \( x^{10} \) from the infinite product
\[
    P(x) = \prod_{m=1}^{\infty} \frac{1}{1-x^{m}},
\]
using \href{https://www.wolframalpha.com/input?i=SeriesCoefficient\%5B\%5Cprod_\%7Bm\%3D1\%7D\%5E\%7B\%5Cinfty\%7D+\%5Cfrac\%7B1\%7D\%7B1-x\%5E\%7Bm\%7D\%7D\%2C+\%7Bx\%2C+0\%2C+10\%7D\%5D}{WolframAlpha}.

Interestingly, we can also find the same coefficient from the truncated product
\[
    Q(x) = \prod_{m=1}^{10} \frac{1}{1-x^{m}},
\]
using \href{https://www.wolframalpha.com/input?i=SeriesCoefficient\%5B\%5Cprod_\%7Bm\%3D1\%7D\%5E\%7B10\%7D+\%5Cfrac\%7B1\%7D\%7B1-x\%5E\%7Bm\%7D\%7D\%2C+\%7Bx\%2C+0\%2C+10\%7D\%5D}{WolframAlpha}.

Why do both methods yield the same result?
\end{frame}

\subsection{Paying for Your Sandwich with Odd Denominations}

\begin{frame}
    \frametitle{Paying with bills of odd denominations \emoji{dollar}}

    Let \( o_{n} \) denote the number of ways to pay \$\(n\) using bills with odd denominations, i.e., \$1, \$3, \$5, \ldots.

    \cake{} So, what is the generating function \( O(x) \) for the sequence \( (o_{n})_{n \ge 0} \)?
    \[
        O(x) = \sum_{n=0}^{\infty} o_{n} x^{n} = \prod_{m=1}^{\infty} \frac{1}{1-x^{2m-1}}
    \]

    According to the \href{https://oeis.org/A000009}{OEIS}, the initial values for \( o_{n} \) are
    \[
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \]
\end{frame}

\begin{frame}
    \frametitle{Paying with bills of distinct values \emoji{dollar}}

    Let \( d_{n} \) denote the number of ways to pay \$\(n\) using at most one of each type of bill: \$1, \$2, \$3, \ldots.

    The generating function \( D(x) \) for the sequence \( (d_{n})_{n \ge 0} \) is
    \[
        D(x) = \sum_{n=0}^{\infty} d_{n} x^{n} = \prod_{m=1}^{\infty} (1+x^{m})
    \]

    According to the \href{https://oeis.org/A000009}{OEIS}, the initial values for \( d_{n} \) are
    \[
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \]

    \cake{} Do you see something strange?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.16 --- a surprise awaits}

    For each \( n \ge 1 \), $d_n = o_n$, i.e., the number of partitions of \( n \) into distinct
    parts is the same as the number of partitions of \( n \) into odd parts.

    The proof is by showing \( O(x) = D(x) \).

    \zany{}
    For a more intricate combinatorial proof, 
    consult this
    \href{https://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/syl84.html}{source}.
\end{frame}

\begin{frame}[fragile]
    \frametitle{\tps{}}

    Let $b_{n}$ be the number of ways to pay \$n with \$1 and \$2 bills.
    Then $b_{1} = 1$ and $b_{2} = 2$.

    Find a recursive formula for $b_{n}$ in the form of
    \begin{equation*}
        b_{n} = \blankshort{}, \qquad (\text{for all $n \ge 3$}).
    \end{equation*}

    \puzzle{} Show that
    \begin{equation*}
        b_{n} = \ceil*{\frac{n+1}{2}}, \qquad n \in \dsN
    \end{equation*}
    with \ac{gf} after the class.
\end{frame}

\section{\acs{ac} 8.6 Exponential Generating Function}

\subsection{What is \ac{egf}?}

\begin{frame}{Understanding \acf{egf}}
    Given an infinite sequence \((a_{n})_{n \ge 0} = (a_{0}, a_{1}, \dots)\), we
    can associate this sequence with a \ac{gf} \( F(x) \), defined as 
    \[
        F(x) = \sum_{n \ge 0} \frac{a_{n}}{n!} x^{n}.
    \]
    This \ac{gf} is known as the \alert{\acf{egf}} of \((a_{n})_{n \ge 0}\).

    In simpler terms, the \ac{egf} of \( (a_{n})_{n \ge 0} \) is just the \ac{gf} of
    \( (\frac{a_{n}}{n!})_{n \ge 0} \).

    To distinguish the \ac{egf} from \ac{gf} of \( (a_{n})_{n \ge 0} \), 
    some authors call the latter \alert{\ac{ogf}}.
\end{frame}

\subsection{\acp{egf} and Strings}

\begin{frame}
    \frametitle{Strings consisting of one letter}
    
    Let $a_{n}$ be the number of strings of length $n$ consisting of only the letter
    \emoji{apple}.

    The \acf{egf} of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = \sum_{n = 0}^{\infty} \frac{a_{n}}{n!} x^{n} = 1 + x + \frac{x^{2}}{2!} +
        \frac{x^{3}}{3!} + \cdots = e^{x}.
    \end{equation*}

    \pause{}

    Let $b_{n}$ be the number of strings of length $n$ consisting of only the letter
    \emoji{banana}.

    The \acf{egf} of $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = \sum_{n = 0}^{\infty} \frac{b_{n}}{n!} x^{n} = 1 + x + \frac{x^{2}}{2!} +
        \frac{x^{3}}{3!} + \cdots = e^{x}.
    \end{equation*}

    \hint{} Note that $A(x) = B(x)$.
\end{frame}

\begin{frame}
    \frametitle{Strings consisting of two letters}
    
    Let $c_{n}$ be the number of strings of length $n$ consisting of either the
    letter \emoji{apple} or the letter \emoji{banana}.

    The \acf{egf} of $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) = \sum_{n = 0}^{\infty} \frac{c_{n}}{n!} x^{n} = 1 + (2 x) + \frac{(2 x)^{2}}{2!} +
        \frac{(2 x)^{3}}{3!} + \cdots = e^{2 x}.
    \end{equation*}
    Note that
    \begin{equation*}
        \begin{aligned}
            A(x) B(x) = e^{x} e^{x} = e^{2 x} = C(x).
        \end{aligned}
    \end{equation*}

    \cool{} There is no coincidence in this class.
\end{frame}

\begin{frame}
    \frametitle{Mixing two types of strings}
    
    Let $a_n$ and $b_n$ be the numbers of strings of length $n$ with a alphabet
    $\scA$ and $\scB$ respectively.

    Let $c_n$ be the number of strings of length $n$ which combines these two
    types of strings.

    Let $A(x), B(x), C(x)$ be the corresponding \acp{egf}.

    Then we have
    \begin{equation*}
        C(x) = A(x) B(x).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Why does this work?}

    \think{} Let's compute the coefficient of, say \( x^{n} \), in the \ac{egf} \( A(x) \times B(x) \).
    \begin{equation*}
        \begin{aligned}
            A(x) \times B(x)
            &=
            {\color{red} \left(\sum_{n \ge 0} \frac{a_n}{n!} x^{n}\right)}
            {\color{blue} \left(\sum_{m \ge 0} \frac{b_m}{m!} x^{m}\right)}
            \\
            &=
            \cdots
            +
            \left( \sum_{k=0}^{n}  
            {\color{red} \frac{a_{k}}{k!} x^{k}}
            {\color{blue} \frac{b_{n-k}}{(n-k)!} x^{n-k}} \right)
            +
            \cdots
            \\
            &=
            \cdots
            +
            \left( \sum_{k=0}^{n}  
            {\color{red} \frac{a_{k}}{k!}}
            {\color{blue} \frac{b_{n-k}}{(n-k)!}} \right)x^{n}
            +
            \cdots
            \\
            &=
            \cdots
            +
                \frac{1}{n!}
            \left( \sum_{k=0}^{n}  
                \binom{n}{k}
                {\color{red} a_{k}}
                {\color{blue} b_{n-k}} \right)x^{n}
            +
            \cdots
            \\
            &=
            \cdots
            +
                \frac{1}{n!}
                c_{n} x^{n}
            +
            \cdots
            \\
            &=
            C(x)
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Further explanation}
    
    In the computation on the previous slide, why do we have
    \begin{equation*}
        \left( \sum_{k=0}^{n}  
            \binom{n}{k}
            {\color{red} a_{k}}
        {\color{blue} b_{n-k}} \right)
        =
        c_{n}
    \end{equation*}
\end{frame}

\subsection{A More Interesting Example}

\begin{frame}
    \frametitle{String with even number of \emoji{cherries}}
    
    Let $d_{n}$ be  the number of strings consisting only of even number of
    \emoji{cherries}.

    The \ac{egf} of $(d_{n})_{n \ge 0}$ is
    \begin{equation*}
        D(x) 
        = \sum_{n = 0}^{\infty} \frac{d_{n}}{n!} x^{n} 
        = 1 + \frac{x^{2}}{2!} + \cdots = \frac{e^{x} + e^{-x}}{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Ternary strings with even number of \emoji{cherries}}
    
    Let $e_{n}$ be  the number of ternary strings consisting of \emoji{apple},
    \emoji{banana} and even number of \emoji{cherries}.

    The \ac{egf} of $(e_{n})_{n \ge 0}$ is
    \begin{equation*}
        E(x) 
        = \sum_{n = 0}^{\infty} \frac{e_{n}}{n!} x^{n} 
        = 
        A(x) B(x) D(x) =  e^{x} e^{x} \frac{e^{x} + e^{-x}}{2}
    \end{equation*}
    \cake{} From this, how to get
    \begin{equation*}
        e_{n} = \frac{3^{n} + 1}{2}.
    \end{equation*}

    \puzzle{} Can you prove this without \ac{egf} after the class?
\end{frame}

\subsection{Summary}

\begin{frame}
    \frametitle{\emoji{ramen} The Recipe for counting strings via \ac{egf}}

    This section deals with counting the number of strings composed of various letters—\emoji{apple}, \emoji{banana}, \emoji{cherries}, etc.—each subject to specific restrictions.

    To tackle such problems, follow these steps:
    \begin{itemize}
        \item Determine the \ac{egf} for each letter, adhering to its \emph{restrictions}.
        \item Combine these by multiplying them to obtain a new \ac{egf}.
        \item Extract the coefficient of $x^n$ in the new \ac{egf}.
        \item Multiply this coefficient by \(n!\) to get the final counts.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Given that the \ac{egf} of \( (a_{n})_{n \ge 0} \) is
    \begin{equation*}
        \frac{1}{1-x} \log \frac{1}{1-x},
    \end{equation*}
    find a formula for computing $a_{n}$.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 8.8: 17, 19, 21, 23, 25, 27.
            \end{itemize}

            \href{https://aofa.cs.princeton.edu/30gf/}{\acf{iaa}}
            \begin{itemize}
                \item[\emoji{pencil}] Exercises: 3.11.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
