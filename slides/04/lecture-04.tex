\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{dm} 3.2 Proofs}

\subsection{What is a (Mathematical) Proof?}

\begin{frame}[c]
    \frametitle{\zany{} What is a (mathematical) proof?}

    A \alert{proof} is a logical argument that establishes the truth of a
    \emph{statement}, consisting of ---

    \begin{enumerate}
        \item Selecting a set of valid \alert{deduction rules} (classical logic,
            constructive logic, etc.).
        \item Selecting a set \alert{axioms}.
        \item Selecting a set of \alert{premises}.
        \item Applying \emph{deduction rules} to these premises, 
            along with previously deduced statements, 
            to derive the \alert{conclusion} being proved.
        \item Concluding that the premises imply the conclusion.
    \end{enumerate}

    In practice, deduction rules and the axioms are implicit.
\end{frame}

\begin{frame}
    \frametitle{A classic theorem}
    
    \begin{block}{Theorem 3.2.1}
        There are infinitely many primes.
    \end{block}

    People use \emoji{robot} to find larger and larger primes.

    The \href{}{largest known prime} now is $2^{82,589,933} - 1$.

    However, only a \emph{proof} can convince us the theorem is true.
\end{frame}

\begin{frame}
    \frametitle{Euclid's Proof by contradiction}

    \begin{proof}
        \scriptsize
        \begin{enumerate}[{P}1]
            \item<+-> Suppose there are only finitely many primes. (Suppose this is
                true for now.)
            \item<+-> Let $p$ be the largest prime. (by P1)
            \item<+-> Let $N = p! + 1$. (Just a notation)
            \item<+-> $N > p$. (by P3)
            \item<+-> $N$ is not divisible by any $i \in \{2,\dots, p\}$. (by P3)
            \item<+-> $N$ has a prime factor greater than $p$. (by P5, P2)
            \item<+-> $p$ is not the largest prime. (by P6)
            \item<+-> P7 contradicts P2.
            \item<+-> P1 must be false.
            \item<+-> There must be infinitely many primes. (by P9)
        \end{enumerate}
    \end{proof}

    The $\qedsymbol$ means Q.E.D., i.e., the proof is complete.
\end{frame}

\subsection{Direct Proof}

\begin{frame}
    \frametitle{Direct proofs}
    
    A \alert{direct proof} of $P \imp Q$ is as follows ---

    \begin{quote}
        Assume $P$ (is true). Explain, explain, \dots, explain. Therefore $Q$
        (is true).
    \end{quote}

    Often we want to prove 
    $$\forall x (P(x) \imp Q(x)).$$
    In this case, we fix $x$ to be an \emph{arbitrary} element of 
    the domain of discourse (the things we are interested in).
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.2}
    
    For all integers $n$, if $n$ is even, then $n^2$ is even.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Prove that for all integers $n$, if $n$ is odd, then $n^2$ is odd.
\end{frame}

\subsection{Proof by Contrapositive}

\begin{frame}
    \frametitle{Proof by contrapositive}

    Recall that $P \imp Q$ is logically equivalent to its \alert{contrapositive} 
    $\neg Q \imp \neg P$.
    
    Thus, to show $P \imp Q$, we can use \alert{proof by contrapositive} ---

    \begin{quote}
        Assume $\neg Q$. Explain, explain, \dots explain. Therefore $\neg P$.
    \end{quote}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.4}

    If $n^2$ is even, then $n$ is even.

    \hint{} Together with Example 3.2.2, we have $n$ is even if and only if
    $n^2$ is even.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.5}
    
    Prove by contrapositive that for all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or $b$ is odd.
\end{frame}

%\begin{frame}[t]
%    \frametitle{Example 3.2.6}
%    
%    For every prime number $p$, if $p \ne 2$, then $p$ is odd.
%\end{frame}

\subsection{Proof by Contradiction}

\begin{frame}
    \frametitle{Proof by contradiction}

    A statement like $C \wedge \neg C$ is called a \alert{contradiction}.

    Since contradiction is forbidden in \emph{classical logic},
    if $\neg P \imp (C \wedge \neg C)$,
    then $P$ must be true.

    Thus, to show $P$, we can use \alert{proof by contradiction}  ---
    \begin{enumerate}
        \item Assume $\neg P$. Explain, \dots explain \dots, therefore $C$.
        \item Assume $\neg P$. Explain, \dots explain \dots, therefore $\neg C$.
        \item Thus there is a \alert{contradiction} and we must have $P$.
    \end{enumerate}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.7}
    
    A \alert{rational number} is a real number that can be expressed as the quotient \( \frac{a}{b} \) of two integers, \( a \) and \( b \), with \( b \neq 0 \).

    A real number that is not rational is called \alert{irrational}.

    Prove that $\sqrt{2}$ is irrational.
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Did Hippasus drown because of $\sqrt{2}$?}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \small

            Hippasus, a Pythagorean philosopher,
            discovered the existence of irrational numbers. 

            \medskip{}

            This revelation shocked the Pythagoreans. 

            \medskip{}

            It's said that Hippasus was drowned at sea as divine punishment.

            \hfill --- From \href{https://en.wikipedia.org/wiki/Hippasus}{Wikipedia}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{Hippasus.png}
                \caption*{Hippasus (530-450 BC). By \href{https://books.google.co.uk/books?id=1urLxOK8pzkC}{Girolamo Olgiati}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.8}
    \cake{} Prove that there are no integers $x$ and $y$ such that $x^2 = 4y + 2$.
\end{frame}

%\begin{frame}
%    \frametitle{Proof by contradiction and by contrapositive}
%    
%    In some cases, we can prove $P \imp Q$ by     
%    showing that
%    \begin{enumerate}
%        \item Assume $P \wedge \neg Q$. Therefore $P$.
%        \item Assume $P \wedge \neg Q$. Explain, \dots explain \dots, therefore
%            $\neg P$.
%        \item Thus there is a \alert{contraction} and we must have $P \imp Q$.
%    \end{enumerate}
%
%    Essentially we are just proving that
%    \begin{equation*}
%        \neg Q \imp \neg P
%    \end{equation*}
%    In other words, it is the same as proof by contrapositive.
%\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.9}

    The Pigeonhole Principle: If more than $n$ \emoji{bird} fly into $n$ pigeon
    holes, then at least one pigeon hole will contain at least two \emoji{bird}.

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{proof}
                Assume that each hole has at most one \emoji{bird}.

                \vspace{3cm}
            \end{proof}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{TooManyPigeons.jpg}
                \caption*{Photo from \href{https://en.wikipedia.org/wiki/Pigeonhole\_principle}{Wikipedia}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Proof by (Counter) Example}

\begin{frame}
    \frametitle{An experiment}
    
    For $n \in [7]$, $n^2 - n + 41$ are all primes. 
 
    \begin{equation*}
        \begin{array}{ c c c c c c c c  }
            \toprule
            n & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
            \midrule
            n^2 − n + 41 & 41 & 43 & 47 & 53 & 61 & 71 & 83 \\
            \bottomrule
        \end{array}
    \end{equation*}

    \cake{} Is it true that for all $n \in \dsN$, 
    $n^{2} - n + 41$ is a prime?

    \cake{} If not, can you think of an $n$ such that 
    $n^{2} - n + 41$ is \emph{not} a prime?
\end{frame}

\begin{frame}
    \frametitle{Most of the time you cannot prove by example}
    
    \bomb{} It is almost never OK to prove $\forall x P(x)$ by examples.

    To prove that
    \begin{quote}
        \item For all $n \in \dsN$, $n^{2} - n + 41$ is prime.
    \end{quote}
    it is not enough to show it true for $n \in \{1, \dots, 7\}$.
\end{frame}

\begin{frame}
    \frametitle{But occasionally you can prove by example}
    But it is fine to use example to show $\exists x P(x)$ by one example.

    To prove that
    \begin{quote}
        \item There exists $n \in \dsN$, $n^{2} - n + 41$ is \emph{not} a prime.
    \end{quote}
    it is enough to show it is true for a specific $n$.
\end{frame}

%\begin{frame}[t]
%    \frametitle{Example 3.2.10}
%    
%    We proved, 
%    \begin{itemize}
%        \item For all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or
%    $b$ is odd.
%    \end{itemize}
%    \cake{} Is the converse true? Can you prove your answer?
%\end{frame}

%\begin{frame}
%    \frametitle{Strong law of small numbers}
%    
%    \begin{exampleblock}{A more striking example}
%        The statement
%        \begin{equation*}
%            \ceil*{ \frac{2}{2^{1/n}-1}} = \floor*{ \frac{2 n}{\log(2)} }
%        \end{equation*}
%        is true for $n$ from $1$ to $777451915729367$ but false for $777451915729368$
%    \end{exampleblock}
%
%    \pause{}
%
%    There aren't enough small numbers to meet the many demands made of them.
%
%    \hfill --- Richard K.~Guy 
%
%\end{frame}
%
%{
%\setbeamertemplate{background}[default]
%\usebackgroundtemplate{\includegraphics[width=\paperwidth]{Zeilberger.jpg}}%
%\begin{frame}[c]
%    \frametitle{A different opinion}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            Unfortunately, these examples serve as `weapons' in the propaganda efforts of
%            rigorists, fanatical, `purists', who see truth as black and white, and claim that
%            you can't guarantee the truth of a statement no matter how many special cases you
%            have checked.
%
%            \hfill --- \href{https://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/small.html}{Doron Zeilberger}
%        \end{column}
%    \end{columns}
%\end{frame}
%}

\subsection{Proof by Cases}

\begin{frame}
    \frametitle{Proof by cases}

    To show that
    \begin{equation*}
        (Q_{1} \vee Q_{2} \dots \vee Q_{n})
        \imp
        P
        ,
    \end{equation*}
    it suffices to show that
    \begin{equation*}
        (Q_1 \imp P)
        \wedge
        (Q_2 \imp P)
        \wedge
        \dots
        \wedge
        (Q_n \imp P)
        .
    \end{equation*}

    In other words, if in each possible case $P$ is true,
    then $P$ is always true.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.11}

    Prove that for any integer $n$, the number $(n^3 − n)$ is even.
\end{frame}

\begin{frame}[t]
    \frametitle{A classical example}
    
    Prove that there exist two \emph{irrational} numbers $s$ and $t$ such that
    $s^{t}$ is rational.

    \begin{proof}
        Consider two cases 
        $\sqrt{2}^{\sqrt{2}} \in \dsQ$
        and
        $\sqrt{2}^{\sqrt{2}} \notin \dsQ$.
        (One of these must be true, but we don't care which one.)

        \vspace{4cm}
    \end{proof}
\end{frame}

%\subsection{If and only if}
%
%\begin{frame}[t]
%    \frametitle{Exercise 3.2.12}
%    Show that 
%    \begin{equation*}
%        x y = \frac{{(x + y)^2}}{4}
%    \end{equation*}
%    if and only if $x = y$.
%\end{frame}

%\begin{frame}[t]
%    \frametitle{Strangers and friends}
%    
%    \begin{theorem}
%        In any group of 6 people, there are either 3 mutual friends (every pair knows each other) or 3
%        mutual strangers (no pair knows each other).
%    \end{theorem}
%\end{frame}

%\subsection{Is this a correct proof?}
%
%\begin{frame}[t]
%    \frametitle{Which proof is correct?}
%
%    \begin{block}{Statement}
%        If $ab$ is an even number, then $a$ or $b$ is even.
%    \end{block}
%
%    \begin{proof}
%    \begin{figure}[htpb]
%        \centering
%        \begin{overlayarea}{\textwidth}{5cm}
%        \includegraphics<1>[width=\linewidth]{proof1.png}
%        \includegraphics<2>[width=\linewidth]{proof2.png}
%        \includegraphics<3>[width=\linewidth]{proof3.png}
%        \includegraphics<4>[width=\linewidth]{proof4.png}
%        \end{overlayarea}
%        \vspace{-1em}
%    \end{figure}
%    \end{proof}
%\end{frame}

%\subsection{Put It Together}
%
%\begin{frame}
%    \frametitle{Another Proof of Infinitely Many Primes}
%
%    Consider Fermat numbers $F_n = 2^{2^{n}} + 1$ for $n \in \dsN$.
%
%    By induction (we will talk about it later)
%    \begin{equation*}
%        \prod_{k=0}^{n-1} F_{k} = F_{n} - 2 \qquad (n \ge 1).
%    \end{equation*}
%
%    \think{} There for when $n \ne k$,
%    \begin{equation*}
%        \gcd(F_{n}, F_{k}) = 1.
%    \end{equation*}
%
%    \think{} So there are infinitely many primes.
%    
%    \emoji{eye} Proof \href{https://dannysiublog.com/blog/using-fermat-number-to-prove-the-infinity-of-primes/}{link}.
%\end{frame}

%\begin{frame}
%    \frametitle{\zany{} Romeo and Juliet}
%    Consider the following proof --
%    \begin{itemize}
%        \item Everyone loves a lover; Romeo loves Juliet; so everyone loves Juliet.
%    \end{itemize}
%    How to write this proof formally?
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-04.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 3.2: 1-7, 9, 10, 13.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
