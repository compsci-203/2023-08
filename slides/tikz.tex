%-----------------------------------------------------------------------------------------
% TIKZ
%-----------------------------------------------------------------------------------------
\usepackage{tikz,ifthen}
\usetikzlibrary{shapes}

\usepackage{pgfmath, pgffor}

\usetikzlibrary{backgrounds, calc, patterns}

\tikzstyle{latticenode}=[
    shape=circle,
    fill=orange, 
    draw=black,
    minimum size=4pt,
    inner sep=0pt,
    ] 

\tikzstyle{latticegrid}=[semithick, blue]
\tikzstyle{latticediagonal}=[semithick, green]
\tikzstyle{latticepath}=[line width=2pt, red]

% For graphs
\tikzstyle{graphnode}=[
    circle,
    draw=black,
    fill=JuliaOrange, 
    ultra thick,
    minimum height = 0.8cm,
    minimum width = 0.8cm,
]

\tikzstyle{graphannotation}=[
    fill=none,
    rectangle,
    draw=black,
    thin,
    align=center
]

\tikzstyle{graphlabel}=[
    fill=white,
    rectangle,
    draw=none,
]

\tikzstyle{graphedge}=[
    draw=TrolleyGrey,
    ultra thick,
]

\tikzstyle{grapharrow}=[
    draw=TrolleyGrey,
    thin,
    ->,
    dashed,
]

\tikzstyle{graphnodesmall}=[
    circle,
    draw=black,
    fill=JuliaOrange, 
    ultra thick,
    minimum height = 0.4cm,
    minimum width = 0.4cm,
]

\newcommand{\triinline}[1]{%
    \tikz[scale=0.3]{\draw[#1, thick] (-1,-1) -- (0, 0) -- (1,-1) -- cycle;}
}

\newcommand{\drawgrid}[4]{%
    \foreach \i in {1,...,#1} {
        \foreach \j in {1,...,#2} {
            \pgfmathparse{int(mod(\i+\j,2))}
            \ifnum\pgfmathresult=0\relax
                \fill[#3, draw=black] (\j,\i) rectangle ++(1,1);
            \else
                \fill[#4, draw=black] (\j,\i) rectangle ++(1,1);
            \fi
        }
    }
}

\newcommand{\drawchessboard}[2]{%
    \drawgrid{#1}{#2}{White}{pattern=north east lines}%
}

\newcommand{\drawtile}[2]{%
    \drawgrid{#1}{#2}{PastelYellow}{SeaGreen}%
    \draw[thick, red] (1,1) rectangle ++(#2,#1);
}

\newcommand{\drawLtile}{%
    \drawgrid{2}{2}{PastelYellow}{SeaGreen}%
    \fill[white, draw=white] (2,2) rectangle ++(1,1);
    \draw[thick, red] (1,1) -- (1,3) -- (2,3) -- (2,2) -- (3,2) -- (3,1) -- (1,1);
}


\newcommand{\drawFunction}[3]{
    % Calculate height and vertical shift
    \pgfmathsetmacro{\leftHeight}{#1*0.6}
    \pgfmathsetmacro{\rightHeight}{#2*0.6}
    \pgfmathsetmacro{\leftShift}{(#1-1)*0.3}
    \pgfmathsetmacro{\rightShift}{(#2-1)*0.3}

    % Draw the domain
    \node[draw, fill=PastelYellow, ellipse, minimum width=2cm, minimum height=\leftHeight cm] at (-0.5,0) {};
    \foreach \value [count=\i] in {1,...,#1} {
        \node[fill, circle, inner sep=1pt, label=left:{$\value$}] (a\i) at (-0.5,{\leftShift-(\i-1)*0.6}) {};
    }

    % Draw the codomain
    \node[draw, fill=PastelYellow, ellipse, minimum width=1.8cm, minimum height=\rightHeight cm] at (4.5,0) {};
    \foreach \value [count=\i] in {1,...,#2} {
        \node[fill, circle, inner sep=1pt, label=right:{$\value$}] (b\i) at (4.5,{\rightShift-(\i-1)*0.6}) {};
    }

    % Draw the arrows for the function
    \foreach \value [count=\i] in {#3} {
        \draw[->, thick] (a\i) -- (b\value);
    }
}

\usepackage{xparse}

\NewDocumentCommand{\drawKmn}{O{jorange,jorange} m m}{
    \begin{tikzpicture}[
        every node/.style={circle, draw},
        scale=1.0,
        ]
        
        % Extract colours
        \def\colourlist{#1}
        \foreach \x [count=\xi] in \colourlist {
            \ifnum\xi=1
                \xdef\colourA{\x}
            \else
                \xdef\colourB{\x}
            \fi
        }
        
        % Calculate loop limits and vertical offsets
        \pgfmathtruncatemacro{\maxA}{#2 - 1}
        \pgfmathtruncatemacro{\maxB}{#3 - 1}
        \pgfmathsetmacro{\offsetA}{(#2 - 1) / 2}
        \pgfmathsetmacro{\offsetB}{(#3 - 1) / 2}
        
        % Vertices in the first set
        \foreach \i in {0,...,\maxA} {
            \pgfmathtruncatemacro{\labelA}{\i + 1}
            \node[fill=\colourA, label=left:{$v_\labelA$}] (A\i) at (0, \i - \offsetA) {};
        }
        
        % Vertices in the second set
        \foreach \i in {0,...,\maxB} {
            \pgfmathtruncatemacro{\labelB}{\i + #2 + 1}
            \node[fill=\colourB, label=right:{$v_\labelB$}] (B\i) at (2, \i - \offsetB) {};
        }
        
        % Edges
        \foreach \i in {0,...,\maxA} {
            \foreach \j in {0,...,\maxB} {
                \draw (A\i) -- (B\j);
            }
        }
    \end{tikzpicture}
}

\newcommand{\drawKn}[1]{
    \begin{tikzpicture}[
        every node/.style={graphnode},
        style={graphedge},
        scale=1.5,
        ]
        % Calculate angle increment and loop limit
        \pgfmathsetmacro{\angleInc}{360 / max(#1, 1)}
        \pgfmathtruncatemacro{\maxN}{#1 - 1}
        
        % Calculate radius, ensuring it's at least 1
        \pgfmathsetmacro{\radius}{max(#1/5, 1)}
        
        % Vertices
        \foreach \i in {0,...,\maxN} {
            \pgfmathsetmacro{\angle}{\i * \angleInc + 90}
            \pgfmathtruncatemacro{\label}{\i + 1}
            \node (\label) at (\angle:\radius) {\label};
        }
        
        % Edges
        \ifnum#1>1
            \foreach \i in {1,...,\maxN} {
                \pgfmathtruncatemacro{\startJ}{\i + 1}
                \foreach \j in {\startJ,...,#1} {
                    \draw (\i) -- (\j);
                }
            }
        \fi
    \end{tikzpicture}
}

\newcommand{\drawCn}[1]{
    \begin{tikzpicture}[
        every node/.style={graphnode},
        style={graphedge},
        scale=1.5,
        ]
        % Calculate angle increment and loop limit
        \pgfmathsetmacro{\angleInc}{360 / max(#1, 1)}
        \pgfmathtruncatemacro{\maxN}{#1 - 1}
        
        % Calculate radius, ensuring it's at least 1
        \pgfmathsetmacro{\radius}{max(#1/5, 1)}
        
        % Vertices
        \foreach \i in {0,...,\maxN} {
            \pgfmathsetmacro{\angle}{\i * \angleInc + 90}
            \pgfmathtruncatemacro{\label}{\i + 1}
            \node (\label) at (\angle:\radius) {\label};
        }
        
        % Edges
        \ifnum#1>1
            \foreach \i in {1,...,#1} {
                \pgfmathtruncatemacro{\next}{mod(\i, #1) + 1}
                \draw (\i) -- (\next);
            }
        \fi
    \end{tikzpicture}
}

\newcommand{\drawComplementOfCn}[1]{
    \begin{tikzpicture}[
        every node/.style={graphnode},
        style={graphedge},
        scale=1.5,
        ]
        % Calculate angle increment and loop limit
        \pgfmathsetmacro{\angleInc}{360 / max(#1, 1)}
        \pgfmathtruncatemacro{\maxN}{#1 - 1}
        
        % Calculate radius, ensuring it's at least 1
        \pgfmathsetmacro{\radius}{max(#1/5, 1)}
        
        % Vertices
        \foreach \i in {0,...,\maxN} {
            \pgfmathsetmacro{\angle}{\i * \angleInc + 90}
            \pgfmathtruncatemacro{\label}{\i + 1}
            \node (\label) at (\angle:\radius) {\label};
        }
        
        % Edges
        \ifnum#1>2
            \foreach \i in {1,...,#1} {
                \foreach \j in {1,...,#1} {
                    \ifnum\i<\j
                        \pgfmathtruncatemacro{\next}{mod(\i, #1) + 1}
                        \pgfmathtruncatemacro{\prev}{mod(\i - 2 + #1, #1) + 1}
                        \ifnum\j=\next
                        \else
                            \ifnum\j=\prev
                            \else
                                \draw (\i) -- (\j);
                            \fi
                        \fi
                    \fi
                }
            }
        \fi
    \end{tikzpicture}
}
