\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 8.2 Another look at distributing carrots}

\subsection{A Fruit Basket Problem}

\begin{frame}
    \frametitle{Fruit baskets}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{basket.jpg}
        \caption*{A fruit basket is a collection of fruit in a \emoji{basket}.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 8.5}

    A grocery store is preparing holiday fruit baskets for sale. 

    Each fruit basket will have $n$ pieces of fruit in it, chosen from \emoji{apple}, \emoji{banana}, \emoji{cherries}, and \emoji{dumpling}.

    How many different ways can such a basket be prepared if 
    \begin{itemize}
        \item there must be at least one \emoji{apple} in a basket, 
        \item a basket cannot contain more than three \emoji{banana},
        \item and the number of \emoji{cherries} must be a multiple of four.
    \end{itemize}

\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{apple}}

    Let $a_{n}$ be the number of ways to put in $n$ \emoji{apple} in the basket so that we have at least one \emoji{apple}.  

    The \ac{gf} for $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = x + x^{2} + x^{3} + \cdots = \frac{x}{1-x}
    \end{equation*}
\end{frame}


\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{banana}}

    Let $b_{n}$ be the number of ways to put in $n$ \emoji{banana} in the basket so that we have at most three \emoji{banana}. 

    The \ac{gf} for $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = 1 + x + x^{2} + x^{3}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{cherries}}

    Let $c_{n}$ be the number of ways to put in $n$ \emoji{cherries} in the basket so that we have a multiple of four \emoji{cherries}. 

    The \ac{gf} for $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) = 1 + x^{4} + x^{8} + \cdots = \frac{1}{1-x^{4}}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{dumpling}}

    Let $d_{n}$ be the number ways to put in $n$ \emoji{dumpling} in the basket.

    The \ac{gf} for $(d_{n})_{n \ge 0}$ is
        \begin{equation*}
            D(x) = 1 + x + x^{2} + \cdots = \frac{1}{1-x}
        \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{The \ac{gf} for fruit baskets}

    Let $e_{n}$ be the number of ways to put $n$ fruits in the basket so \emph{all}
    conditions are satisfied.

    The \ac{gf} for $(e_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            \sum_{n = 0}^{\infty} e_{n} x^{n} 
            %&
            %=
            %\sum_{n = 0}^{\infty}
            %\sum_{\substack{k_{1} k_{2} k_{3} k_{4}:\\
            %k_{1} + k_{2} + k_{3} + k_{4} = n}}
            %a_{k_{1}}
            %b_{k_{2}}
            %c_{k_{3}}
            %d_{k_{4}}
            %x^{n}
            %\\
            &
            =
            A(x) B(x) C(x) D(x)
            \\
            &
            =
            \frac{x}{1-x}
            (1 + x + x^{2} + x^{3})
            \frac{1}{1-x^{4}}
            \frac{1}{1-x}
            \\
            &
            =
            \sum_{n = 1}
            \binom{n+1}{2}
            x^{n}
        \end{aligned}
    \end{equation*}

    The last step follows either by either by \emoji{pencil} or by
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}.
\end{frame}

\subsection{Partial Fractions}

\begin{frame}
    \frametitle{Example 8.6}
    
    Find the number of integer solutions to the equation
    \begin{equation*}
        x_1 + x_2 + x_3 = n, \qquad (n \in \dsN),
    \end{equation*}
    with $x_1 \ge 0$ being even, $x_2 \ge 0$, and $2 \ge x_3 \ge 0$.

    \begin{block}{Solution with \ac{gf}}
        \only<1>{
            Let $f_n$ be the number of such solutions. 
            The \ac{gf} for $(f_n)_{n \ge 0}$ is
            \begin{equation*}
                F(x) 
                = \frac{1+x+x^2}{(1-x)(1-x^2)}
                = \frac{1+x+x^2}{(1+x)(1-x)^2}
            \end{equation*}
        }
        \only<2>{
            With the help of
            \href{https://www.wolframalpha.com/input?i=SeriesCoefficient\%5B\%281\%2Bx\%2Bx\%5E2\%29\%2F\%28\%281-x\%29\%281-x\%5E2\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}, we get
            \begin{equation*}
                f_n = 
                    \frac{1}{4} (6n + (-1)^n + 3), \qquad n \in \dsN.
            \end{equation*}
            How can we do this with only \emoji{pencil}?
        }
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{\zany{} Partial fraction decomposition}
    
    A \alert{rational function} is any function of the form $\frac{f(x)}{g(x)}$
    where $f(x)$ and $g(x)$ are polynomials.

    We can \emph{always} express a rational function in the form
    \begin{equation*}
        \frac{f(x)}{g(x)} = p(x) + \sum_{j} \frac{f_j(x)}{g_j(x)}
    \end{equation*}
    where 
    \begin{itemize}
        \item $p(x)$ is a polynomial that may be zero, 
        \item and $f_j(x)$ and $g_j(x)$ are polynomials with the degree of $f_j(x)$ less than the degree of $g_j(x)$, 
        \item and each $g_j(x)$ is an \alert{irreducible polynomial} .
    \end{itemize}

    This is called \alert{partial fraction decomposition}.
\end{frame}

\begin{frame}
    \frametitle{Partial fraction decomposition --- an example}

    For example, we can always write
    \begin{equation*}
        F(x) 
        = \frac{1+x+x^2}{(1+x)(1-x)^2}
        =
        \frac{A}{1+x}
        +
        \frac{B}{1-x}
        +
        \frac{C}{(1-x)^2}
        ,
    \end{equation*}
    which implies (\href{https://www.wolframalpha.com/input?i=Apart\%5B\%281\%2Bx\%2Bx\%5E2\%29\%2F\%28\%281\%2Bx\%29\%281-x\%29\%5E2\%29\%5D}{WolframAlpha})
    \begin{equation*}
        A = \frac{1}{4}, \qquad B = -\frac{3}{4}, \qquad C = \frac{3}{2}.
    \end{equation*}
    Finding $A$, $B$, $C$ allows us to extract the coefficients of $x^n$ in
    $F(x)$ easily.
\end{frame}

\subsection{Summary}

\begin{frame}
    \frametitle{\emoji{ramen} The Recipe for Counting Integer Compositions via \ac{gf}}

    The fruit basket problem can be framed as finding the number of integer solutions to
    \begin{equation*}
        x_{1} + x_{2} + \cdots + x_{k} = n,
    \end{equation*}
    where \( x_{1}, x_{2}, \ldots, x_{k} \) each satisfies certain restrictions.

    To solve problems of this nature, follow these steps:
    \begin{itemize}
        \item Determine the \ac{gf} for each variable (representing a fruit), in accordance with its \emph{restrictions}.
        \item Combine these by multiplying them to form a new \ac{gf}.
        \item Extract the coefficients from this new \ac{gf}.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Consider the equality
    \begin{equation*}
        x_1 + x_2 + x_{3} = n
    \end{equation*}
    where $x_1, x_{2}, x_3, n \ge 0$ are all integers, and
    \begin{itemize}
        \item $x_1 \ge 2$
        \item $x_2$ is a multiple of 5, 
        \item and $0 \le x_3 \le 4$. 
    \end{itemize}

    Let $d_n$ be the number of solutions.
    Can you find the \ac{gf} for $(d_n)_{n \ge 0}$?

    \puzzle{} Try to find a closed formula for $d_n$ after the class.
\end{frame}

\section{\acs{ac} 8.4 Newton's Binomial Theorem}

\begin{frame}
    \frametitle{A combinatorial identity}
    
    It is not obvious how to find a combinatorial proof that for
    \begin{equation*}
        2^{2n} = \sum_{k = 0}^{n} \binom{2k}{k} \binom{2n-2k}{n-k},
        \qquad
        (\text{for all $n \in \dsN$})
        .
    \end{equation*}
    We will derive this with \ac{gf}.

    \puzzle{} Try find a combinatorial proof after class.
\end{frame}

\begin{frame}
    \frametitle{Review --- Permutations}
    
    We have defined $P(p, k)$ for integers $p$ and $k$ with $p \ge k \ge 0$ 
    as the number of strings of $k$ distinct letters chosen from
    an $p$-alphabet.

    In other words,
    \begin{equation*}
        P(p, k) = \blankshort{}
    \end{equation*}
    Now let's extent this definition.
\end{frame}

\begin{frame}
    \frametitle{Definition 8.8}
    For all $p \in \dsR$ and $k \in \dsN$, 
    the number $P(p, k)$
    is defined by
    \begin{equation*}
        P(p, k) = 
        \begin{cases}
            1 & \text{if $k = 0$,} \\
            p P(p − 1, k − 1) & \text{if $k \ge 1$},
        \end{cases}
    \end{equation*}

    \only<1>{\cake{} What are $P(3, 3)$ and $P(3, 4)$?}
    \only<2>{\cake{} What are $P(m, n)$ if $m, n \in \dsN$ and $m < n$?}
    \only<3>{\cake{} What are $P(\pi, 0)$ and $P(\pi, 1)$?}
\end{frame}

\begin{frame}[t]
    \frametitle{Definition 8.9 -- Binomial coefficients}
    Now let's extend the definition of binomial coefficients.

    For all $p \in \dsR$ and $k \in \dsN$,  we define
    \[
        \binom{p}{k}
        =
        \frac{P(p,k)}{k!}
    \]

    \only<1>{\cake{} What are $\binom{3}{2}$, $\binom{3}{3}$, $\binom{3}{4}$?}
    \only<2>{\cake{} What are $\binom{\pi}{0}$, $\binom{\pi}{1}$?}
\end{frame}

\begin{frame}[t]{Newton's Binomial Theorem}
    \begin{block}{Theorem 2.30 (\acs{ac}) --- Binomial Theorem}
        For all \(p \in \dsN\) with \(p \ge 0\),
        \begin{equation*}
            (1+x)^{p} = \sum_{n = 0}^{p} \binom{p}{n} x^{n}
        \end{equation*}
        \vspace{-0.5em}
    \end{block}

    \begin{block}{Theorem 8.10 (\acs{ac}) --- Newton's Binomial Theorem}
        For all \(p \in \mathbb R\) with \(p \ne 0\),
        \begin{equation*}
            (1+x)^{p} = \sum_{n = 0}^{\infty} \binom{p}{n} x^{n}
        \end{equation*}
        \vspace{-0.5em}
    \end{block}

    \zany{} For a proof, see
    \href{https://math.libretexts.org/Bookshelves/Combinatorics_and_Discrete_Mathematics/Combinatorics_and_Graph_Theory_\%28Guichard\%29/03\%3A_Generating_Functions/3.02\%3A_Newton\%27s_Binomial_Theorem}{here}.
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Sir Isaac Newton}
    
    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            Sir Isaac Newton was an \emoji{uk} mathematician, physicist,
            \emoji{telescope}, \emoji{alembic}, \emoji{latin-cross}.

            He is credited with --
            \begin{itemize}
                \item laws of \emoji{running}
                \item law of universal \emoji{earth-asia}
                \item theory of \emoji{rainbow}
                \item computed speed of \emoji{loud-sound}
                \item binomial theorem \dots
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{Newton.jpg}
                \caption*{Sir Isaac Newton (25 December 1642 – 20 March 1726/27)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\zany{} Newton and \emoji{apple} tree}
    
    The legend says that Newton got hit by an \emoji{apple} on his head and
    begun to think about gravity? Do you think this is true?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{bunny-under-apple-tree.jpg}
    \end{figure}
\end{frame}

\subsection{8.4 An Application of the Binomial Theorem}

\begin{frame}{Lemma 8.12}
    For all integers \(k \ge 0\), 
    \vspace{-1em}
    \[
        \binom{-1/2}{k}=(-1)^{k} \frac{\binom{2 k}{k}}{2^{2 k}}. 
    \]
    \vspace{-1em}
\end{frame}

\begin{frame}{Theorem 8.13 --- A generating function}
        \[
            \frac{1}{\sqrt{1-4x}}
            =
            \sum_{n \ge 0} \binom{2n}{n} x^n
            .
        \]
        \vspace{-1em}
\end{frame}

\begin{frame}[t]{Theorem 8.13 (\acs{ac}) --- Applying the Binomial Theorem}
    \[
        \frac{1}{\sqrt{1-4x}}
        =
        \sum_{n \ge 0} \binom{2n}{n} x^n
        .
    \]

\end{frame}

\begin{frame}
    \frametitle{Corollary 8.14 --- The identity which we are looking for}
    
    For all integers \(n \ge 0\)
    \begin{equation*}
        2^{2n} = \sum_{k \ge 0} \binom{2k}{k} \binom{2n-2k}{n-k}.
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 8.8: 5, 7, 9, 11, 13, 15.
            \end{itemize}

            \emoji{bomb} Note the original solution for 8.8.11 is incorrect.
            See \texttt{applied-combinatorics-ch-08.pdf}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
