\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../language.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Basic Notation and Terminology for Graphs}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 5.1 Basic Notation and Terminology for Graphs}

\subsection{Graphs in the Real World}

\newcommand{\animals}{{"dog", "cat", "fish", "bird", "elephant", "tiger", "lion", "snake", "bear", "monkey"}}

\begin{frame}[c]
    \frametitle{Friendships}

    We can model friendships as a graph in which each vertex (node) represents a
    friend and each edge (line) represents a friendship.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{graph-1.pdf}
        \caption*{Friendships in COMPSCI 203}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Metro network as a graph}

    A metro network can be modelled as a graph. In this representation, each vertex corresponds to a station, and each edge signifies a metro line.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{metro.png}
        \caption*{Mini Metro game screenshot. Source: 
        \href{https://commons.wikimedia.org/wiki/File:Mini\_Metro\_screenshot\_0.png}{Wikipedia}.}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{RNA secondary structure as a graph}

    The secondary structure of RNA can be modelled as a graph. 
    In this graph, loops function as vertices, and stems serve as edges.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{rna.png}
        \caption*{RNA secondary structures. Source: \href{https://www.nature.com/articles/s41467-021-21194-4}{Nature}.}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{TV show characters as graphs}

    Characters in a TV show can be modelled as vertices in a graph, 
    with edges representing various relationships.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{breaking-bad.jpg}
        \caption*{Connections among Breaking Bad characters. Source: \href{https://www.reddit.com/r/breakingbad/comments/29exlh/how_all_the_characters_in_breaking_bad_are/}{Reddit}.}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{\dizzy{} Heavy dose of notations ahead!}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{bunny-confused.png}
%    \end{figure}
%\end{frame}

\subsection{Basic Notation and Terminology for Graphs}

\begin{frame}
    \frametitle{A graph}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{graph-01-4.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definition of a graph}
    
    A \alert{graph} \( G \) is defined as a pair \( (V, E) \).

    \( V \) is the \alert{vertex set}, and \( E \) is the \alert{edge set}, comprising 2-element subsets of \( V \).

    An element in \( V \) is called a \alert{vertex} or \alert{node}.

    An element in \( E \) is called an \alert{edge}.
    
    We can abbreviate an edge \( \{x, y\} \) as \( xy \).

    If \( xy \in E \), then \( x \) and \( y \) are \alert{adjacent}, and the edge \( xy \) is \alert{incident to} both \( x \) and \( y \).
\end{frame}

\begin{frame}
    \frametitle{Drawing graphs: An example}

    Let \( V = \{a, b, c, d, e\} \) and \( E = \{\{a, b\}, \{c, d\}, \{a, d\}, \{c, b\} \} \).

    \cake{} Which of the following diagrams accurately depict \( G = (V, E) \)?

    \vspace{1em}

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{\includegraphics[width=0.3\textwidth]{graph-01-1.pdf}}
        \colorbox{white}{\includegraphics[width=0.3\textwidth]{graph-01-2.pdf}}
        \colorbox{white}{\includegraphics[width=0.3\textwidth]{graph-01-3.pdf}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Neighbourhood of a vertex}

    The \alert{neighbourhood} of a vertex \( x \), denoted \( N(x) \), comprises vertices adjacent to \( x \).

    \cake{} What is the neighbourhood of vertex \( 12 \) in the graph below?

    \vspace{1em}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{graph-2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Degrees}

    The \alert{degree} of a vertex \( v \) in a graph \( G \), denoted by \( \deg_G(v) \), is the number of vertices adjacent to \( v \).

    \cake{} Which vertices have the maximum degree in the graph below?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{graph-2.pdf}
    \end{figure}

    \zany{} The Facebook graph has a maximum degree of 5,000.
\end{frame}

\begin{frame}
    \frametitle{First proof in graph theory}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-pilate.png}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.9 (\acs{ac})}

    Let $\deg_G(v)$ denote the degree of vertex $v$ in graph $G = (V, E)$. 
    Then
    \begin{equation*}
        \sum_{v \in V} \deg_{G}(v) = 2 \abs{E}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4.1.7 (\ac{dm})}

    At a recent maths seminar, 9 \teacher{} greeted each other with
    \emoji{handshake}. 

    Is it possible for each \teacher{} to have \emoji{handshake} with exactly 7 others?

    \hint{} This is to ask, if it is possible to have a graph with 9 vertices
    and each vertex has degree 7.

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[scale=0.6]
        \foreach \i in {1,2,...,9} {
            \node (T\i) at ({\i*360/9}: 3cm) {\teacher{}};
        }

        \foreach \i [evaluate=\i as \start using int(\i+1)] in {1,2,...,8} {
            \foreach \j in {\start,...,9} {
                \pgfmathparse{rand>0.6 ? int(1) : int(0)}
                \ifnum\pgfmathresult>0
                    \draw[thick, SeaGreen] (T\i) -- (T\j);
                \fi
            }
        }
    \end{tikzpicture}
    \caption*{A \emoji{handshake} graph}
\end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Corollary 5.10}
    
    For any graph, the number of vertices of odd degree is even.
\end{frame}

\subsection{Subgraphs}

\begin{frame}
    \frametitle{Subgraphs}
    
    When $G =(V, E)$ and $H =(W, F)$ are graphs, we say $H$ is 
    a \alert{subgraph} of $G$ when $W \subseteq V$ and $F \subseteq E$.

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{\includegraphics[width=0.5\linewidth]{subgraph.pdf}}
        \caption*{A subgraph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spanning subgraphs}
    
    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is a \alert{spanning subgraph} when $W = V$. 

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph.pdf}}
            \caption*{A subgraph which is \emph{not} a spanning subgraph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph-spanning.pdf}}
            \caption*{A subgraph which is also a spanning subgraph}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Induced subgraphs}

    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is called \alert{an induced subgraph}
    if $xy \in E$ and $x, y \in W$ implies that $xy \in F$.

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph.pdf}}
            \caption*{A subgraph which is \emph{not} an induced subgraph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph-induced.pdf}}
            \caption*{A subgraph which is also an induced subgraph}
        \end{subfigure}
    \end{figure}
\end{frame}

\subsection{Some common graphs}

\begin{frame}[t]
    \frametitle{Complete graph}
    A graph $G = (V, E)$ is called a \alert{complete graph} when $x y$ is an
    edge in $G$ for every distinct pair $x, y \in V$. 

    We let $K_{n}$ denote the complete graph with $n$ vertices.

    \foreach \n [count=\i from 1] in {3,4,5,6} {
        \only<\i>
        {
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.3\textwidth]{k\n.pdf}
                \caption*{$K_{\n}$}
            \end{figure}
            \cake{} How many edges do \(K_{\n}\) have?
        }
    }
    \only<4>{What about $K_{n}$?}
\end{frame}

\begin{frame}[t]
    \frametitle{Independent graph}
    A graph $G = (V, E)$ is called a \alert{independent graph} when there is no
    edge in the graph, i.e., $E = \emptyset$. 

    We let $I_{n}$ denote the independent graph with $n$ vertices.

    \vspace{1em}

    \begin{figure}[htpb]
        \centering
        \colorbox{white}{\includegraphics[width=0.3\linewidth]{i4.pdf}}
        \caption*{$I_4$}
    \end{figure}

    \cake{}  How many edges do $I_{10^{100}}$ have?
\end{frame}

\subsection{Moving Around in Graphs}

\begin{frame}
    \frametitle{Walks}

    A sequence \( (x_1, x_2, \ldots, x_n) \) is termed a \alert{walk} if \( x_i x_{i+1} \) is an edge for each \( i \in [n-1] \).

    \bomb{} A walk may revisit vertices.

    Consider the walk \( (9, 8, 3, 6, 2, 6, 1) \).

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{graph-2.pdf}
    \end{figure}

    \vspace{-1em}

    \zany{} What is the length of the longest walk in this graph?
\end{frame}

\begin{frame}
    \frametitle{Paths}

    A \alert{path} is a walk that visits each vertex \emph{at most once}.

    \cake{} Can you find the longest path in the graph?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\linewidth]{graph-2.pdf}
        \includegraphics<2>[width=0.4\linewidth]{../images/graphs/k12.pdf}
        \only<2>{\caption*{\(K_{12}\)}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cycles}

    A \alert{cycle} is a walk that starts and ends at the same vertex, visiting only distinct vertices otherwise.

    \cake{} Can you find the longest cycle in the graph?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\linewidth]{graph-2.pdf}
        \includegraphics<2>[width=0.4\linewidth]{../images/graphs/k12.pdf}
        \only<2>{\caption*{\(K_{12}\)}}
    \end{figure}
\end{frame}

\subsection{Graph Isomorphism}

\begin{frame}
    \frametitle{Graph Isomorphism}

    Given two graphs \( G = (V, E) \) and \( H = (W, F) \), 
    \( G \) is \alert{isomorphic} to \( H \) 
    if \( G \) can be relabelled to obtain \( H \).

    \vspace{1em}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{graph-isomorphic-01.pdf}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{graph-isomorphic-02.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Can you show these two graphs are isomorphic?

    \vspace{1em}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                {\includegraphics[width=0.8\linewidth]{graph-isomorphic-03.pdf}}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                {\includegraphics[width=0.8\linewidth]{graph-isomorphic-04.pdf}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\zany{} An unsolved mystery in \ac{cs}}

    The graph isomorphism problem seeks to determine if two finite graphs are isomorphic. 

    It holds a unique position:
    \begin{itemize}
        \item No known polynomial-time algorithm.
        \item Not NP-complete.
    \end{itemize}

    \think{} Can this problem be solved in polynomial time?
\end{frame}

\subsection{Connected Graphs}

\begin{frame}
    \frametitle{Connected Graphs}

    A graph \( G \) is \alert{connected} if there exists a path between any two
    vertices \( x, y \in V \). Otherwise, \( G \) is \alert{disconnected}.

    A maximal connected subgraph is called a \alert{component}.

    \think{} What are the components of the graph below?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{graph-6.pdf}
    \end{figure}
\end{frame}

\section{\acs{ac} 5.2 Multigraphs: Loops and Multiple Edges}

\begin{frame}
    \frametitle{Multigraphs}
    
    An edge with both endpoints the same is called a \alert{loop}.

    A graph contains loops or multiple edges between two vertices is calla a
    \alert{multigraph};
    otherwise it is called a \alert{simple graph}.

    \bomb{} In our course, a \emph{graph} means a \emph{simple graph} unless
    otherwise specified.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[every node/.style=graphnodesmall, every path/.style=graphedge]
            % Vertices
            \node (1) at (0,0) {1};
            \node (2) at (2,0) {2};
            \node (3) at (3,1.5) {3};
            \node (4) at (1,2) {4};
            \node (5) at (-1,1.5) {5};

            % Edges
            \draw (1) -- (2);
            \draw (2) -- (3);
            \draw (2) to [out=90, in=180] (3); % Second edge for the "double" edge
            \draw (3) -- (4);
            \draw (4) -- (1);
            \draw (4) -- (5);
            \draw (1) to [out=90,in=180,looseness=8] (1); % Loop at 1
        \end{tikzpicture}
        \caption*{A multigraph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{When to use multigraphs}
    
    Suitable scenarios for multigraph modeling include:
    \begin{itemize}
        \item[\emoji{pencil}] Academic collaboration networks
        \item[\emoji{tv}] TV production collaboration
        \item[\emoji{car}] Transportation systems
        \item[\emoji{computer}] Telecommunication networks
        \item[\emoji{dna}] Bioinformatics interactions
        \item[\emoji{money-with-wings}] Financial markets
    \end{itemize}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 1, 3, 5, 7.
            \end{itemize}

            \href{http://discrete.openmathbooks.org/dmoi3}{\acf{dm}}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 4.1: 1--4, 6--11,  13--16.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
