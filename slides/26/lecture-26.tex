\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Test of Convergence}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acf{ec} 9.2 Test of Convergence}

\begin{frame}
    \frametitle{Impact of Finite Changes on Limits}
    
    Let \( \lim_{{n \to \infty}} a_n = M \).
    
    If \( (a_n')_{n \in \mathbb{N}} \) differs from \( (a_n)_{n \in \mathbb{N}} \) 
    by finitely many terms, then \( \lim_{{n \to \infty}} a_n' = M \).
    
    In other words, if there exists an \( N \) such that 
    \[
        a_n' = a_n, \quad \forall n \geq N,
    \]
    then
    \begin{equation*}
        \lim_{{n \to \infty}} a_n' = \lim_{{n \to \infty}} a_n = M.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example}
    
    Consider the sequence
    \begin{equation*}
        a_n =
        \begin{cases}
            e^n, & n \le 10^{9}, \\
            2^{-n}, & n > 10^{9}.
        \end{cases}
    \end{equation*}

    \cake{} What is $\lim_{n \to \infty} a_n$?
\end{frame}

\begin{frame}
    \frametitle{Bounded Sequence}

    \only<1>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded from
        above} if there exists $M \in \mathbb{R}$ such that
        \begin{equation*}
            a_n \leq M, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<2>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded from
        below} if there exists $N \in \mathbb{R}$ such that
        \begin{equation*}
            a_n \geq N, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<3>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded}
        if it is both bounded from above and from below.
    }
\end{frame}

\begin{frame}
    \frametitle{Example: Bounded Sequence}

    \cake{} Is $(2^{-n})_{n \in \mathbb{N}}$ bounded from above or below?

    \vfill{}

    \cake{} Is $(2^{n})_{n \in \mathbb{N}}$ bounded from above or below?

    \vfill{}

    \cake{} Can you think of a sequence that is both bounded from above and
    below, i.e., bounded?

    \vfill{}

    \cake{} Can you think of a sequence that is neither bounded from above nor
    below?
\end{frame}

\begin{frame}
    \frametitle{Monotone Sequence}

    \only<1>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{monotone
        increasing} if
        \begin{equation*}
            a_n \leq a_{n+1}, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }

    \only<2>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{monotone
        decreasing} if
        \begin{equation*}
            a_n \geq a_{n+1}, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }

    \only<3>{
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{monotone}
        if it is either monotone increasing or monotone decreasing.
    }
\end{frame}

\begin{frame}
    \frametitle{Example: Monotone Sequence}
    
    \cake{} Is $(2^{n})_{n \in \mathbb{N}}$ monotone increasing or decreasing?

    \vfill{}

    \cake{} Is $\left(\frac{1}{{n}}\right)_{n \ge 1}$ monotone increasing or decreasing?

    \vfill{}

    \cake{} Can you think of a sequence that is both monotone increasing and monotone decreasing?

    \vfill{}

    \cake{} Can you think of a sequence that is neither monotone increasing nor monotone decreasing?
\end{frame}

\begin{frame}
    \frametitle{Monotone Bounded Test}

    If a sequence $(a_n)_{n \in \mathbb{N}}$ is \alert{monotone
    decreasing} and \alert{bounded from below}, then it is convergent.

    If a sequence $(a_n)_{n \in \mathbb{N}}$ is \alert{monotone
    increasing} and \alert{bounded from above}, then it is convergent.

    A simple example is $(2^{-n})_{n \in \mathbb{N}}$.

    \begin{figure}[htpb]
        \colorbox{white}{
            \begin{tikzpicture}[scale=0.9]
                \begin{axis}[
                    width=4in,
                    height=2in,
                    axis lines=middle,
                    xmin=-1, xmax=11,
                    ymin=-0.1, ymax=1.1,
                    xlabel={$n$},
                    ylabel={$2^{-n}$},
                    xtick={0,5,10},
                    ytick={0,0.2,0.4,0.6,0.8,1},
                    grid=major,
                    ]
                    \addplot[only marks, mark=*, mark options={fill=red, draw=black}, samples at={0,1,...,10}] {pow(2,-x)};
                    \addplot[dashed, blue, domain=-1:11] {0};
                \end{axis}
            \end{tikzpicture}
        }
        \caption*{Example: \(2^{-n} \to 0\)}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 7.9}

    Show the sequence
    \begin{equation*}
        a_n =
        \frac{1 \cdot 3 \cdot 5 \dots (2n-1)}{2 \cdot 4 \cdot 6 \dots 2n},
        \qquad
        n \ge 1,
    \end{equation*}
    is convergent.

    \pause{}

    \puzzle{} Can you show that $\lim_{n \to \infty} a_n = 0$?

    \hint{} Show that
    \begin{equation*}
        a_n = \cfrac{\binom{2n}{n}}{2^{2n}}
    \end{equation*}
    and use Stirling's approximation.
\end{frame}

\begin{frame}
    \frametitle{Comparison Test}
    
    \only<1>{
        If \( a_n \to \infty \) and
        \begin{equation*}
            a_n \leq b_n, \quad \forall n \in \mathbb{N}
        \end{equation*}
        then
        \( b_n \to \infty \).

        \cake{} Can you think of such two sequences?
    }
    \only<2>{
        If \( b_n \to -\infty \) and
        \begin{equation*}
            a_n \leq b_n, \quad \forall n \in \mathbb{N}
        \end{equation*}
        then
        \( a_n \to -\infty \).

        \cake{} Can you think of such two sequences?
    }
\end{frame}

\begin{frame}
    \frametitle{$n$-th Term Test}
    
    If $\sum_{n=0}^{\infty} a_n$ converges, then $\lim_{n \to \infty} a_n = 0$.

    Thus if 
    \begin{itemize}
        \item $\lim_{n \to \infty} a_n$ does not exist,
        \item or $\lim_{n \to \infty} a_n \ne 0$,
    \end{itemize}
    then $\sum_{n=0}^{\infty} a_n$ diverges.

    \cake{} Does this test tell us if
    \begin{equation*}
        \sum_{n=1}^{\infty} \only<1>{n}\only<2>{\frac{1}{n}}
    \end{equation*}
    converge?
\end{frame}

\begin{frame}
    \frametitle{Ratio Test}
    
    For a series $\sum_{n=0}^{\infty} a_n$,
    let
    \begin{equation*}
        R = \lim_{n \to \infty} \frac{a_{n+1}}{a_n}
    \end{equation*}
    Then
    \begin{itemize}
        \item if $R > 1$, then $\sum_{n=0}^{\infty} a_n$ diverges;
        \item if $R < 1$, then $\sum_{n=0}^{\infty} a_n$ converges;
        \item if $R = 1$ or $R$ does not exist, then $\sum_{n=0}^{\infty} a_n$
            may or may not converge.
    \end{itemize}

    \cake{} Does this test tell us if
    \begin{equation*}
    \sum_{n=1}^{\infty} \only<1>{2^n}\only<2>{2^{-n}}\only<3>{\frac{1}{n}}
    \end{equation*}
    converge?
\end{frame}

\begin{frame}
    \frametitle{Integral Test}
    
    Let $(a_n)_{n \in \mathbb{N}}$ be a sequence of positive terms.

    Let $f(x)$ be a decreasing function on $[1, \infty)$
    such that $f(n)=a_n$ for all $n \in \mathbb{N}$.
    Then
    \begin{equation*}
        \sum_{n=1}^{\infty} a_n 
        \qquad
        \text{and}
        \qquad
        \int_{1}^{\infty} f(x) \, \dd{x}
    \end{equation*}
    both converge or diverge.

    \cake{} Does this test tell us if
    \begin{equation*}
    \sum_{n=1}^{\infty} \only<1>{\frac{1}{n^{2}}}\only<2>{\frac{1}{n^{1/2}}}\only<3>{\frac{1}{n}}
    \end{equation*}
    converge?
\end{frame}

\begin{frame}
    \frametitle{$p$-series Test}
    
    The series $\sum_{n=1}^{\infty} p^{-n}$ converges if and only if $p > 1$.

    \cake{} Which of the series below converge?
    \begin{equation*}
        \sum_{n=1}^{\infty} 
        \frac{1}{n^{2}},
        \qquad
        \sum_{n=1}^{\infty} 
        \frac{1}{n^{1/2}},
        \qquad
        \sum_{n=1}^{\infty} 
        \frac{1}{n}.
    \end{equation*}
\end{frame}

\begin{frame}{Comparison Test for Convergence}
    % Given Series
    Consider two series \( \sum a_n \) and \( \sum b_n \) such that \( 0 \le a_n \le b_n \) for all \( n > N \).

    \only<1>{
        % Implication for Convergence
        If \( \sum b_n \) converges, then \( \sum a_n \) also converges.

        % An example here
        Example: 
        \begin{equation*}
            \sum_{n=1}^{\infty} \frac{1}{n(n+1)},
            \qquad
            \sum_{n=1}^{\infty} \frac{1}{n^{2}}.
        \end{equation*}
    }

    \only<2>{
        % Implication for Divergence
        If \( \sum a_n \) diverges, then \( \sum b_n \) also diverges.

        % An example here
        Example: 
        \begin{equation*}
            \sum_{n=1}^{\infty} \frac{1}{n},
            \qquad
            \sum_{n=1}^{\infty} \frac{1}{n-10^{-n}}.
        \end{equation*}
    }
\end{frame}

\begin{frame}{Limit Comparison Test for Convergence}
    % Given Series
    Consider two series \( \sum a_n \) and \( \sum b_n \) with positive terms.
    Let
    \begin{equation*}
        L = \lim_{n \to \infty} \frac{a_{n}}{b_n}
    \end{equation*}

    \only<1>{
        % Implication for Convergence
        If \(0 < L < \infty\), then \( \sum a_n \) and \( \sum b_n \) both
        converge or both diverge.

        Example:
        \begin{equation*}
            a_n = \frac{1}{n^2},
            \qquad
            b_n = \frac{1}{n^2 - (-1)^{n}}.
        \end{equation*}
    }

    \only<2>{
        % Implication for Convergence
        If \(L=0\) and \( \sum b_n \) converges, 
        then \( \sum a_n \) converges.

        Example:
        \begin{equation*}
            a_n = \frac{1}{\log(n+1) n^2},
            \qquad
            b_n = \frac{1}{n^2}.
        \end{equation*}
    }

    \only<3>{
        % Implication for Convergence
        If \(L=\infty\) and \( \sum b_n \) diverges, 
        then \( \sum a_n \) diverges.

        Example: 
        \begin{equation*}
            a_n = \frac{\log(n)}{n},
            \qquad
            b_n = \frac{1}{n}.
        \end{equation*}
    }
\end{frame}

\begin{frame}
    \frametitle{Telescope Series Test}
    Suppose that
    \begin{equation*}
        a_n = b_{n} - b_{n+1}.
    \end{equation*}
    Then $\sum_{n=0}^{\infty} a_n$ converges if and only if
    $b_n \to L$, in which case
    \begin{equation*}
        \sum_{n=1}^{\infty} a_n
        =
        b_1 - L
    \end{equation*}

    \pause{}

    Example: Note that
    \begin{equation*}
        \frac{1}{n(n+1)} = \frac{1}{n} - \frac{1}{n+1}.
    \end{equation*}
    Thus
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{n(n+1)}
        =
        1
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.8}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n}{2n+1}
        =
        \frac{1}{3}
        +
        \frac{1}{5}
        +
        \frac{1}{7}
        +
        \cdots
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.9}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n}{2^{n}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.10}
    Show that the $p$-series $\sum_{n=1}^{\infty} \frac{1}{p^{n}}$ converges if
    $p>1$ and diverges if $p=1$ using the integral test.
\end{frame}

\begin{frame}
    \frametitle{Example 9.11}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{1}{n^{n}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.12}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n+3}{n 2^{n}}
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \acf{ec}
            \begin{itemize}
                \item Exercise 9.2: 6-21.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{\emoji{clap} The end of graph theory module!}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{seven-bridges-map.png}
%        \caption*{Where Graph Theory Started}
%    \end{figure}
%\end{frame}

\end{document}
