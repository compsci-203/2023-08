\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../language.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Trees}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{dm} 4.2 Trees}

\subsection{Definition of Trees}

\begin{frame}
    \frametitle{Acyclic graphs}
    
    A graph is \alert{acyclic} when it does not contain any cycle.

    \cake{} Is the following graph acyclic?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.7\linewidth]{graph-6.pdf}
        \includegraphics<2>[width=0.7\linewidth]{graph-2.pdf}
        \includegraphics<3>[width=0.5\linewidth]{k6.pdf}
        \includegraphics<4>[width=0.5\linewidth]{bipartite-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Trees, forests and leaves}
    A connected acyclic graph is called a \alert{tree}.

    Acyclic graphs are also called \alert{forests}.

    \begin{figure}[htpb]
        \centering
        \resizebox{0.8\linewidth}{!}{\huge \color{lime} 木 林}
        \caption*{Two Chinese characters to help you understand.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example of trees and forests}

    \cake{} Is this graph a tree? A forest?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.5\linewidth]{tree.pdf}
        \includegraphics<2>[width=0.5\linewidth]{tree-1.pdf}
        \includegraphics<3>[width=0.5\linewidth]{star-6.pdf}
        \includegraphics<4>[width=0.5\linewidth]{planar-face-3.pdf}
        \includegraphics<5>[width=0.8\linewidth]{graph-6.pdf}
    \end{figure}
\end{frame}

\subsection{Properties of Trees}

\begin{frame}
    \frametitle{Exercise: Trees}

    \cake{} Which of the following graphs are \emph{tree}s?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.28\textwidth]{tree-random-1.pdf}
        \hfill
        \includegraphics[width=0.28\textwidth]{tree-random-2.pdf}
        \hfill
        \includegraphics[width=0.28\textwidth]{tree-random-3.pdf}
    \end{figure}

    \vspace{0.5em}

    \begin{figure}[htpb]
        \includegraphics[width=0.28\textwidth]{tree-random-4.pdf}
        \hfill
        \includegraphics[width=0.28\textwidth]{tree-random-5.pdf}
        \hfill
        \includegraphics[width=0.28\textwidth]{tree-random-6.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proposition 4.2.1}

    A graph $T$ is a \emph{tree} if and only if between every pair of distinct vertices of
    $T$ there is a unique path.
\end{frame}

\begin{frame}
    \frametitle{Corollary 4.2.2}
    
    A graph $F$ is a \emph{forest} if and only if between any pair of vertices
    in $F$ there is at most one path.

    \puzzle{} Prove with details this after the class.
\end{frame}

\begin{frame}
    \frametitle{Proposition 4.2.3 (\acs{dm}) Proposition 5.11 (\acs{ac})}

    Any \tree{} with at least two vertices has at least two vertices of degree
    one,
    which we call \alert{leaves}.

    % Thought-provoking questions
    \only<1>{\think{} Can we prove this by contradiction? (\ac{dm})}
    \only<2>{\think{} Can we prove this by induction? (\ac{ac})}
\end{frame}

\begin{frame}
    \frametitle{Proposition 4.2.4}
    
    If $T$ is a \tree{} with $v$ vertices and $e$ edges, then $e = v − 1$.
\end{frame}

\begin{frame}
    \frametitle{A subtle point}
    
    In both the proofs of Proposition 4.2.4 and 4.2.3,
    the induction is done by deleting an edge from a larger \tree{}.

    Why don't we get a larger \tree{} by adding an edge to a smaller \tree{}?
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Can you show that every \tree{} with 
%    average degree $a$, i.e., 
%    \begin{equation*}
%        \frac{\sum_{v \in v} \deg\left(v\right)}{\abs{V}} = a,
%    \end{equation*}
%    has $\frac{2}{2-a}$ vertices?
%
%    \hint{} Recall that:
%    \begin{itemize}
%        \item A \tree{} of $\abs{V}$ vertices has \blankveryshort{} edges.
%        \item $\sum_{v \in V} \deg(v) = 2 \abs{E}$.
%    \end{itemize}
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    If a graph G with $v$ vertices and $e$ edges is connected and has $e > v-1$,
    must it contain a cycle? Why?
\end{frame}

\subsection{Rooted Trees}

\begin{frame}
    \frametitle{A rooted trees}
    
    \begin{figure}
        \centering
        \begin{subfigure}{0.48\textwidth}
            \centering
            \includegraphics[width=\linewidth]{tree-1.pdf}
            \caption*{A tree}%
        \end{subfigure}
        \begin{subfigure}{0.48\textwidth}
            \centering
            \includegraphics<1>[width=\linewidth]{tree-2.pdf}
            \includegraphics<2>[width=\linewidth]{tree-3.pdf}
            \caption*{\only<1>{A}\only<2>{Another} rooted tree}%
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{How graph theorist draw rooted trees}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{tree-upside-down.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Rooted trees}
    
    A \alert{rooted tree} is a \tree{} in which one vertex is designated as the \alert{root}.
    
    In a rooted tree, among two adjacent vertices, the one closer to the root is
    termed the \alert{parent}, while the other is termed the \alert{child}.
    
    The distance from a vertex to the root is referred to as its \alert{generation} or \alert{depth}.
    
    The longest distance from any vertex to the root defines the \alert{height} of the tree.
    
    Two vertices with the same parent are called \alert{siblings}.

    \cake{} Why are siblings never adjacent?
\end{frame}

\begin{frame}
    \frametitle{\zany{} Example: Binary search tree}
    In \acs{cs}, a \alert{text}  is a rooted binary tree data structure with the key of each
    node being greater than all the keys in the left
    subtree and less than the ones in its right subtree.    

    A classic problem in algorithm analysis --- If the keys are uniformly
    distributed, what is the height of the tree on average?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{binary-search-tree.png}
        \caption*{A binary search tree}
    \end{figure}
\end{frame}

\subsection{Bipartite Graphs}

\begin{frame}
    \frametitle{Speed dating}
    
    In a speed dating event, \emoji{man} are rotated to engage in 3-8 minute
    conversations with \emoji{woman}.
    
    What type of graph emerges when we model this event?
    
    \begin{figure}[htpb]
        \huge
        \centering
        \begin{tikzpicture}[
            style={graphedge, thin},
            every node/.style={inner sep=0pt},
            scale=2,
            ]
            \node (1) at (1,1.5) {\emoji{man-teacher}};
            \node (2) at (2,1.5) {\emoji{man-student}};
            \node (3) at (3,1.5) {\emoji{man-factory-worker}};
            \node (4) at (4,1.5) {\emoji{man-farmer}};
            \node (5) at (5,1.5) {\emoji{man-judge}};
            \node (6) at (1.5,0) {\emoji{woman-mechanic}};
            \node (7) at (2.5,0) {\emoji{woman-police-officer}};
            \node (8) at (3.5,0) {\emoji{woman-firefighter}};
            \node (9) at (4.5,0) {\emoji{woman-astronaut}};
            \foreach \i in {1,...,5}{
                \foreach \j in {6,...,9}{
                    \draw (\i) -- (\j);
                }
            }
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Bipartite graph}
    
    A \alert{bipartite graph} is a graph in which the vertices can be divided
    into two disjoint sets such that no edges exist between vertices within the
    same set.
    
    \cake{} Are the following graphs bipartite?
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=0.7\linewidth]{bipartite-1.pdf}
                \includegraphics<2>[width=0.7\linewidth]{c3.pdf}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=0.7\linewidth]{bipartite-2.pdf}
                \includegraphics<2>[width=0.7\linewidth]{c4.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{More bipartite graphs}

    \cake{} Are these bipartite graphs?
    
    \hint{} If a subgraph of $G$ is \emph{not} bipartite, then $G$ \emph{cannot}
    be bipartite.

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\linewidth]{bipartite-3.pdf}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\linewidth]{bipartite-4.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 4.2.6}

    Explain why every \tree{} is a bipartite graph!

    Let's just take the following \tree{} as an example.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{tree-large-2.pdf}
    \end{figure}
\end{frame}

\subsection{Spanning Trees}

\begin{frame}
    \frametitle{Spanning Trees}
    
    For $G = (V, E)$, a subgraph $H = (W, F)$ of $G$ is called a \alert{spanning
    tree} if $H$ is both a spanning subgraph of $G$ and a tree.
    
    \think{}  Is this subgraph (highlighted with green) a spanning tree?

    \begin{figure}
        \centering
        \includegraphics<1>[width=0.5\linewidth]{subgraph-spanning.pdf}
        \includegraphics<2>[width=0.5\linewidth]{subgraph-spanning-tree.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 4.2.7}
    \cake{} Can you find two different spanning trees for the following graph?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{spanning-tree-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Minimal spanning trees}
    
    A \alert{weighted graph} is a graph in which each edge has an associated
    numerical value, known as its weight.
    
    A \alert{minimal spanning tree} is a spanning tree of a weighted graph that
    minimizes the sum of the weights of its edges.
    
    This concept is crucial in network design, where the goal
    is often to connect nodes with the least cost.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{Minimum-spanning-tree.png}
        \caption*{A minimal spanning tree. Source: \href{https://en.wikipedia.org/wiki/Minimum_spanning_tree\#/media/File:Minimum_spanning_tree.svg}{Wikipedia}}
    \end{figure}
\end{frame}

\subsection{Cut Edges}

\begin{frame}
    \frametitle{Cut edge}
    
    An edge in a connected graph $G$ is a \alert{cut edge} if removing it
    disconnects the graph.

    \cake{} What are some cut-edges in the following graph?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{graph-2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Explain why that every edge in a tree is a cut-edge.

    \begin{columns}
        \begin{column}{0.5\textwidth}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{tree.pdf}
                \caption*{An example of a tree}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \emoji{green-book} \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 4.2: 1--10, 12, 14--16.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
