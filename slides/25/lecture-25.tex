\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Sequences and Series}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acf{ec} 9.1 Infinite Sequence and Series}

\subsection{Sequences}

\begin{frame}
    \frametitle{Motivational example: Swedes and cakes}
    
    In \emoji{sweden}, it is impolite to eat the last piece of food.

    If the first Swede eats $\frac{1}{2}$ of a \emoji{birthday},
    the second eats $\frac{1}{4}$ of a \emoji{birthday}, and so on,
    how many \emoji{birthday} do we need?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{bunny-cake.jpg}
    \end{figure}
\end{frame}


\begin{frame}[c]
    \frametitle{An intuitive answer}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Intuitively, the answer is 
            \begin{equation*}
                \frac{1}{2} 
                + 
                \frac{1}{4}
                +
                \frac{1}{8}
                +
                \frac{1}{16}
                +
                \dots
                =
                1
                .
            \end{equation*}

            \medskip{}

            \think{} How to make this mathematically rigorous?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{tikzpicture}[
                    scale=1.3,
                    ]
                    % Draw the circle
                    \draw[fill=SeaGreen] (0,0) circle (2cm);

                    % Initialize angle
                    \newcommand{\currentangle}{0}

                    % Loop to draw each sector
                    \foreach \x [count=\xi] in {1/2, 1/4, 1/8, 1/16, 1/32, 1/64} {
                        % Draw sector
                        \draw (0,0) -- ++(\currentangle:2cm) arc (\currentangle:\currentangle+360*\x:2cm) -- cycle;

                        % Label sector
                        \pgfmathsetmacro{\labelangle}{\currentangle + 540*\x}
                        \node at (\labelangle:1.5cm) {\footnotesize \(\x\)};

                        % Update angle
                        \pgfmathsetmacro{\currentangle}{\currentangle + 360*\x}
                    }
                \end{tikzpicture}
                \caption*{Dividing a \emoji{birthday}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Review: Sequences}

    A \alert{sequence} is \emph{a list of numbers in a specific order}. For example,
    \begin{equation*}
        a_0, a_1, a_2, a_3, \dots
    \end{equation*}
    We can denote a sequence as 
    \begin{equation*}
        \{a_{n}\}_{n \in \mathbb{N}},
        \quad 
        \{a_{n}\},
        \quad 
        (a_{n})_{n \in \mathbb{N}},
        \quad 
        (a_{n}),
        \quad 
        (a_{n})_{n \ge m}.
    \end{equation*}

    Think of a sequence as a function mapping natural numbers to real numbers. 
    For instance, the sequence \( (n) \) corresponds to the function \( f(n) = n \).

    \hint{} This week, we'll expand our focus from \alert{integer sequences} to
    \alert{real number sequences}.
\end{frame}

\begin{frame}
    \frametitle{Review: limit of function}

    A real number \( L \) is called the \alert{limit} of a function \( f(x) \)
    as \( x \) approaches \( x_{0} \), denoted as
    \[
        \lim_{{x \to x_0}} f(x) = L
    \]
    or simply \( f(x) \to L \),
    if for any given number \( \epsilon > 0 \), there exists a \( \delta > 0 \) such that
    \[
        | f(x) - L | < \epsilon
    \]
    whenever \( | x - x_0 | < \delta \).
\end{frame}

\begin{frame}
    \frametitle{Example: \( f(x) = \sin(x)/x \)}

        Recall from calculus that
        \[
        \lim_{{x \to 0}} \frac{\sin(x)}{x} = 1
        \]
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=0.8
                ]
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( x \),
                    ylabel = \( f(x) \),
                    xtick = {-3.14159, 0, 3.14159},
                    xticklabels = {\( -\pi \), \( 0 \), \( \pi \)},
                    ytick = {0, 1},
                    ymin = -0.5,
                    ymax = 1.5,
                    xmin = -4,
                    xmax = 4,
                    samples = 100
                    ]
                    \addplot[blue, domain=-4:4] {sin(deg(x))/x};
                    \addplot[red, only marks, mark=*, samples at={0}] {1};
                \end{axis}
            \end{tikzpicture}
            \caption*{Example: A function $f(x)$ which has a limit \( 1 \) as \( x \to 0 \).}
        \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Limit of sequence}

    A real number \( L \) is called the \alert{limit} of an infinite sequence \( \{ a_n \} \), denoted as
    \[
        \lim_{{n \to \infty}} a_n = L
    \]
    or simply \( a_n \to L \),
    if for any given number \( \epsilon > 0 \), there exists an integer \( N \) such that
    \[
        | a_n - L | < \epsilon
    \]
    for all \( n > N \).

\end{frame}

\begin{frame}
    \frametitle{Convergence \& Divergence}

    A sequence \( \{ a_n \} \) \alert{converges} to \( L \) if \( \lim_{{n \to \infty}} a_n = L \).

    If no such \( L \) exists, the sequence is \alert{divergent}.

    \hint{} When viewing \( (a_n) \) as a function \( f: \mathbb{N} \to \mathbb{R} \),
    \[
        \lim_{{n \to \infty}} a_n \text{ is the same as } \lim_{{n \to \infty}} f(n)
        ,
    \]
    where the latter is the limit of \( f(n) \) as \( n \) approaches infinity.
\end{frame}

\begin{frame}
    \frametitle{Example 9.1}

    For integers $n ≥ 1$ define $a_n = 2^{-n}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{A function that diverges to infinity}

    The function \( f(x) \) is said to \alert{diverges to \(\infty\)} as \( x \) 
    approaches \( x_{0} \), denoted as
    \[
        \lim_{{x \to x_0}} f(x) = \infty
    \]
    or simply \( f(x) \to \infty \),
    if for any given real number \( M \), there exists a \( \delta > 0 \) such that
    \[
        f(x) > M
    \]
    whenever \( 0 < | x - x_0 | < \delta \).
\end{frame}

\begin{frame}
    \frametitle{Example: \( f(x) = 1/|x| \)}

    Recall from calculus that
    \[
    \lim_{{x \to 0}} \frac{1}{|x|} = \infty
    \]
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8
            ]
            \begin{axis}[
                axis lines = middle,
                axis equal, % This ensures the aspect ratio is 1
                xlabel = \( x \),
                ylabel = \( f(x) \),
                xtick = {-3, -2, -1, 0, 1, 2, 3},
                ytick = {1, 2, 3, 4},
                ymin = 0,
                ymax = 5,
                xmin = -4,
                xmax = 4,
                samples = 100
                ]
                \addplot[blue, domain=-4:-0.1] {1/abs(x)};
                \addplot[blue, domain=0.1:4] {1/abs(x)};
            \end{axis}
        \end{tikzpicture}
        \caption*{Example: A function \( f(x) \) which has a limit \( \infty \) as \( x \to 0 \).}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A sequence that diverges to infinity}

    A sequence \( a_n \) is said to \alert{diverges of \(\infty\)} as \( n \) approaches infinity,
    denoted as
    \[
        \lim_{{n \to \infty}} a_n = \infty
    \]
    or simply \( a_n \to \infty \),
    if for any given real number \( M \), there exists a natural number \( N \) such that
    \[
        a_n > M
    \]
    whenever \( n > N \).

    \cake{} How should we define $a_{n} \to - \infty$?
\end{frame}

\begin{frame}
    \frametitle{Review: L'Hôpital's Rule}

    To evaluate the limit of a function in the form of
    \( \frac{0}{0} \) or \( \frac{\infty}{\infty} \), use
    \[
    \lim_{{x \to x_0}} \frac{f(x)}{g(x)} = \lim_{{x \to x_0}} \frac{f'(x)}{g'(x)}
    \]
    if \ac{rhs} limits exist.

    \begin{block}{Conditions}
        \begin{itemize}
            \item Differentiable near \( x_0 \).
            \item \( g'(x) \neq 0 \) near \( x_0 \).
        \end{itemize}
    \end{block}

    \begin{block}{Example}
        \[
        \lim_{{x \to 0}} \frac{\sin(x)}{x} = 1
        \]
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 9.2}
    For integers $n ≥ 0$ define $a_n = \frac{2n+1}{3n-2}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{Example 9.3}
    For integers $n ≥ 0$ define $a_n = \frac{e^n}{3n+2}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{Example 9.4}
    Let $(F_{n})_{n \in \dsN}$ be Fibonacci sequence.

    Let $a_{n} = F_{n}/F_{n-1}$ for $n \geq 2$.

    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{\zany{} Golden Ratio}
    The limit in the previous example
    \begin{equation*}
        \phi = \frac{1+\sqrt{5}}{2}
        \approx
        1.618
    \end{equation*}
    is called the \alert{Golden Ratio}.

    % Add bit explanation of Golden Ratio
    It is considered aesthetically pleasing.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{golden-ratio.png}
        \caption*{A golden rectangle. Source:
        \href{https://en.wikipedia.org/wiki/Golden\_ratio}{Wikipedia}}
    \end{figure}
\end{frame}

\subsection{Series}

\begin{frame}
    \frametitle{Series}

    Given a sequence $(a_{n})_{n \in \mathbb{N}}$, we call
    \begin{equation*}
        \sum_{n=1}^{\infty} a_{n}
    \end{equation*}
    a \alert{series}. 

    A series \alert{converges} if the sequence $(s_k)_{k \in \mathbb{N}}$ defined by
    \begin{equation*}
        s_k = \sum_{n=1}^{k} a_n = a_{1} + \dots + a_{k}
    \end{equation*}
    converges.
    Otherwise, it is called \alert{divergent}.

    The numbers $s_k$ are called \alert{partial sums}.
\end{frame}

\begin{frame}
    \frametitle{Geometric Progression}

    One important convergent series is a geometric progression:
    \begin{equation*}
        a + a r + a r^2 + \dots
    \end{equation*}
    with $a \ne 0$ and $\abs{r} < 1$.

    For this series, we have
    \begin{equation*}
        \lim_{n \to \infty} s_n = \frac{a}{1-r}\tag{9.6}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.5}

    If the first Swede eats $\frac{1}{2}$ of a \emoji{birthday},
    the second eats $\frac{1}{4}$ of a \emoji{birthday}, and so on,
    how many \emoji{birthday} do we need?

    In other words, what is
    \begin{equation*}
        \frac{1}{2} + \frac{1}{4} + \dots
        =
        \sum_{n=1}^{\infty} \frac{1}{2^n}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.6}

    Write the repeating decimal $0.\overline{17} = 0.17171717\dots$ as a rational number.
\end{frame}

\subsection{Continued Fractions}

\begin{frame}
    \frametitle{Continued Fractions}

    A \alert{continued fraction} is an expression of the form:
    \[
        a_0 + \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{1}{a_3 + \cdots}}}
    \]

    It provides an efficient way to approximate irrational numbers.

    Application includes number theory and optimization.
\end{frame}

\begin{frame}
    \frametitle{Example: Gold Ratio}

    Show that the golden ratio \(\phi = \frac{1+\sqrt{5}}{2}\) can be represented as:
    \[
        \phi = 1 + \cfrac{1}{1 + \cfrac{1}{1 + \cfrac{1}{1 + \cdots}}}
    \]
\end{frame}

\begin{frame}
    \frametitle{Example: Square Root of \(2\)}

    Show that
    \[
        \sqrt{2} = 1 + \cfrac{1}{2 + \cfrac{1}{2 + \cfrac{1}{2 + \cdots}}}
    \]

    \laughing{} If all \emoji{computer} were to disappear,
    we can still compute $\sqrt{2}$ by \emoji{pencil} using continued fractions.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you guess what is the number
    \[
        1 + \cfrac{3}{2 + \cfrac{3}{2 + \cfrac{3}{2 + \cdots}}}
    \]
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \acf{ec}
            \begin{itemize}
                \item Exercise 9.1: 1, 2, 3, 5, 7, 9, 11, 13, 14, 15, 20, 24.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{\emoji{clap} The end of graph theory module!}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{seven-bridges-map.png}
%        \caption*{Where Graph Theory Started}
%    \end{figure}
%\end{frame}

\end{document}
