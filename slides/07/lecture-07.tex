\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 2.5 The Ubiquitous Nature of Binomial Coefficients}

\subsection{Lattice Paths}

\begin{frame}
    \frametitle{A walk in the city}

    A \emoji{robot} at the south-west corner of a city needs to walk to the
    \emoji{fuel-pump} at the north-east corner of the city, as shown in the
    picture.

    The \emoji{robot} can only walk one-block east or one-block north.

    How many ways are there for the \emoji{robot} to do this?

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.70]
            % background color
            \fill[lightgray!30] (-1,-1) rectangle (6,5);
            \draw[ultra thick] (0, 0) 
                -- (2, 0) 
                -- (2, 1) 
                -- (3, 1)
                -- (3, 4)
                -- (5, 4)
                ;
            % grid lines
            \draw[step=1, gray, thin] (0,0) grid (5,4);
            \node at (0, 0) {\emoji{robot}};
            \node at (5, 4) {\emoji{fuel-pump}};
        \end{tikzpicture}
        \caption*{One way the \emoji{robot} can reach the \emoji{fuel-pump}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lattice paths}

    A \alert{lattice path} is a walk on the edges of a grid 
    which can only go Up (U) or Right (R).

    The \emoji{robot} problem is the same as asking for the number of lattice
    path from $(0, 0)$ to $(5, 4)$.

    \vspace{-1em}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.70]
                    % background color
                    \fill[lightgray!30] (-1,-1) rectangle (6,5);
                    \draw[ultra thick] (0, 0) 
                        -- (2, 0) 
                        -- (2, 1) 
                        -- (3, 1)
                        -- (3, 4)
                        -- (5, 4)
                        ;
                    % grid lines
                    \draw[step=1, gray, thin] (0,0) grid (5,4);
                \end{tikzpicture}
                \caption*{A lattice path from}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.70]
                    % background color
                    \fill[lightgray!30] (-1,-1) rectangle (6,5);
                    \draw[ultra thick] (0, 0) 
                        -- (1, 0) 
                        -- (1, 1) 
                        -- (2, 1) 
                        -- (2, 0) 
                        -- (3, 0) 
                        -- (3, 1)
                        -- (3, 3)
                        -- (4, 4)
                        -- (5, 4)
                        ;
                    % grid lines
                    \draw[step=1, gray, thin] (0,0) grid (5,4);
                \end{tikzpicture}
                \caption*{\alert{Not} a lattice path}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Encode a lattice path by a string}
    
    A lattice path can be encoded by a string with the alphabet 
    $\{\text{U}, \text{R}\}$.

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.70]
            % background color
            \fill[lightgray!30] (-1,-1) rectangle (6,5);
            \draw[ultra thick] (0, 0) 
                -- (2, 0) 
                -- (2, 1) 
                -- (3, 1)
                -- (3, 4)
                -- (5, 4)
                ;
            % grid lines
            \draw[step=1, gray, thin] (0,0) grid (5,4);
        \end{tikzpicture}
        \caption*{The lattice path RRURUURR}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Convert a string to path}

    What the lattice path encoded by URURURRRU?
    
    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.70]
            % background color
            \fill[lightgray!30] (-1,-1) rectangle (6,5);
            % grid lines
            \draw[step=1, gray, thin] (0,0) grid (5,4);
        \end{tikzpicture}
        \caption*{The lattice path URURURRRU}
    \end{figure}

    As the example shows, given a string consisting of $4$ U and $5$ R,
    we can find a \alert{unique} lattice path with this encoding.
\end{frame}

\begin{frame}
    \frametitle{The number of lattice paths}
    
    In summary, there is a bijection between a lattice path from \( (0,0)\) to
    \( (m,n) \) and strings consists of \blankshort{} R and \blankshort{} U.

    Thus the number of lattice path from $(0,0)$ to $(m, n)$ is the same as such
    strings.

    \cake{} What is this number?
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}
    
    How many lattice paths from (0, 0) to (5, 4) 
    are there that pass through (2, 3)?

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.80]
            % background color
            \fill[lightgray!30] (-1,-1) rectangle (6,5);
            % grid lines
            \draw[step=1, gray, thin] (0,0) grid (5,4);
            \node at (0, 0) {\emoji{robot}};
            \node at (5, 4) {\emoji{fuel-pump}};
            \node at (2, 3) {\emoji{fuel-pump}};
        \end{tikzpicture}
    \end{figure}
\end{frame}

\subsection{Catalan Numbers}

\begin{frame}
    \frametitle{``Good'' and ``bad'' paths}
    \bomb{} Now let's consider only paths from \( (0,0)\) to \( (n,n)\).

    If a lattice path does not cross the diagonal, we call it \alert{good}.
    Otherwise we call it \alert{bad}.

    \think{} How many good lattice paths are there?

    \vspace{-1em}

    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.60, 
                background rectangle/.style={fill=lightgray!30},
                show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,6);
                \draw[latticediagonal] (0, 0) -- (6, 6);
                \draw[latticepath] (0, 0) 
                    -- (2, 0) 
                    -- (2, 2) 
                    -- (3, 2)
                    -- (3, 3)
                    -- (5, 3)
                    -- (5, 4)
                    -- (6, 4)
                    -- (6, 6)
                    ;
            \end{tikzpicture}
            \caption*{A good lattice path}
        \end{figure}
        \column{0.5\textwidth}
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.60, 
                background rectangle/.style={fill=lightgray!30},
                show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,6);
                \draw[latticediagonal] (0, 0) -- (6, 6);
                \draw[latticepath] (0, 0) 
                    -- (1, 0) 
                    -- (1, 1) 
                    -- (3, 1)
                    -- (3, 5)
                    -- (5, 5)
                    -- (5, 6)
                    -- (6, 6)
                    ;
            \end{tikzpicture}
            \caption*{A bad lattice path}
        \end{figure}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Flipping the path}

    The bad path must first cross the diagonal and arrive at \( (j,j+1)\) for some
    $j$. We call this the \alert{crossing point} .

    Let's flip the lattice path after the crossing point along the line from
    \((0,1)\) to \((n,n+1)\) and see where it ends.

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.60, 
            background rectangle/.style={fill=lightgray!30},
            show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (6,6);
            \draw[latticediagonal] (0, 0) -- (6, 6);
            \draw[latticediagonal, dashed] (0, 1) -- (6, 7);
            \draw[latticepath] (0, 0) 
                -- (1, 0) 
                -- (1, 1) 
                -- (3, 1)
                -- (3, 5)
                -- (5, 5)
                -- (5, 6)
                -- (6, 6)
                ;
            \draw (3,4) node [latticenode, label={[fill=white]left:{\small Crossing point}}] {};
        \end{tikzpicture}
        \caption*{Flipped bad lattice path}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Another example}
    Note that flipped path always ends at \blankshort{}.

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.60, 
            background rectangle/.style={fill=lightgray!30},
            show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (6,6);
            \draw[latticediagonal] (0, 0) -- (6, 6);
            \draw[latticediagonal, dashed] (0, 1) -- (6, 7);
            \draw[latticepath] (0, 0) 
                -- (1, 0) 
                -- (1, 1) 
                -- (2, 1)
                -- (2, 5)
                -- (3, 5)
                -- (3, 6)
                -- (6, 6)
                ;
            \draw (2,3) node [latticenode, label={[fill=white]left:{\small Crossing point}}] {};
        \end{tikzpicture}
        \caption*{Another flipped bad lattice path}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Going the other way}
    A lattice path ending at $(n-1, n+1)$ must have a \emph{crossing point}.
    
    If we flip the part of the path after the crossing point along the line from \((0,1)\) to \((n,n+1)\),
    where does the new path end?

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.70, 
            background rectangle/.style={fill=lightgray!30},
            show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (6,6);
            \draw[latticediagonal] (0, 0) -- (6, 6);
            \draw[latticediagonal, dashed] (0, 1) -- (6, 7);
            \draw[latticepath] (0, 0) 
                -- (1, 0) 
                -- (1, 1) 
                -- (2, 1)
                -- (2, 3)
                ;
            \draw[latticepath, blue] (2, 3) 
                -- (4, 3)
                -- (4, 4)
                -- (5, 4)
                -- (5, 7)
                ;
            \draw (2,3) node [latticenode] {};
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A bijection}

    In other words, there is a bijection between 
    \begin{center}
        \emph{bad lattice paths from $(0,0)$ to $(n,n)$} 
    \end{center}
    and 
    \begin{center}
        \emph{lattice paths from $(0,0)$ to $(n-1, n+1)$}
    \end{center}
    Thus, the two numbers are the same.

    \cake{} What is this number then?
\end{frame}

\begin{frame}
    \frametitle{Catalan Number}
    Thus, we can show the number of good paths,
    called the \alert{Catalan Number}
    is
    \begin{equation*}
        C(n) = \frac{1}{n+1} \binom{2n}{n}.
    \end{equation*}

    \begin{block}{Proof}
        \vspace{12em}
    \end{block}
\end{frame}

\begin{frame}{Catalan numbers}
    Catalan numbers often appears in combinatorics.
    Stanley's book gives $214$ interpretations.

    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{catalan.png}
    \end{figure}
\end{frame}

\subsection{Dyck Paths}

\begin{frame}[t]
    \frametitle{Catalan number and Dyck path}

    A Dyck path of length $2n$ is a path from $(0, 0)$ to $(2n, 0)$
    with steps $(1, 1)$ (up-right) and $(1, −1)$ (down-right), 
    with the additional condition that the path never
    goes below the $x$-axis. 

    \only<1>{
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=1, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (2,1);
                \draw[ultra thick] (0, 0) -- (1, 1) -- (2, 0);
            \end{tikzpicture}
            \caption*{Dyck paths of length $2 = 2 \times 1$}
        \end{figure}
    }
    \only<2>{
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=1, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (4,2);
                \draw[ultra thick] (0, 0) -- (1, 1) -- (2, 0) -- (3,1) -- (4, 0);
            \end{tikzpicture}
            \quad
            \begin{tikzpicture}[scale=1, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (4,2);
                \draw[ultra thick] (0, 0) -- (1, 1) -- (2, 2) -- (3,1) -- (4, 0);
            \end{tikzpicture}
            \caption*{Dyck paths of length $4 = 2 \times 2$}
        \end{figure}
    }
    \only<3>{
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.4, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,3);
            \end{tikzpicture}
            \quad
            \begin{tikzpicture}[scale=0.4, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,3);
            \end{tikzpicture}
            \quad
            \begin{tikzpicture}[scale=0.4, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,3);
            \end{tikzpicture}
            \quad
            \begin{tikzpicture}[scale=0.4, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,3);
            \end{tikzpicture}
            \quad
            \begin{tikzpicture}[scale=0.4, background rectangle/.style={fill=lightgray!30}, show background rectangle]
                \draw[step=1, gray, thin] (0,0) grid (6,3);
            \end{tikzpicture}
            \quad
            \caption*{Dyck paths of length $6 = 2 \times 3$}
        \end{figure}
        \cake{} Why the length of a Dyck path must be a multiple of $2$?
    }
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    The number of Dyck paths of length $2$, $4$, and $6$
    are $1, 2, 5$.

    We also have the first three Catalan numbers being $1, 2, 5$.

    Can you show that the number of Dyck paths of length $2n$ is $C(n)$ for any $n \in
    \dsN$.
    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=1, background rectangle/.style={fill=lightgray!30}, show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (4,2);
            \draw[ultra thick] (0, 0) -- (1, 1) -- (2, 0) -- (3,1) -- (4, 0);
        \end{tikzpicture}
        \quad
        \begin{tikzpicture}[scale=1, background rectangle/.style={fill=lightgray!30}, show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (4,2);
            \draw[ultra thick] (0, 0) -- (1, 1) -- (2, 2) -- (3,1) -- (4, 0);
        \end{tikzpicture}
        \caption*{Dyck paths of length $2 \times 2$}
    \end{figure}

    \hint{} Find a bijection between Dyck paths and good lattice paths.
\end{frame}

%\begin{frame}{Triangulation}
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.7\textwidth]{triangulation.png}
%    \end{figure}
%
%    A \emph{triangulation} of a convex polygon with \(n+2\) vertices is set of \(n-1\) diagonals
%    which do not cross.
%
%    \begin{block}{Claim}
%        The number of triangulation of a convex \(n+2\)-gon is \(C(n)\).
%    \end{block}
%\end{frame}

\subsection{A Recursive Formula for Catalan Numbers}

\begin{frame}
    \frametitle{Breaking a Dyck path}
    
    Consider a Dyck path of length $2 \times 4$, which returns to the $x$-axis
    at $(6, 0)$.

    How many such paths are there?

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=1.00,
            background rectangle/.style={fill=lightgray!30},
            show background rectangle]
            \draw[step=1, gray, thin] (0,0) grid (8,3);
            \draw[thick, red] (0, 0) -- (1, 1);
            \draw[thick, red] (5, 1) -- (6, 0);
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A recursive formula}

    Catalan numbers satisfy the recursion
    \begin{equation*}
        C_{n} = \sum_{k=0}^{n-1} C_{k} C_{n-k-1} \qquad (n \ge 1).
    \end{equation*}
\end{frame}
%
%\begin{frame}[t]
%    \frametitle{\tps{}}
%    In a binary tree each node has either no children, one left-child, one right-child, or two
%    children.
%
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.9\textwidth]{bin.png}
%    \end{figure}
%
%    \think{} 
%    Let $B_{n}$ be the number of binary trees of \(n\) nodes is \(B(n)\).
%    Can you show that $C_n = B_n$?
%
%    \hint{} Show that 
%    \begin{equation*}
%        B_{n} = \sum_{k=0}^{n-1} B_{k} B_{n-k-1} \qquad (n \ge 1).
%    \end{equation*}
%\end{frame}

%\begin{frame}{Plane trees}
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.8\textwidth]{general-tree.png}
%    \end{figure}
%
%    \begin{block}{Formal definition}
%        A plane tree consists of a root node an ordered list of plane trees (subtrees).
%    \end{block}
%\end{frame}
%
%\begin{frame}{Catalan number and plane trees}
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.8\textwidth]{general-tree.png}
%    \end{figure}
%
%    \begin{block}{Claim}
%        \[G_{n+1}=C_{n}\]
%    \end{block}
%\end{frame}

%\begin{frame}{Catalan Number everywhere}
%    \begin{figure}
%        \centering
%        \includegraphics[width=1.05\textwidth]{catalan-summary.png}
%    \end{figure}
%\end{frame}

\section{\acs{ac} 2.6 Binomial Theorem}

\begin{frame}
    \frametitle{Binomial}
    
    \alert{Binomial} --- a mathematical expression consisting of two terms connected by a
    plus sign or minus sign.

    Examples of binomials ---
    \begin{itemize}
        \item $x + y$
        \item $3 + 9$
        \item $a - 11$
    \end{itemize}
\end{frame}

\begin{frame}{An observation}
    Look at the coefficients of these powers of binomials carefully.
    \begin{align}
        &
        (x+y)^0 = 1
        \\
        &
        (x + y)^1 = 1 x + 1 y
        \\
        &
        {\left(x + y\right)}^{2} = 1 x^{2} + 2 \, x y + 1 y^{2}
        \\
        &
        {\left(x + y\right)}^{3} = 1 x^{3} + 3 \, x^{2} y + 3 \, x y^{2} + 1 y^{3}
        \\
        &
        {\left(x + y\right)}^{4} = 1 x^{4} + 4 \, x^{3} y + 6 \, x^{2} y^{2} + 4 \, x y^{3} + 1 y^{4}
    \end{align}
\end{frame}

\begin{frame}{Pascal's triangle}
    The coefficients form a triangle
    \begin{center}
        \begin{tabular}{ccccccccc}
            & & & & $1$ & & & & \\
            & & & $1$ & & $1$ & & & \\
            & & $1$ & & $2$ & & $1$ & & \\
            & $1$ & & $3$ & & $3$ & & $1$ & \\
            $1$ & & $4$ & & $7$ & & $4$ & & $1$ \\
        \end{tabular}
    \end{center}
    which is the same as Pascal's triangle ---
    \begin{center}
        \begin{tabular}{ccccccccc}
            & & & & $\binom{0}{0}$ & & & & \\
            & & & $\binom{1}{0}$ & & $\binom{1}{1}$ & & & \\
            & & $\binom{2}{0}$ & & $\binom{2}{1}$ & & $\binom{2}{2}$ & & \\
            & $\binom{3}{0}$ & & $\binom{3}{1}$ & & $\binom{3}{2}$ & & $\binom{3}{3}$ & \\
            $\binom{4}{0}$ & & $\binom{4}{1}$ & & $\binom{4}{2}$ & & $\binom{4}{3}$ & & $\binom{4}{4}$ \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Theorem 2.30 --- Binomial theorem}

    We have
    \[
        (x+y)^{n} = \sum_{k=0}^{n} \binom{n}{k}  x^{n-k} y^{k}.
    \]

    \only<1>{
        \begin{exampleblock}{When $n=3$}
            We have
            \begin{equation*}
                \begin{aligned}
                    (x+y)^3
                    =
                    &
                    \textcolor{red}{(x+y)}
                    \textcolor{blue}{(x+y)}
                    \textcolor{orange}{(x+y)}
                    \\
                    =
                    &
                    \textcolor{red}{x}
                    \textcolor{blue}{x}
                    \textcolor{orange}{x}
                    +
                    \\
                    &
                    \textcolor{red}{x}
                    \textcolor{blue}{x}
                    \textcolor{orange}{y}
                    +
                    \textcolor{red}{x}
                    \textcolor{blue}{y}
                    \textcolor{orange}{x}
                    +
                    \textcolor{red}{y}
                    \textcolor{blue}{x}
                    \textcolor{orange}{x}
                    +
                    \\
                    &
                    \textcolor{red}{x}
                    \textcolor{blue}{y}
                    \textcolor{orange}{y}
                    +
                    \textcolor{red}{y}
                    \textcolor{blue}{x}
                    \textcolor{orange}{y}
                    +
                    \textcolor{red}{y}
                    \textcolor{blue}{y}
                    \textcolor{orange}{x}
                    \\
                    &
                    \textcolor{red}{y}
                    \textcolor{blue}{y}
                    \textcolor{orange}{y}
                    \\
                    =
                    &
                    \blankveryshort{} x^3
                    +
                    \blankveryshort{} x^2 y
                    +
                    \blankveryshort{} x y^2
                    +
                    \blankveryshort{} y^3
                \end{aligned}
            \end{equation*}
        \end{exampleblock}
    }

    \only<2>{
        \begin{block}{Proof}
            When we expand the following
            \[
                (x+y)^{n} = (x+y)(x+y)(x+y)\cdots(x+y),
            \]
            from each of the \(n\) terms, 
            we choose either \(x\) or \(y\).

            To get $x^{n-k}y^{k}$, \(x\) must be chosen \(n-k\) times,
            then \(y\) must be chosen \(k\) times.

            There are \blankshort{} ways to do this.

            So the coefficient of $x^{n-k}y^{k}$ is \blankshort{}.
        \end{block}
    }
\end{frame}

%\begin{frame}[t]
%    \frametitle{Applying the binomial theorem}
%    \begin{block}{Theorem 2.30}
%        \[
%            (x+y)^{n} = \sum_{k=0}^{n} \binom{n}{k}  x^{k} y^{n-k}.
%        \]
%    \end{block}
%
%    \cake{} What is the coefficient of \(a^{14}b^{18}\) in
%    \( (3 a^{2}-5 b)^{25} \) ?
%\end{frame}

\begin{frame}[t]
    \frametitle{A new proof}
    \begin{block}{Theorem 2.30}
        \[
            (x+y)^{n} = \sum_{k=0}^{n} \binom{n}{k}  x^{n-k} y^{k}.
        \]
    \end{block}

    \cake{} How to apply the binomial theorem to prove
    \[
    \binom{n}{0}2^{0}+\binom{n}{1}2^{1}+\cdots+\binom{n}{n}2^{n}=3^n.
    \]
\end{frame}

\section{\acs{ac} 2.7 Multinomial Coefficients}

\begin{frame}
    \frametitle{Strings with a three-letters alphabet}
    
    The number of strings consists of
    \begin{itemize}
        \item \(k_{1}\) {\color{red} a},
        \item \(k_{2}\) {\color{blue} b},
        \item the rest \(k_{3}=n-k_{1}-k_{2}\) {\color{purple} c}, 
    \end{itemize}
    is
    \[
        {\color{red}\binom{n}{k_{1}}}
        {\color{blue}\binom{n-k_{1}}{k_{2}}}
        {\color{purple}\binom{k_{3}}{k_{3}}}
        =
        {\color{red}\frac{n!}{k_{1}! (n-k_{1})!}}
        {\color{blue}\frac{(n-k_{1})!}{k_{2}! (n-k_{1}-k_{2})!}}
        {\color{purple}1}
        =
        \frac{n}{k_{1}!k_{2}!k_{3}!}
    \]
\end{frame}

\begin{frame}
    \frametitle{Multinomial coefficients}

    Such numbers are called \alert{multinomial coefficients}, denoted by
    \[
        \binom{n}{k_{1},k_{2},k_{3},\dots,k_{r}}
        =
        \frac{n!}{k_{1}!k_{2}!k_{3}!\dots!k_{r}!}
        .
    \]

    We can write binomial coefficients as multinomial coefficients since
    \begin{equation*}
        \binom{n}{k}
        =
        \frac{n!}{k! (n-k)!}
        =
        \binom{n}{k, n-k}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 2.33 -- Multinomial Theorem}
    We have
    \[
        \left( 
        x_{1}+x_{2}+\dots+x_{r}
        \right)^{n}
        =
        \sum_{k_{1}+k_{2}+\dots+k_{r}=n}
        \binom{n}{k_{1},k_{2},\dots,k_{r}}
        x_{1}^{k_{1}}
        x_{2}^{k_{2}}
        \dots
        x_{r}^{k_{r}}
    \]
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 2.9: 23, 25, 27, 29, 31, 33.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
