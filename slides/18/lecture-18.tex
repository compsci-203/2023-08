\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../language.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Eulerian and Hamiltonian Graphs}

%\includeonlyframes{current}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 5.3 Eulerian and Hamiltonian Graphs}

\subsection{Eulerian Graphs}

\begin{frame}[c]
    \frametitle{Leonhard Euler}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \setlength{\parskip}{1em}  % Enhance paragraph spacing

            Euler was a \emoji{switzerland}
            \emoji{teacher}\emoji{magnet}\emoji{telescope}\emoji{world-map}\emoji{interrobang}\emoji{mechanic} who founded the
            studies of \emph{graph theory} and topology \dots

            He also \emoji{mega} the use of notations such as $\pi$, $\sum$,
            $f(x)$, \dots.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Euler.jpg}
                \caption*{Leonhard Euler (1707--1783)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A walk with Euler}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-bridge.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The \emoji{seven}-bridges problem}
    
    In city Königsberg where Euler lived, there was a river and seven bridges on it.

    Is it possible for Euler to plan a walk so that he crosses
    each bridge exactly once and return to where he started?

    \laughing{} Of course he is not allowed to take a \emoji{ferry} or
    \emoji{airplane} or \emoji{swimmer}.

    \begin{figure}
        \centering
        \includegraphics[width=0.5\textwidth]{Konigsberg_bridges.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The 一笔画 problem}
    
    The 7-bridges problem is equivalent to ---

    Can you draw this multigraph \emph{without} lifting 
    \emoji{pencil} from the paper and return to where you started?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{seven-bridges.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Circuits}
    
    In a graph $G$ 
    a \emph{walk} $(x_{0}, \ldots, x_{t})$ is called a \alert{circuit} if 
    $x_0 = x_t$.

    \cake{} What is the difference between \emph{circuits} and \emph{cycles}?

    \cake{} Can you give an example of a circuit which is \emph{not} a cycle?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{graph-2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Eulerian Graphs}

    If there exists a circuit which traverses each edge of \( G \)
    \alert{exactly once}, then the graph is termed an \alert{eulerian}
    graph.\footnote{One of the greatest \emoji{trophy} in mathematics is to have
    one's name used in lowercase.}

    Such a journey is termed an \alert{eulerian circuit}.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\textwidth]{eulerian-graph-1.pdf}
        \caption*{An Eulerian graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Is this eulerian?}
    This graph is bit larger. But we will see that it trivial to check that is
    in fact eulerian.
    \vspace{-1em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{eulerian-graph-3.pdf}
        \caption*{An Eulerian graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A special case}
    
    Although the graph is not connected, it is still eulerian.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.45\textwidth]{eulerian-isolated-vertex.pdf}

        \caption*{A disconnected eulerian graph}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.13 (\acs{ac}) --- Euler's Theory}

    A \emph{connected}\footnote{The textbook overlooked this condition.} 
    graph is Eulerian if and only if all vertices have even degree.

    \only<1>{\cake{} Proof of Necessity ---}

    \only<2>{\sweat{} Proof of Sufficiency (Sketch) --- 

        An algorithm is devised that
        \begin{itemize}
            \item returns an Eulerian circuit,
            \item or confirms the presence of vertices with odd degrees,
            \item or confirms that the graph is disconnected.
        \end{itemize}
    }
\end{frame}

\begin{frame}[label=current]
    \frametitle{The algorithm to find an eulerian circuit}
    
    \begin{enumerate}
        \only<1>{
        \item Start with the trivial circuit \( C=(1) \)}
        \only<1>{
            \item If \( C = (x_{1}, \dots, x_{t}) \) travels through all
        edges, then the graph is \emph{eulerian}.}
        \only<1>{
            \item Find the first vertex \( x_i \) in \( C \) that has an
        untraveled edge incident to it.}
        \only<1>{
            \item If no such \( i \) exists, then the graph is
        \emph{disconnected}.}
        \only<2>{
            \setcounter{enumi}{4}
            \item Starting from \( x_i \), we do a walk \( (u_{0}=x_i, u_1, \ldots) \).}
        \only<3>{
            \setcounter{enumi}{5}
            \item If we reach $u_{s} \ne u_{0}$ with no untraveled edge
            incident to it, then $u_s$ must have odd degree.
        }
        \only<4>{
            \setcounter{enumi}{6}
            \item If we reach $u_{s} = u_{0}$, then we replace $x_i$ in $C$ by
            $(u_{0}, \dots, u_{s})$ and go to step 2.
        }
    \end{enumerate}

    \vspace{-2em}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={graphnode, thin, font=\footnotesize, minimum
            size=2em, inner sep=0pt},
            draw={red},
            thick,
            ->,
            scale=1.1,
            ]
            % Vertices
            \node (x1) at (0,0) {\(x_1\)};
            \node (x2) at (1.5,0) {\(x_2\)};
            \node (x3) at (3,0) {\(x_3\)};
            \node (xi) at (4.5,0) {\(x_{i}\)};
            \node (xi1) at (6,0) {\(x_{i+1}\)};
            \node (xt1) at (8,0) {\(x_{t-2}\)};
            \node (xt) at (9.5,0) {\(x_{t-1}\)};

            % Edges
            \draw (x1) -- (x2);
            \draw (x2) -- (x3);
            \draw[dotted] (x3) -- (xi);
            \draw (xi) -- (xi1);
            \draw[dotted] (xi1) -- (xt1);
            \draw (xt1) -- (xt);
            \draw (xt) to[out=210,in=-30,looseness=0.5] (x1);
            \only<1>{
                \draw[SeaGreen] (xi) -- ++(0,1.5);
                \node[rectangle,draw=black, fill=none, inner sep=2pt] (text1) at (7,1)
                    {Expand $C$ from here};
                \draw[graphedge,thin,dashed,->] (text1) to[out=180,in=0] ($(xi) +
                    (0,0.7)$);
                \node[rectangle,draw=black, fill=none, inner sep=2pt] (text2) at (2,1)
                    {Same as $x_1$};
                \draw[graphedge,thin,dashed,->] (text2) to[out=180,in=90] (x1);
            }
            \only<2->{
                \node[fill=MintGreen] (u0) at (xi) {\(u_{0}\)};
                \node[fill=MintGreen] (u1) at (4.5,1) {\(u_{1}\)};
                \node[fill=MintGreen] (u2) at (4.5,2) {\(u_{2}\)};
                \draw[SeaGreen] (u0) -- (u1) -- (u2);
            }
            \only<2>{
                \draw[SeaGreen] (u1) -- ++(0.7,0.7);
                \draw[SeaGreen] (u1) -- ++(-0.7,0.0);
                \node[rectangle,draw=black, fill=none, inner sep=2pt, align=center] (text1) at (7.5,1)
                    {Choose only \\ untraveled edges};
                \draw[graphedge,thin,dashed,->] (text1) to[out=180,in=0] ($(xi) + (0,0.5)$);
                \node[rectangle, draw=black, fill=none, inner sep=2pt, align=center] (text2) at (2,2)
                    {Choose smallest \\ label as \( u_2 \)};
                \draw[graphedge,thin,dashed,->] (text2) to[out=0,in=180] (u2);
            }
            \only<3>{
                \node[fill=MintGreen] (us) at (4.5,3) {\(u_{s}\)};
                \draw[SeaGreen] (u2) edge[dotted] (us);
                \foreach \x/\y [count=\i] in {-1/-1, -1/0, -1/1, 0/1, 1/1, 1/0} {
                    \pgfmathparse{ifthenelse(isodd(\i), "->", "<-")};
                    \draw[\pgfmathresult] 
                        (us) -- ++({\x/sqrt(\x*\x + \y*\y)},{\y/sqrt(\x*\x + \y*\y)});
                }
                \node[rectangle,draw=black, fill=none, inner sep=2pt, align=center] (text1) at (7.5,1)
                    {A vertex with \\ an odd degree};
                \draw[graphedge,thin,dashed,->] (text1) to[out=180,in=-45] (us);
            }
            \only<4> {
                \node[fill=MintGreen] (us) at (xi) {\(u_{s}\)};
                \draw[SeaGreen,dotted] (u2) to[out=180,in=135] (us);
                \node[rectangle,draw=black, fill=none, inner sep=2pt, align=center]
                    (text1) at (7.5,1) {Same as $u_0$.};
                \draw[graphedge,thin,dashed,->] (text1) to[out=180,in=45] (us);
            }
        \end{tikzpicture}
    \end{figure}

    \vspace{-1em}

    \only<3>{
        \hint{} Every time we travel into a vertex and then get out of it,
        we use two of the edges incident to the vertex.
    }
    \only<4>{
        \hint{} Eventually, the algorithm will stop.
    }
\end{frame}

\begin{frame}[c]
    \frametitle{The first example}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The algorithm gradually extends a circuit until it traverses all
            edges.

            \begin{enumerate}
                \item (1)
                \item (1, 2, 4, 3, 1)
                \item (1, 2, 5, 8, 2, 4, 3, 1)
                \item \dots
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{circuit-1.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A second example}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            Let's try the algorithm on another graph.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{eulerian-graph-2.pdf}
                \caption*{An Eulerian graph}
            \end{figure}
        \end{column}
    \end{columns}
    
\end{frame}


\begin{frame}
    \frametitle{A disconnected graph?}

    What happens if we try the algorithm on a disconnected graph?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{eulerian-disconnected.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{A graph with odd degrees?}

    What if we try the algorithm on a graph with odd degrees?

    \begin{columns}
        \begin{column}{0.4\textwidth}

        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{seven-bridges.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Find an eulerian circuit in the graph $G$ using this \alert{algorithm}.
    
    \begin{columns}
        \begin{column}{0.5\textwidth}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{eulerian-exercise.pdf}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Hamiltonian Graphs}

\begin{frame}
    \frametitle{Let's deliver some packages}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-postman.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A mail\emoji{rabbit-face}'s problem}

    \cake{} Can the \emoji{rabbit-face} leave the \emoji{post-office},
    visit each building  exactly once,
    and gets back at the \emoji{post-office}?
    
    \begin{figure}[htpb]
        \Large
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-hamiltonian.pdf}
        \caption*{The city where the \emoji{rabbit-face} delivers
        \emoji{envelope}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Hamiltonian Graphs}

    A graph $G$ is \alert{hamiltonian} if there is a cycle $C = (x_{0}, \ldots, x_{n})$ in $G$
    that contains every vertex of $G$.

    The cycle $C$ is called a \alert{hamiltonian cycle}.

    \cake{} Is this graph hamiltonian?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.4\textwidth]{hamiltonian-example.pdf}
        \includegraphics<2>[width=0.1\textwidth]{k1.pdf}
        \includegraphics<3>[width=0.05\textwidth]{k2.pdf}
        \includegraphics<4>[width=0.4\textwidth]{c5.pdf}
        \includegraphics<5>[width=0.4\textwidth]{k12.pdf}
        \caption*{%
            \foreach \n/\captiontext in {
                1/Hamiltonian example,
                2/$K_1$, 
                3/$K_2$, 
                4/{$C_5$ --- A cycle of length $5$}, 
                5/{$K_{12}$ --- A complete graph with $12$ vertices}
            } {
                \only<\n>{\captiontext}
            }
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Hamiltonian property in complete bipartite graphs}
    
    A \alert{complete bipartite graph}, denoted $K_{m,n}$, connects every vertex from its first part to every vertex in its second.
    
    \cake{} Is this graph hamiltonian?
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.25\textwidth]{k3-3.pdf}
        \includegraphics<2>[width=0.25\textwidth]{k2-3.pdf}
        \includegraphics<3>[width=0.25\textwidth]{k4-2.pdf}
        \caption*{%
            \foreach \n/\captiontext in {
                1/{$K_{3,3}$}, 
                2/{$K_{2,3}$},
                3/{$K_{4,2}$}
            } {
                \only<\n>{\captiontext}
            }
        }
    \end{figure}
    
    \puzzle{} When is $K_{m,n}$ a hamiltonian graph?
\end{frame}

\begin{frame}
    \frametitle{Cut-vertex and hamiltonian graph}
    A \alert{cut-vertex} is a vertex whose removal disconnects the graph.
    
    \puzzle{} Can a graph with a cut-vertex be hamiltonian?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{circuit-1.pdf}
        \caption*{A graph with a cut-vertex}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Petersen Graph}

    The following so-called Petersen graph is not hamiltonian.

    \puzzle{} Read the proof
    \href{https://mathworld.wolfram.com/PetersenGraph.html}{here}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{petersen-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Floor and Ceiling Functions}

    % Definitions
    We define:
    \begin{itemize}
        \item Floor of $x$, \( \lfloor x \rfloor \), as the greatest integer \( \leq x \)
        \item Ceiling of $x$, \( \lceil x \rceil \), as the smallest integer \( \geq x \)
    \end{itemize}

    % Examples
    Examples:
    \begin{align*}
        \lfloor 3.7 \rfloor &= 3, & \lceil 3.7 \rceil &= 4, \\
        \lfloor 3 \rfloor &= 3, & \lceil 3 \rceil &= 3
    \end{align*}

    % Properties
    Properties:
    \begin{itemize}
        \item \( \lfloor x \rfloor \leq x < \lfloor x \rfloor + 1 \)
        \item \( \lceil x \rceil - 1 < x \leq \lceil x \rceil \)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5.18 (\acs{ac}) -- Sufficient condition}

    If \( G \) is a graph on \( n \ge 3 \) vertices and the minimum degree is at
    least \( \lceil n/2 \rceil \), then \( G \) is Hamiltonian.

    \only<1-3>{\cake{} What does the theorem tell us about the following graph?}

    \only<1-3>{
        \begin{figure}[htpb]
            \centering
            \includegraphics<1>[width=0.4\textwidth]{hamiltonian-example.pdf}
            \includegraphics<2>[width=0.35\textwidth]{c5.pdf}
            \includegraphics<3>[width=0.05\textwidth]{c2.pdf}
        \end{figure}
    }
        
    \only<2>{\bomb{} This example shows the theorem's conditions are sufficient but
        not necessary for a graph to be hamiltonian.
    }

    \only<3>{\bomb{} This shows that the condition \( n \ge 3 \) cannot be
        omitted.
    }
\end{frame}
    
\begin{frame}
    \frametitle{Theorem 5.18 (\acs{ac}) -- The proof}

    \only<1>{Let's assume that \( G \) is not hamiltonian. We will derive a
        contradiction from this assumption.

        Let $P = (x_1, \dots, x_t)$ be the longest path in $G$.
        Then all neighbours of $x_1$ and $x_t$ must be in the path.

        This implies $n \ge t \ge \ceil{n/2} + 1$.
    }
    \only<2-3>{Imagine that each edge incident to $x_1$ or $x_t$ has
        a $\temoji{bird}$ attached to it.

        Imagine that the edge on the path $(x_1, \dots, x_t)$ has a
        $\temoji{wastebasket}$ attached to it.

    }
    \only<2>{If $x_1$ is adjacent to $x_{i+1}$, then we put one \emoji{bird} in the
        $\temoji{wastebasket}$ $x_{i}x_{i+1}$.
    }
    \only<3>{If $x_t$ adjacent to $x_{i}$, then we put one \emoji{bird} in the
        $\temoji{wastebasket}$ $x_{i}x_{i+1}$.
    }
    \only<4>{
        There are in total at least \( 2 \ceil{n/2} \ge n \) $\temoji{bird}$.

        There are \( t - 1 \le n - 1\) $\temoji{wastebasket}$.

        By the pigeonhole principle, at least two \emoji{bird} must
        be in the same pigeonhole.

        In other words, there is an edge $x_{i}x_{i+1}$ as shown in the picture.
    }
    \only<5>{In other words, we have a cycle of length $t$.

        Since we have assumed that $G$ is not hamiltonian, $t \le n-1$.

        So there is at least 
        \begin{equation*}
            n - t \ge n - (n-1) = 1
        \end{equation*}
        and at most
        \begin{equation*}
            n - t 
            \le n - (\ceil{n/2} + 1)
            \le n/2-1
            \le \ceil{n/2}-1
        \end{equation*}
        vertices which are not on the cycle.
    }
    \only<6>{Pick a vertex $y$ outside the cycle.

        Since there at at most $\ceil{n/2}-1$ vertices outside the cycle,
        one of $y$'s neighbours must be on the cycle.

        But this contradicts the assumption that $(x_1, \dots, x_t)$ is the
        longest path.

        This means the graph must be hamiltonian.
    }

    \only<2->{\vspace{-2em}}


    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={graphnode, thin, font=\small, minimum size=2em, inner sep=0pt},
            style={graphedge},
            scale=1.1,
            ]
            % Vertices
            \node (x1) at (0,0) {\(x_1\)};
            \node (x2) at (1.5,0) {\(x_2\)};
            \node (x3) at (3,0) {\(x_3\)};
            \node (xi) at (4.5,0) {\(x_{i}\)};
            \node (xi1) at (6,0) {\(x_{i+1}\)};
            \node (xt1) at (8,0) {\(x_{t-1}\)};
            \node (xt) at (9.5,0) {\(x_t\)};

            % Edges
            \draw (x1) -- (x2);
            \draw (x2) -- (x3);
            \draw[dotted] (x3) -- (xi);
            \only<1-4>{
                \draw (xi) -- (xi1);
            }
            \only<1-5>{
                \draw[dotted] (xi1) -- (xt1);
            }
            \draw (xt1) -- (xt);
            \only<2, 4->{
                \draw (x1) to[out=30,in=150] (xi1);
            }
            \only<3->{
                \draw (xi) to[out=30,in=150] (xt);
            }
            \only<2-4>{
                \foreach \i [count=\n] in {0.75, 2.25, 5.25, 8.75} {
                    \node[draw=none, fill=none] (wastebasket\n) at (\i, -0.5) {\emoji{wastebasket}};
                }
            }
            \only<2, 4>{
                \node[draw=none, fill=none] (b1) at (3, 1.2) {\emoji{bird}};
            }
            \only<3, 4>{
                \node[draw=none, fill=none] (bt) at (7, 1.2) {\emoji{bird}};
            }
            \only<2, 4>{
                \draw[thin, red, dashed, ->] (b1) to[out=45,in=90] (wastebasket3);
            }
            \only<3, 4>{
                \draw[thin,red, dashed, ->] (bt) to[out=135,in=90] (wastebasket3);
            }
            \only<6>{
                \node[fill=SeaGreen] (y) at (7,-1) {\(y\)};
                \draw[ultra thick, SeaGreen] 
                    (y) -- (7.2,0) -- (xt1) -- (xt)
                    to[out=150,in=30] (xi)
                    -- (x3) -- (x2) -- (x1)
                    to[out=30,in=150] (xi1)
                    -- (6.8, 0);
            }
        \end{tikzpicture}
        \caption*{The longest path in \( G \)}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{An application of Theorem 5.18 (\acs{ac})}
    
    Show that any graph $G$ with $17$ vertices and $129$ edges must be
    hamiltonian.

\end{frame}

%\begin{frame}[t]
%    \frametitle{Complete bipartite graphs}
%    
%    A complete bipartite graph $K_{m, n}$ is a bipartite graph with two parts of
%    sizes $m$ and $n$ such that every vertex in the first part is connected to every
%    vertex in the second part.
%
%    What is a necessary and sufficient condition for $K_{m,n}$ to be hamiltonian?
%\end{frame}
%
%\begin{frame}[t]
%    \frametitle{Number of components}
%
%    \begin{exampleblock}{Can you prove?}
%        If $G$ has a hamiltonian cycle, then for each nonempty set 
%        $S \subseteq V$, the graph $G \setminus S$ has at most $|S|$ vertices.
%    \end{exampleblock}
%
%    \begin{exampleblock}{Can you distinguish?}
%        Which of the following are hamiltonian.
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{hamiltonian-3.png}
%    \end{figure}
%    \end{exampleblock}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \emoji{closed-book} \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 8--12.
            \end{itemize}

            \emoji{green-book} \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 4.5: 1,2, 7--10.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
