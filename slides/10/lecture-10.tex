\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}
    \frametitle{Why do we care about generating functions?}

    Though many problems amenable to \ac{gf} can be tackled otherwise, \acp{gf}
    offer unique strengths ---

    \begin{itemize}
        \item 
            They serve as a unified language for combinatorics.

        \item
            Theoretically, they yield precise and elegant counting formulas.

        \item
            Practically, they facilitate computational analysis of sequences.

        \item
            They bridge disparate areas of mathematics, including analysis,
            algebra, combinatorics, and number theory.
    \end{itemize}
\end{frame}

\section{\acs{ac} 8.1 Basic Notion and Terminology}

\subsection{\acp{gf} are not functions}

\begin{frame}{What is a \acf{gf}}
    Given an infinite sequence \((a_{n})_{n \ge 0} = (a_{0},a_{1},\dots)\), 
    we can associate it with a ``\emph{function}''
    \[
        F(x)=\sum_{n \ge 0} a_{n} x^{n},
    \]
    called the \alert{generating function} of \((a_{n})_{n \ge 0}\).

    \hint{} Think it as an alternative way to write $(a_{n})_{n \ge 0}$.
\end{frame}
        
\begin{frame}{\ac{gf} is not function}
    \astonished{} A \ac{gf} \(F(x)\) is \emph{not} actually a function.

    We will \alert{never} ask questions like ``what is $F(1/2)$?''

    The following sum does not converge for any $x$ except $x=0$,
    \begin{equation*}
        F(x) = \sum_{n \ge 0} n! x^{n}.
    \end{equation*}
    But it is still a perfectly fine \ac{gf}.
\end{frame}

\begin{frame}
    \frametitle{Examples of \ac{gf}}

    What are the \acp{gf} for the following sequences?
    \begin{itemize}
        \item $a_{0} = 1$, and $a_{n} = 0$ for $n \ge 1$?

        \item $b_{0} = 1$, $b_{1} = -1$, and $b_{n} = 0$ for $n \ge 2$?

        \item $c_{n} = 1$ for all $n \ge 0$?

        \item $d_{n} = 2^{n}$ for all $n \ge 0$?
    \end{itemize}
\end{frame}

\subsection{Manipulate \acp{gf}}

\begin{frame}[t]
    \frametitle{Add \acp{gf}}
    
    Given two \acp{gf} 
    \begin{equation*}
        A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}, 
        \qquad
        B(x) = \sum_{n=0}^{\infty} b_{n} x^{n},
    \end{equation*}
    we \emph{define}
    \begin{equation*}
        A(x)+ B(x) = \sum_{n=0}^{\infty} (a_{n} + b_{n}) x^{n}
        .
    \end{equation*}

    \only<1>{
        \cake{} What is $\sum_{n=0}^{\infty} x^{n} + \sum_{n=0}^{\infty} (-1)^{n} x^{n}$?
    }
    \only<2>{
        \cake{} How to define $A(x)-B(x)$?
    }
\end{frame}

\begin{frame}
    \frametitle{Multiply polynomials}
    Consider how we \emoji{multiply} polynomials.

    \cake{} Can you expand
    \begin{flalign*}
        \quad (1-x)(1+x+x^{2}) = &&
    \end{flalign*}
\end{frame}

\begin{frame}[t]
    \frametitle{Multiply \acp{gf}}
    Given two \acp{gf} 
    \begin{equation*}
        A(x) = {\color{red} \sum_{n=0}^{\infty} a_{n} x^{n}}, 
        \qquad
        B(x) = {\color{blue} \sum_{n=0}^{\infty} b_{n} x^{n}},
    \end{equation*}
    we \emph{define}
    \begin{equation*}
        A(x)B(x) 
        = 
        \sum_{n=0}^{\infty} \left(\sum_{k=0}^{n} {\color{red} a_{k} x^{k}}
        {\color{blue} b_{n-k} x^{n-k}} \right)
        = 
        \sum_{n=0}^{\infty} \left(\sum_{k=0}^{n} a_{k} b_{n-k}\right) x^{n}
        .
    \end{equation*}

    \only<1>{
        \cake{} What is $(1-x) \sum_{n=0}^{\infty} x^{n}$?
    }
    \only<2>{
        \cake{} How should we define $A(x)^{n}$?
    }
\end{frame}

\begin{frame}
    \frametitle{Multiplicative inverse}

    Given $F(x)$, if there exists $G(x)$ such that $G(x) F(x) = 1$, 
    then we \emph{define} the \alert{multiplicative inverse} of $F(x)$ by
    \begin{equation*}
        F(x)^{-1}=
        \frac{1}{F(x)} = G(x).
    \end{equation*}

    \cake{} 
    Let $F(x) = 1-x$, what is its multiplicative inverse $F(x)^{-1}$?
\end{frame}

\begin{frame}
    \frametitle{The composition of two \acp{gf}}

    We also define
    \begin{equation*}
        A(B(x)) = \sum_{n=0}^{\infty} a_{n} B(x)^{n}.
    \end{equation*}

    \cake{} What is $\frac{1}{1-x^{2}}$?
\end{frame}

%\begin{frame}[t]
%    \frametitle{The integral of a \ac{gf}}
%
%    The integral of $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ is
%    \emph{defined} by
%    \begin{equation*}
%        \int A(x) \dd x 
%        = 
%        \sum_{n=0}^{\infty} \int a_{n} x^{n} \dd x
%        = 
%        \sum_{n=0}^{\infty} \underline{\phantom{\frac{a_{n}}{n+1} x^{n+1}}}
%        .
%    \end{equation*}
%
%    \cake{} What is $\int \frac{1}{1-x} \dd x$?
%\end{frame}

%\begin{frame}
%    \frametitle{A quick exercise from calculus}
%
%    What is
%    \begin{equation*}
%        2^{-0} + 2^{-1} + 2^{-2} + 2^{-3} + \cdots = \sum_{n = 0}^{\infty} \frac{1}{2^{n}}
%    \end{equation*}
%    and
%    \begin{equation*}
%        3^{-0} + 3^{-1} + 3^{-2} + 3^{-3} + \cdots = \sum_{n = 0}^{\infty} \frac{1}{3^{n}}
%    \end{equation*}
%    and
%    When $\abs{x} < 1$, we know that the sum converges and
%    \[
%        \sum_{n = 0}^{\infty} x^{n} = \frac{1}{1-x}.
%    \]
%
%\end{frame}
%
%\begin{frame}{Examples of \ac{gf}}
%    If \(a_{n}=1\) for all \(n \ge 0\), 
%    then its generating function is
%    \[
%        F(x)=1 + x + x^{2} + x^{3} + \cdots = \sum_{n = 0}^{\infty} x^{n}.
%    \]
%
%    \think{} Can we write
%    \[
%        F(x) = \frac{1}{1-x}
%    \]
%    What does it even mean?
%\end{frame}

\begin{frame}[t]
    \frametitle{The derivative of a \ac{gf}}

    The derivative $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ is
    \emph{defined} by
    \begin{equation*}
        \dv{x} A(x) 
        = 
        \sum_{n=0}^{\infty} \dv{x} (a_{n} x^{n})
        = 
        \sum_{n=0}^{\infty} \underline{\phantom{n a_{n} x^{n-1}}}
        .
    \end{equation*}

    \cake{} What is $\dv{x} \frac{1}{1-x}$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    The $n$-th derivative of a \ac{gf} is defined by
    \begin{equation*}
        \dv[n]{x} F(x)
        =
        \begin{cases}
            \displaystyle\dv{x} F(x) & \text{for } n = 1, \\
            \displaystyle\dv[n-1]{x} (\dv{x} F(x)) & \text{for } n \ge 2.
        \end{cases}
    \end{equation*}

    \cake{} What are the coefficients of $\dv[2]{x} \frac{1}{1-x}$?
\end{frame}

\subsection{The Transfer Principle}

%\begin{frame}
%    \frametitle{Function or \ac{gf}?}
%    
%    Note that as functions
%    \begin{equation*}
%        \dv{x} e^{x} = e^{x}.
%    \end{equation*}
%
%    \cake{} And as \acp{gf}, we have
%    \begin{flalign*}
%        \quad
%        \dv{x} e^{x} 
%        =
%        \dv{x} 
%        \sum_{n=0}^{\infty}
%        \frac{x^{n}}{n!}
%        = 
%        \sum_{n=0}^{\infty}
%        \dv{x} 
%        \frac{x^{n}}{n!}
%        =
%        &
%        &
%    \end{flalign*}
%\end{frame}

\begin{frame}
    \frametitle{Review --- Functions defined by infinite sums}

    In calculus, we define functions using infinite sums, such as
    \begin{equation*}
        e^{x} 
        = 
        1 + x + \frac{x^2}{2!} + \frac{x^{3}}{3!} + \cdots
        =
        \sum_{n=0}^{\infty} \frac{x^{n}}{n!}
        ,
        \qquad (\text{for all $x \in \dsR$})
    \end{equation*}
    and
    \begin{equation*}
        -\log(1-x) = x + \frac{x^2}{2} + \frac{x^3}{3} + \frac{x^{4}}{4} + \cdots
        =
        \sum_{n=1}^{\infty} \frac{x^{n}}{n}
        ,
        \qquad (\text{for all $x < 1$})
    \end{equation*}

    \cake{} We often use functions such as $e^{x}$ and $-\log(1-x)$
    to denote \acp{gf}
    \begin{equation*}
        F(x) 
        = \sum_{n=0}^{\infty} \frac{x^{n}}{n!},
        \qquad
        G(x)
        =
        \sum_{n=1}^{\infty} \frac{x^{n}}{n},
    \end{equation*}
    which have the same coefficient sequences respectively.
\end{frame}

\begin{frame}
    \frametitle{The transfer principle}

    Given $(a_n)_{n=0}^{\infty}$ and $(b_n)_{n=0}^{\infty}$, we can view
    \begin{equation*}
        A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}, 
        \qquad
        B(x) = \sum_{n=0}^{\infty} b_{n} x^{n}
    \end{equation*}
    as \acp{gf} or functions.


    The transfer principle of \acp{gf} states that

    \begin{quote}
    If $A(x) = B(x)$ as functions on a non-empty neighbourhood of $0$,
    then $A(x) = B(x)$ as \acp{gf}.
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{Example of the transfer principle}
    
    Consider \acp{gf} $A(x) = e^{x}$, $B(x) = -\log(1-x)$ and $C(x) = \frac{1}{1-x}$.

    How to show that $A(B(x)) = C(x)$?
\end{frame}

\begin{frame}
    \frametitle{A useful identity}
    
    From calculus, we have
    \begin{equation*}
        \dv[k]{x} \frac{1}{1-x}
        =
        \frac{k!}{(1-x)^{k+1}}
        \qquad
        (\text{for all $k \ge 1$})
        .
    \end{equation*}

    How to use this to show that
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}}
        =
        \sum_{n = 0}^{\infty} \binom{n+k}{k} x^{n}
        \qquad
        (\text{for all $k \ge 1$})
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Check the identity}
%
%    It follows from the previous identity that
%    \begin{equation}
%        \dv{x} \sum_{n=0}^{\infty} x^{n}
%        =
%        \dv{x} \frac{1}{1-x}
%        =
%        \frac{1}{(1-x)^{2}}
%        .
%        \label{MWOX}
%    \end{equation}
%
%    By the definition of derivative for \acp{gf}, i.e.,
%    \begin{equation}
%        \dv{x} 
%        \sum_{n=0}^{\infty} x^{n}
%        = 
%        \sum_{n=0}^{\infty} n x^{n-1}
%        .
%        \label{SHIS}
%    \end{equation}
%    How can we show the \eqref{MWOX} and \eqref{SHIS} are the same?
%\end{frame}

\section{\acs{ac} 8.2 Another look at distributing carrots}

%\begin{frame}
%    \frametitle{Amanda's money again}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.7\textwidth]{Amanda-01.jpg}
%    \end{figure}
%\end{frame}

\subsection{Amanda's \carrot{} again}

\begin{frame}[t]
    \frametitle{Review --- $n$ \carrot{} and five \bunny{}}

    Let $b_n$ be the number of ways that $n$ \emoji{carrot} can be distributed to $5$ \bunny{}
    so that each \bunny{} gets at least \emph{one} \carrot{}. 

    Then
    \begin{equation*}
        b_n = 
        \begin{cases}
            {\displaystyle \binom{\hspace{2cm}}{\hspace{2cm}}}
            &
            n \ge 5, \\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \think{} Can we get this from \ac{gf}?
\end{frame}

\begin{frame}[t]
    \frametitle{Just one \bunny{}}
    
    Let $a_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{one}
    \bunny{} so that the \bunny{} has $>0$ \carrot{}.

    Thus
    \begin{equation*}
            a_n = 
        \begin{cases}
            \underline{\hspace{0.5cm}} & (n = 0) \\
            \underline{\hspace{0.5cm}} & (n \ge 1) \\
        \end{cases}
    \end{equation*}

    The \ac{gf} of $(a_{n})_{n \ge 0}$ is
    \begin{flalign*}
        \quad
        A(x)
        =
        \sum_{n \ge 0} a_{n} x^n
        =
        &&
    \end{flalign*}

\end{frame}

\begin{frame}[t]
    \frametitle{When there are five \bunny{}}
    
    Let $b_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{five}
    \bunny{}  so that each \bunny{} has $>0$ \carrot{}.

    The \ac{gf} for $ (b_{n})_{n \ge 0}$ is
    \begin{flalign*}
        \quad
        B(x)
        =
        \sum_{n = 0}^{\infty}
        b_{n} x^{n}
        =
        &&
    \end{flalign*}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Is this a coincidence?}
    
    Using
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}}
        =
        \sum_{n = 0}^{\infty} \binom{n+k}{k} x^{n}
        \qquad
        (\text{for all $k \ge 1$})
        ,
    \end{equation*}
    we can show that
    \begin{equation*}
        A(x)^{5}
        =
        \frac{x^{5}}{(1-x)^{5}}
        =
        \sum_{n = 5} \binom{n-1}{4} x^{n}
        =
        B(x)
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{No, it is not a coincidence!}

    Note that
    \begin{equation*}
        \begin{aligned}
            &
            A(x)^{5}
            \\
            &
            =
            {\color{red} (x + x^2 + \cdots)}
            {\color{blue} (x + x^2 + \cdots)}
            {\color{orange} (x + x^2 + \cdots)}
            {\color{Teal} (x + x^2 + \cdots)}
            {\color{Maroon} (x + x^2 + \cdots)}
            \\
            &
            =
            0 x^{0}
            +
            0 x^{1}
            +
            0 x^{2}
            +
            0 x^{3}
            +
            0 x^{4}
            +
            \\
            \pause{}
            &
            \qquad
            {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{Teal}x}{\color{Maroon}x}
            +
            \\
            &
            \pause{}
            \qquad
            (
            {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{Teal}x}{\color{Maroon}x^{2}}
            +
            {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{Teal}x^{2}}{\color{Maroon}x}
            +
            {\color{red}x}{\color{blue}x}{\color{orange}x^{2}}{\color{Teal}x}{\color{Maroon}x}
            +
            {\color{red}x}{\color{blue}x^{2}}{\color{orange}x}{\color{Teal}x}{\color{Maroon}x}
            +
            {\color{red}x^{2}}{\color{blue}x}{\color{orange}x}{\color{Teal}x}{\color{Maroon}x}
            )
            +
            \dots
        \end{aligned}
    \end{equation*}

    \pause{}

    In other words
    \begin{equation*}
        A(x)^{5}
        =
        \sum_{n \ge 5}
        \sum_{\substack{k_{1}, \dots , k_{5} \ge 1\\ k_{1} + \dots k_{5} = n}}
        {\color{red}x^{k_{1}}}{\color{blue}x^{k_{2}}}{\color{orange}x^{k_{3}}}{\color{Teal}x^{k_{4}}}{\color{Maroon}x^{k_{5}}}
        =
        \sum_{n = 0}^{\infty}
        b_{n} x^{n}
        =
        B(x)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Recipe \emoji{ramen} --- Integer composition with \ac{gf}}

    Recall the number of partitions of $n$ is the number of integer solutions of
    \begin{equation*}
        x_{1} + x_{2} + \cdots x_{k} = n
    \end{equation*}
    such that $x_{1},\ldots, x_{k} \ge 1$.

    We can solve such a problem by
    \begin{itemize}
        \item find the \ac{gf} for each \bunny{};
        \item multiply them together to a new \ac{gf}\@;
        \item extract the coefficients of the new \ac{gf}, either by
            \emoji{pencil} or by
            \emoji{robot}.
    \end{itemize}
\end{frame}

\subsection{Amanda's \carrot{} with some restrictions}

\begin{frame}[t]
    \frametitle{$n$ \emoji{carrot} and five \bunny{} Again}

    Let $b_n$ be the number of ways that $n$ \emoji{carrot} can be distributed to $5$ \bunny{}
    so that each \bunny{} gets at least \emph{two} \carrot{}. 

    Then
    \begin{equation*}
        b_n = 
        \begin{cases}
            {\displaystyle \binom{\hspace{2cm}}{\hspace{2cm}}}
            &
            n \ge 10, \\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \think{} Can we get this from \ac{gf}?
\end{frame}

\begin{frame}[t]
    \frametitle{Just one \bunny{}}
    
    Let $a_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{one}
    \bunny{} so that the \bunny{} has $\ge 2$ \carrot{}.

    Thus
    \begin{equation*}
            a_n = 
        \begin{cases}
            0 & (n \le 1) \\
            1 & (n \ge 2) \\
        \end{cases}
    \end{equation*}

    The \ac{gf} of $(a_{n})_{n \ge 0}$ is
    \begin{flalign*}
        \quad
        A(x)
        =
        \sum_{n \ge 0} a_{n} x^n
        =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}[fragile]
    \frametitle{When there are five \bunny{}}
    
    Let $b_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{five}
    \bunny{}  so that each \bunny{} has $\ge 2$ \carrot{}.

    Let $B(x) = \sum_{n \ge 0} b_{n} x^{n}$.

    Thus, by the same argument as before
    \begin{equation*}
        B(x)
        =
        A(x)^{5}
        =
        \left(\frac{x^2}{(1-x)}\right)^{5}
        =
        \sum_{n \ge 0} b_{n} x^{n}
    \end{equation*}

\end{frame}

\begin{frame}[fragile]
    \frametitle{\zany{} Get the coefficients by \emoji{robot}}

    \emoji{robot} Using WolframAlpha with the query
    \begin{footnotesize}
    \begin{verbatim}
Factor[SeriesCoefficient[(x^2/(1-x))^5, {x, 0, n}]]
    \end{verbatim}
    \end{footnotesize}
    \vspace{-2em}
    we get
    \begin{equation*}
        b_n =
        \begin{cases}
            \frac{1}{24} (n-9)(n-8)(n-7)(n-6) & (n>5) \\
            0 & (0 \le n \le 5)
        \end{cases}
    \end{equation*}
    But we have got
    \begin{equation*}
        b_n = 
        \begin{cases}
            {\displaystyle \binom{n-6}{4}}
            &
            n \ge 10, \\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}
    \cake{} Are these the same? What about $n \in \{6, 7, 8, 9\}$?
\end{frame}

%\begin{frame}
%    \frametitle{Get the coefficients by \emoji{pencil}}
%
%    How to get $b_n$ from
%    \begin{equation*}
%        \left(\frac{x^2}{(1-x)}\right)^{5}
%        =
%        \sum_{n \ge 0} b_{n} x^{n}
%    \end{equation*}
%    using only \emoji{pencil}?
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 8.8: 1, 2, 3, 4.
            \end{itemize}

            \href{https://aofa.cs.princeton.edu/30gf/}{\acf{iaa}}
            \begin{itemize}
                \item Exercises: 3.1, 3.2, 3.6, 3.7.
            \end{itemize}
            \hint{} In \ac{iaa}, 
            \begin{itemize}
                \item $[z^{n}]F(z)$ denotes the coefficients of $z^{n}$ in $F(z)$,
                \item and $H_{n} = \sum_{k=1}^{n} 1/k$.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
