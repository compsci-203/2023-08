\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Graph Colouring (1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 5.4 Graph Colouring}

\subsection{Motivational Example}

\begin{frame}
    \frametitle{Example 1: Distinct cars for ex-partners}

    A group of \bunny{} plans a car trip.

    Ex-partners, as indicated in the graph, must travel in separate cars.

    \think{} What's the fewest number of cars needed?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.75\linewidth]{graph-2.pdf}
        \caption*{Ex-partner \bunny{}}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Colours to help}

    We can colour the vertices to denote which car each \bunny{} is in.

    The task is to find the minimum number of colours ensuring no adjacent
    vertices share one.

    The picture below shows that with three colours suffice. 

    \cake{} Can we use fewer colours?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.75\linewidth]{graph-2-colour.pdf}
        \caption*{Distinct colours, distinct cars}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 2: Frequency Allocation for Radio Towers}

    Radio \emoji{tokyo-tower} within \(100\) km must use distinct frequency bands.

    Locations are depicted below.

    \think{} What's the fewest number of frequencies to avoid interference?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{radio-1.pdf}
        \caption*{Radio \emoji{tokyo-tower}}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Colouring to help again}

    Let \emoji{orange-circle} symbolize radio \emoji{tokyo-tower}, and edges denote proximity
    within \(100\) km.

    Let the colour denote the frequency band a \emoji{tokyo-tower} uses.

    \think{} How many colours suffice to ensure adjacent vertices differ?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{radio-2.pdf}
        \caption*{Radio \emoji{tokyo-tower} as Graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Can you do better?}

    As shown below, $5$ colours are enough.

    \cake{} Can we use less?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{radio-3.pdf}
        \caption*{A colouring of the \emoji{tokyo-tower} graph}
    \end{figure}
\end{frame}

\subsection{Chromatic Number}

\begin{frame}
    \frametitle{Chromatic number}

    Given \( G = (V, E) \) and a colour set \( C \),

    A \alert{proper colouring} is a function \( \phi: V \to C \) such that \( \phi(x) \ne \phi(y) \) for \( xy \in E \).

    The smallest \( t \) allowing a \emph{proper colouring} with \( t \) colours is the \alert{chromatic number}, \( \chi(G) \).

    \cake{} What graph has \( \chi(G) = 0 \)?
\end{frame}

\begin{frame}
    \frametitle{Nature of colours}

    Elements in \( C \) are arbitrary and serve to categorise vertices. For example, the \( K_5 \) graph can be coloured using different sets for \( C \) as follows:
    \only<1>{\( C = \{\temoji{red-circle}, \temoji{blue-circle}, \temoji{green-circle}, \temoji{yellow-circle}, \temoji{purple-circle}\} \)}
    \only<2>{\( C = \{1, 2, 3, 4, 5\} \)}
    \only<3>{\( C = \{\temoji{cat-face}, \temoji{dog-face}, \temoji{cow-face}, \temoji{rabbit-face}, \temoji{tiger-face}\} \)}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode},
            style={graphedge},
            scale=0.8]
            % Vertices
            \foreach \i/\colour/\animal in {1/BrightRed/cat-face, 2/DaylightBlue/dog-face, 3/MintGreen/cow-face, 4/GoldenWhisper/rabbit-face, 5/LavenderDream/tiger-face}
            {
                \only<1>{\node[label={\i*72-90: \( v_{\i} \)}, fill=\colour] (\i) at (\i*72-90:2cm) {};}
                \only<2>{\node[label={\i*72-90: \( v_{\i} \)}] (\i) at (\i*72-90:2cm) {\i};}
                \only<3>{\node[fill=none, label={\i*72-90: \( v_{\i} \)}] (\i) at (\i*72-90:2cm) {\emoji{\animal}};}
            }
            % Edges
            \foreach \i in {1,...,4} {
                \pgfmathtruncatemacro{\jstart}{\i+1}
                \foreach \j in {\jstart,...,5} {
                    \draw (\i) -- (\j);
                }
            }
        \end{tikzpicture}
        \caption*{Example: \( K_{5} \) colouring}
    \end{figure}
\end{frame}

\subsection{Chromatic Number of Specific Graphs}

\begin{frame}
    \frametitle{Independent Graphs}
    Recall that an independent graph \( I_n \) is a graph with \( n \) vertices and no edges.

    \cake{} What is \( \chi(I_n) \)?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode, minimum size=1em},
            style={graphedge},
            scale=0.8]
            % Vertices
            \foreach \i in {1,...,8}
            {
                \node[label={\i*45-90: \( v_{\i} \)}, fill=MintGreen] (\i) at
                    (\i*45-90:2cm) {};
            }
        \end{tikzpicture}
        \caption*{Example: \( I_{8} \) colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complete graphs}

    \cake{} What is $\chi(K_n)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{k5-2.pdf}
        \caption*{Example: $K_5$ colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Star graphs}

    Let $S_n$ denote the \tree{} with $n+1$ nodes and $n$ \emoji{leaves}.

    \cake{} What is $\chi(S_n)$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode},
            style={graphedge},
            scale=1]
            % Vertices
            \node[fill=GoldenWhisper] (0) at (0, 0) {};
            \foreach \i in {1,...,5}
            {
                \node[fill=MintGreen] (\i) at
                    (\i*72-90:2cm) {};
            }
            % Edges
            \foreach \i in {1,...,5} {
                \draw (\i) -- (0);
            }
        \end{tikzpicture}
        \caption*{Example: \( S_{5} \) colouring}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Not so simple!}
%
%    \think{} 
%    \only<1>{What is $\chi(G)$?}
%    \only<2>{Clearly $\chi(G) \le 4$? But does $3$ colours suffices?}
%
%    \begin{figure}[htpb]
%        \centering
%        \begin{tikzpicture}[
%            style={graphedge},
%            every node/.style={graphnode},
%            scale=2,
%            ]
%            \foreach \i in {1,...,5}{
%                \pgfmathsetmacro\j{int(\i+5)};
%                \node (\i) at (\i,0) {\i};
%                \node (\j) at (\i,1) {\j};
%            }
%            \node (11) at (3, 2) {11};
%            \foreach \i in {2,...,5}{
%                \pgfmathsetmacro\j{int(\i+5)};
%                \pgfmathsetmacro\ilast{int(\i-1)};
%                \draw (\i) -- (\ilast);
%            }
%            \foreach \i in {2,...,4}{
%                \pgfmathsetmacro\jlast{int(\i+4)};
%                \pgfmathsetmacro\jnext{int(\i+6)};
%                \draw (\i) -- (\jnext);
%                \draw (\i) -- (\jlast);
%                \pgfmathsetmacro\j{int(\i+5)};
%                \pgfmathsetmacro\ilast{int(\j-4)};
%                \pgfmathsetmacro\inext{int(\j-6)};
%                \draw (\j) -- (\inext);
%                \draw (\j) -- (\ilast);
%            }
%            \draw (6) -- (5);
%            \draw (1) -- (10);
%            \foreach \j in {6,...,10}{
%                \draw (\j) -- (11);
%            }
%            \draw (1) to [out=315,in=225] (5);
%            \only<2>{
%                % Draw some new nods to colour things
%                \foreach \i in {2,5,11}{
%                    \node[fill=LightGreen] at (\i) {\i};
%                }
%                \foreach \i in {6,7,8,9,10}{
%                    \node[fill=pink] at (\i) {\i};
%                }
%                \foreach \i in {1, 4}{
%                    \node[fill=yellow] at (\i) {\i};
%                }
%                \foreach \i in {3}{
%                    \node[fill=LightBlue] at (\i) {\i};
%                }
%            }
%        \end{tikzpicture}
%    \end{figure}
%\end{frame}

\subsection{Chromatic Number of Cycles}

\begin{frame}
    \frametitle{Cycles}
    
    Let $C_{n}$ denote a cycle of $n$ vertices.

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c3.pdf}
            \end{center}
            \caption*{$C_3$}
        \end{subfigure}
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c4.pdf}
            \end{center}
            \caption*{$C_4$}
        \end{subfigure}
        
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c5.pdf}
            \end{center}
            \caption*{$C_5$}
        \end{subfigure}
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c6.pdf}
            \end{center}
            \caption*{$C_6$}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Even Cycle}
    
    \cake{} What is \( \chi(C_{2n}) \)?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode},
            style={graphedge},
            scale=1]
            % Vertices
            \foreach \i [evaluate=\i as \colour using {int(mod(\i,2))}] in {1,...,6}
            {
                \ifnum\colour=0
                    \node[label={\i*60-90:\( v_{\i} \)}, fill=MintGreen] (\i) at (\i*60-90:2cm) {};
                \else
                    \node[label={\i*60-90:\( v_{\i} \)}, fill=Salmon] (\i) at (\i*60-90:2cm) {};
                \fi
            }
            % Edges
            \foreach \i in {1,...,5} {
                \pgfmathtruncatemacro{\j}{\i+1}
                \draw (\i) -- (\j);
            }
            \draw (6) -- (1);
        \end{tikzpicture}
        \caption*{Example: \( C_{6} \) Colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Odd cycle}
    
    \think{} What is $\chi(C_{2n+1})$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graphnode},
            style={graphedge},
            scale=1]
            % Vertices
            \foreach \i [evaluate=\i as \colour using {int(mod(\i,2))}] in {1,...,9}
            {
                \ifnum\colour=0
                    \node[label={\i*40-90:\( v_{\i} \)}, fill=MintGreen] (\i) at (\i*40-90:2cm) {};
                \else
                    \ifnum\i=9
                        \node[label={\i*40-90:\( v_{\i} \)}, fill=none] (\i) at (\i*40-90:2cm) {};
                    \else
                        \node[label={\i*40-90:\( v_{\i} \)}, fill=Salmon] (\i) at (\i*40-90:2cm) {};
                    \fi
                \fi
            }
            % Edges
            \foreach \i in {1,...,8} {
                \pgfmathtruncatemacro{\j}{\i+1}
                \draw (\i) -- (\j);
            }
            \draw (9) -- (1);
            \node[graphannotation] (text1) at (0,0) {
                What colour can \\ we put here?
            };
            \draw[grapharrow] (text1) -- (9);
        \end{tikzpicture}
        \caption*{Example: \( C_{9} \) Colouring}
    \end{figure}
\end{frame}

\subsection{Chromatic Number of Bipartite Graphs}

%\begin{frame}
%    \frametitle{A wedding puzzle}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{bunny-wedding.jpg}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{A wedding puzzle}

    You need to arrange seats for guests of a \emoji{wedding}.

    People who are ex-couples, as shown in the graph, cannot sit at the same table.

    Can we do this with \emph{at most} two tables?

    \begin{figure}[htpb]
        \huge
        \centering
        \includegraphics[width=0.8\textwidth]{wedding.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{$m$-colourable graphs}
    
    A graph $G$ is \alert{$n$-colourable} if $\chi(G) \le n$.

    For example,
    \begin{itemize}
        \item $K_n$ is $m$-colourable for any $m \ge \blankshort{}$.
        \item $I_n$ is $m$-colourable for any $m \ge \blankshort{}$.
        \item $S_n$ is $m$-colourable for any $m \ge \blankshort{}$.
        \item $C_n$ is $m$-colourable for any $m \ge \blankshort{}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Is this graph $2$-colourable?}
    The wedding problem is to ask if the following graph is $2$-colourable.

    \begin{figure}[htpb]
        \huge
        \centering
        \includegraphics[width=0.8\textwidth]{wedding.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The first characterization of $2$-colourable graphs}

    A graph is $2$-colourable if and only if it is bipartite.

    \begin{figure}[htpb]
        \centering
        \drawKmn[MintGreen,Salmon]{3}{4}
        \caption*{Example: \( K_{3,4} \) colouring}
        \label{fig:}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The distance in a graph}
    
    The \alert{distance} between two vertices $u$ and $v$,
    denoted by $d(u,v)$,
    is the length of the shortest path from $u$ to $v$.

    \cake{} What are $d(3,3), d(3, 8)$, $d(6, 9)$ and $d(10, 9)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{graph-2}
        \caption*{A graph $G$}
    \end{figure}
\end{frame}

\begin{frame}[label=current]
    \frametitle{Theorem 5.21 (\acs{ac}) --- Another characterization}

    A graph is $2$-colourable/bipartite if and only 
    if it does not contain an \emph{odd} cycle.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \emoji{closed-book} \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 15--17.
            \end{itemize}

            \emoji{green-book} \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 4.4: 1--3, 5, 7, 9, 10, 12-13.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
