\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{dm} 0.2  Mathematical Statements}

\subsection{Atomic and Molecular Statements}

\begin{frame}
    \frametitle{Statements}
    
    A \alert{statement} is any declarative sentence which is either \emph{true
    or false}. 

    These are \alert{atomic} statements
    \begin{itemize}
        \item Telephone numbers in the USA have 10 digits.
        \item $42$ is a perfect square.
        \item The \emoji{crescent-moon} is made of \emoji{cheese}.
        \item Every even number greater than 2 can be expressed as the sum of
            two primes. (Goldbach's conjecture)
        \item $3+7=12$.
    \end{itemize}

    \cake{} Can you think of some statements?
\end{frame}

\begin{frame}[c]
    \frametitle{Examples of non-statements}

    These are \alert{not} statements
    \begin{itemize}
        \item Would you like a \emoji{apple}?
        \item The sum of two squares.
        \item $1+3+5+7+ \cdots + (2n+1)$.
        \item Go to the \emoji{crescent-moon}!
        \item $3+x=12$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Molecular statements}
    
    A statement is \alert{atomic} if it cannot be divided into smaller statements,
    otherwise it is called \alert{molecular}.

    You can build molecular statements using \alert{logical connectives}.

    Binary connectives ---
        \begin{itemize}
            \item Sam is a \boy{} \emph{and} Chris is a \girl{}.
            \item Sam is a \boy{} \emph{or} Chris is a \girl{}.
            \item \emph{If} Sam is a \boy{}, \emph{then} Chris is a \girl{}.
            \item Sam is a \boy{} \emph{if and only if} Chris is a \girl{}.
        \end{itemize}

    Unitary connective
        \begin{itemize}
            \item Sam is \alert{not} a \boy{}. (\laughing{} Maybe Sam is a \emoji{dog}.)
        \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Logical connectives}
    
    \begin{center}
    \begin{tabular}{ c  c  c }
        \toprule
    Name & Notations & Read \\
    \midrule
    Conjunction & $P \wedge Q$ & ``$P$ and $Q$''\\
    Disjunction & $P \vee Q$ & ``$P$ or $Q$''\\
    Implication & $P \imp Q$ & ``if $P$ then $Q$''\\
    Biconditional & $P \iff Q$ & ``$P$ if and only if $Q$''\\
    Negation & $\neg P$ & ``not $P$'' \\
    \bottomrule
    \end{tabular}
    \end{center}

    $P$ and $Q$ are called \alert{propositional variables}.

    \begin{exampleblock}{\bomb{} What matters in Logic}
        Given the \tttrue{}/\ttfalse{} values of $P$ and $Q$,
        what is the \tttrue{}/\ttfalse{} value of a molecular statement?
    \end{exampleblock}
\end{frame}

\begin{frame}[c]
    \frametitle{Truth Conditions for Connectives}

    \begin{center}
    \begin{tabular}{ c  l }
        \toprule
    Statement & When is true? \\
    \midrule
    $P \wedge Q$ & $P=\tttrue{}$ and $Q=\tttrue{}$\\
    $P \vee Q$ & $P=\tttrue{}$ or $Q=\tttrue{}$\\
    $P \imp Q$ &  $P=\ttfalse{}$ or $Q=\tttrue{}$ or both\\
    $P \iff Q$ & $P=Q=\tttrue{}$, or  $P=Q=\ttfalse{}$\\
    $\neg P$ & $P=\ttfalse{}$  \\
    \bottomrule
    \end{tabular}
    \end{center}
    
    \begin{exampleblock}{\cake{} True of false?}
        If the \emoji{first-quarter-moon-face} 
        is made of \emoji{cheese},
        then the \emoji{sun-with-face} is made of \emoji{broccoli}.
    \end{exampleblock}
\end{frame}

\subsection{Implications}

\begin{frame}[c]
    \frametitle{Implications}

    An \alert{implication} is a molecular statement of the form $P \imp Q$.

    We say that
    \begin{itemize}
        \item $P$ is the \alert{hypothesis} (not to be confused with \emph{hypnosis}
            \laughing{}).
        \item $Q$ is the \alert{conclusion}.
    \end{itemize}
\end{frame}

\begin{frame}[c]
    \frametitle{Mathematical Statements}

    Most statements in mathematics are implications.

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.6\textwidth}

            \begin{block}{Pythagorean Theorem}
                If $a$ and $b$ are the legs of a right triangle with hypotenuse $c$, 
                then
                \begin{equation*}
                    a^2 + b^2 = c^2.
                \end{equation*}
            \end{block}

        \end{column}
        \begin{column}{0.4\textwidth}

            \begin{figure}
                \centering
                \includegraphics[width=0.7\textwidth]{Pythagorean.png}
                \caption*{From
                \href{https://commons.wikimedia.org/w/index.php?curid=640875}{Wikipedia}}
            \end{figure}

        \end{column}
    \end{columns}

    \vspace{1em}

    \bomb{} The equation $a^2+b^2=c^2$ itself is \emph{not} a \emph{statement}.
\end{frame}

\begin{frame}[t]
    \frametitle{True or false?}
    \vspace{1em}

    \begin{columns}[t, totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            When is $P \imp Q = \tttrue{}$?
            \begin{itemize}
                \item $P= Q = \tttrue{}$, or
                \item $P=\ttfalse{}$.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            When is $P \imp Q =\ttfalse{}$?
            \begin{itemize}
                \item $P= \tttrue{}$ and $Q = \ttfalse{}$.
            \end{itemize}
        \end{column}
    \end{columns}

    \begin{exampleblock}{\cake{} When am I lying?}
    Assume that I tell you ---

    \begin{quote}
    If you get a 90 on the final, then you will pass the class.
    \end{quote}

    In which case can you call me a liar?
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Does implication equal causality?}
    
    \cake{} Is the following statement true? ---

    \begin{quote}
        If the \emoji{santa} exists,
        then \emoji{panda} can fly.
    \end{quote}

    \cake{} Can we say ``the existence of \emoji{santa}'' \emph{causes}
    ``\emoji{panda} having the ability of flying''?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Which of the following statements are true?
    \begin{enumerate}
        \item If $1=1$, then most \emoji{horse} have 4 legs.
        \item If $0=1$, then $1=1$.
        \item If $8$ is a prime number, then the $7624$th digit of $\pi$ is an $8$.
        \item If the $25$th digit of $\pi$ after the decimal mark is an $3$, then $2+2=5$.
    \end{enumerate}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Direct proof of implications}
%    
%    To prove an implication $P \imp Q$, it is enough to \emph{assume} $P =
%    \tttrue{}$, and from it, deduce $Q = \tttrue{}$.
%    
%    \begin{exampleblock}{Example}
%        Prove that if two numbers $a$ and $b$ are even, then their sum $a + b$ is even.
%    \end{exampleblock}
%\end{frame}

\subsection{Converses and Contrapositives}

\begin{frame}
    \frametitle{Converses}

    The \alert{converse} of $P \imp Q$ is $Q \imp P$.

    \bomb{} An implication \emph{is not} equivalent to its converse.

    \cake{} What is the converse of

    \begin{quote}
        If an integer is greater than 2 is prime, then that number is odd.
    \end{quote}

\end{frame}

\begin{frame}[t]
    \frametitle{Contrapositives}

    The contrapositive of $P \imp Q$ is the statement $\neg Q \imp \neg P$. 

    \bomb{} An implication \emph{is always equivalent} to its contrapositive.


    \cake{} What is the \alert{contrapositive} of

    \begin{quote}
        If you draw at least nine playing cards from a deck,
        then you will have at least three cards all of the same suit
        (\emoji{spade-suit} \emoji{heart-suit} \emoji{diamond-suit}
        \emoji{club-suit}). 
    \end{quote}
\end{frame}

\begin{frame}[t]
    \frametitle{If and only if/Biconditional}
    
    When $P \imp Q$ and $Q \imp P$ are both true, we write 
    $$P \iff Q$$
    which reads
    \begin{equation*}
        P \text{ \alert{if and only if} } Q.
    \end{equation*}

    \begin{example}
        An integer $n$ is even if and only if $n^{2}$ is even.
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Which is \alert{if} which is \alert{only if}?}

    Let 
    \begin{enumerate}
        \item $P$ be \emph{I \emoji{microphone}},
        \item $Q$ be \emph{I'm in the \emoji{shower}}.
    \end{enumerate}
    
    \cake{} Can you complete the table?
    \begin{center}
    \begin{tabular}{ c  c}
        \toprule
    Mathematical Notations & English \\
    \midrule
        $P \iff Q$ & I \emoji{microphone} if and only if I'm in the \emoji{shower}.\\
        $Q \imp P$ & \\
        $P \imp Q$ & \\
    \bottomrule
    \end{tabular}
    \end{center}
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Consider 
%
%    \begin{quote}
%    If you will give me a \emoji{panda}, then I will give you \emoji{unicorn}.
%    \end{quote}
%
%    Are the statements below its \emph{converse}, \emph{contrapositive} or
%    \emph{neither}?
%
%    \begin{enumerate}
%        \small
%        \item If you will give me a \emoji{panda}, then I will not give you \emoji{unicorn}.
%        \item If I will not give you \emoji{unicorn}, then you will not give me a \emoji{panda}.
%        \item If I will give you \emoji{unicorn}, then you will give me a \emoji{panda}.
%        \item If you will not give me a \emoji{panda}, then I will not give you \emoji{unicorn}.
%    \end{enumerate}
%\end{frame}

\subsection{Necessity and Sufficiency}

\begin{frame}
    \frametitle{Necessity and Sufficiency}
    
        ``$P$ is \alert{necessary} for $Q$'' means $Q \imp P$.

        ``$P$ is \alert{sufficient} for $Q$'' means $P \imp Q$.

        If $P$ is \alert{necessary and sufficient} for $Q$, then $P \iff Q$.

    \only<2>{
    \begin{exampleblock}{Necessity}
        For Sam to be a \emoji{rabbit}, it is \alert{necessary} for Sam to be an animal.

        For Sam to be a \emoji{rabbit}, it is \alert{not sufficient} for Sam to be an
            animal. (Sam can be a \emoji{tiger}.)
    \end{exampleblock}

    \cake{} Can you think of another example?
    }

    \only<3>{
    \begin{exampleblock}{Sufficiency}
        For Chris to be a plant, it is \alert{sufficient} for Chris to be a \emoji{sunflower}.

        For Chris to be a plant, it is \alert{not necessary} for Chris to
            be a \emoji{sunflower}. (Chris can be a \emoji{cactus}.)
    \end{exampleblock}

    \cake{} Can you think of another example of
    \only<2>{necessity}\only<3>{sufficiency}?
    }
\end{frame}

\subsection{Predicates and Quantifiers}

\begin{frame}
    \frametitle{Predicates}
    
    Let $P(n)$ denote that $n$ is a prime number.
    Consider
    \begin{equation*}
        P(n) \imp \neg P(n+7).
    \end{equation*}

    A sentence that contains variables is called a \alert{predicate}.

    \pause{}

    This is \alert{not} a statement because $n$ is a \alert{free variable}.

    What we really want is to say
    \begin{itemize}
        \item For all integers $n$, if $n$ is prime, then $n+7$ is not.
    \end{itemize}

    \cake{} Is this a true statement?
\end{frame}

\begin{frame}
    \frametitle{Universal and Existential Quantifiers}
    
    The \alert{existential} quantifier is $\exists$ and is read ``there exists''.
    For example,
    \begin{equation*}
        \exists x(x<0).
    \end{equation*}

    The \alert{universal} quantifier is $\forall$
    and is read ``for all''. For example,
    \begin{equation*}
        \forall x(x \ge 0).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Quantifiers and Negation}

    We have
    $$\neg \forall x P(x) = \exists x \neg P(x).$$

    \cake{}
    What is the negation of 

    \begin{quote}
        All \emoji{horse} are black.
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{Quantifiers and Negation}

    We have
    $$\neg \exists x P(x) = \forall x \neg P(x)$$

    \cake{}
    What is the negation of 

    \begin{quote}
        There exists a \emoji{pig} which can fly
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{Example: Negation of quantifiers}

    Whether the statement
    \begin{equation*}
        \forall x \exists y (y < x)
    \end{equation*}
    is true depends on the \alert{domain of discourse}.

    \cake{} Is it true for \alert{integers}
    \begin{equation*}
        \dsZ = \{\cdots, -2, -1, 0, 1, 2, 3, \cdots\}
    \end{equation*}

    \cake{} What about \alert{natural numbers} 
    \begin{equation*}
        \dsN = \{0, 1, 2, 3, \cdots\}
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{\courseassignment{}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Section 0.2: 1, 3-7, 9-10, 14-16.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
