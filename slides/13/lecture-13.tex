\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Recurrence Equations (1)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 9.1 Introduction to Recurrence Equations}

\begin{frame}
    \frametitle{Review: A game of \emoji{crown}}

    We draw some infinitely long lines as boundaries to divide an infinitely
    large continent into a number kingdoms such that
    \begin{itemize}
        \item \emph{No} point on the continent belongs to more than two lines;
        \item and \emph{no} two lines are parallel.
    \end{itemize}


    \think{} How many kingdoms do we get?

    \begin{figure}
        \centering
        \begin{tikzpicture}[scale=0.7,
            draw=brown,
            every node/.style={scale=1.5}]
            \draw[fill=MintWhisper, draw=none] (0,0) rectangle ++(7,5);
            \draw[red, thick] (0, 0) -- (7, 5);
            \draw[thick] (0, 0) -- (7, 5);
            \draw[red, thick] (7, 0) -- (0, 5);
            \draw[thick] (7, 0) -- (0, 5);
            \draw[red, thick] (0, 3.5) -- (7, 3.5);
            \node at (3.5, 3) {\emoji{mouse-face}};
            \node at (3.5, 1) {\emoji{cow-face}};
            \node at (0.5, 4) {\emoji{tiger-face}};
            \node at (0.5, 2) {\bunny{}};
            \node at (6.5, 2) {\emoji{dragon-face}};
            \node at (6.5, 4) {\emoji{snake}};
            \node at (3.5, 4.5) {\emoji{horse-face}};
        \end{tikzpicture}
        \caption*{$3$ lines and $7$ kingdoms}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Closed form vs recurrence}

    The number of kingdoms we get is \( r_{0} = 1 \) and
    \begin{equation*}
        r_{n} = r_{n-1} + n, \qquad \text{for all } n \ge 1.
    \end{equation*}
    This is called an \alert{recurrence equation}.

    \hint{} We can also consider the sequence \( (r_n)_{n \ge 0} \) as a
    function \( r: \mathbb{N} \mapsto \mathbb{N} \) and write
    \begin{equation*}
        r(n) = 
        \begin{cases}
            1, & \text{if } n = 0, \\
            r(n-1) + n, & \text{if } n \ge 1.
        \end{cases}
    \end{equation*}

    \think{} Can we find a \emph{closed} formula for \( r_n \)?
\end{frame}

\section{\acs{ac} 9.2 Linear Recurrence Equations}

\subsection{The Definition of a Linear Recurrence Equation}

\begin{frame}{Linear recurrence equations}
    A sequence \( (a_{n})_{n\ge 0}\) satisfies a \alert{linear recurrence} if
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
    \]
    where 
    \begin{itemize}
        \item \(k \ge 1\) is an integers,
        \item \(c_{0}, c_{1},\dots,c_{k}\)  are constants, 
        \item with \(c_{0}, c_{k} \ne 0\), 
        \item and \(g\) is a function.
    \end{itemize}

    The \alert{order} of the recurrence is $k$.

    \pause{}

    \cake{} What are $k$, $c_{0}, \dots, c_k$ and $g(n)$ of this equation
    \begin{equation*}
        r_{n} = r_{n-1} + n,
        \qquad
        (\text{for all } n \ge 1).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\zany{} The constant \( c_0 \) and \( c_k \)}

    Consider the general \( k \)-th order linear recurrence:
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
    \]
    
    When \( c_{0} = 0 \), the recurrence simplifies to:
    \[
        c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
    \]
    
    Similarly, when \( c_{k} = 0 \), it becomes:
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k-1} a_{n} = g(n),
    \]

    \hint{} To uniquely define an \( k \)-th order linear recurrence, both \( c_{0} \) and \( c_{k} \) must be nonzero.
\end{frame}

\subsection{Homogeneous and Nonhomogeneous Recurrence Equations}

\begin{frame}[c]
    \frametitle{\zany{} Understanding Homogeneity}
    
    \begin{columns}[c]
        % Column 1: Etymology
        \begin{column}{0.5\textwidth}
            The term \emph{homogeneous} originates from two Greek words:
            \begin{itemize}
                \item \textbf{Homos:} Meaning "same" or "similar"
                \item \textbf{Genos:} Meaning "kind" or "type"
            \end{itemize}
            
            Together, they form "homogeneous," indicating things that are of the same or similar kind.
        \end{column}
        
        % Column 2: Funny Bunny Picture
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-identical.jpg}
                \caption*{Homogeni-bunnies: Different bunnies, same vibes.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{homogeneous and nonhomogeneous recurrence}
    The recursion is \alert{homogeneous} if \(g(n)\) is always \(0\), like the
    Fibonacci sequence:
    \[
        f_{n+2} - f_{n+1} - f_n = 0.
    \]
    Otherwise it is \alert{nonhomogeneous}, like the \emoji{crown} sequence:
    \[
        r_{n+1}-r_{n}=n+1.
    \]
\end{frame}

\subsection{Advancement operator}

\begin{frame}
    \frametitle{Advancement operators}

    An \alert{operator} is a function that maps a function to another function.

    The \alert{advancement operator} \( A \) is defined as:
    \begin{equation*}
        A f(n) = f(n + 1)
    \end{equation*}

    We can generalize this to \( A^{p} \):
    \begin{equation*}
        A^{p} f(n) = f(n + p)
    \end{equation*}

    \cake{} Consider these simple functions \( f \):
    \begin{itemize}
        \item \( f(n) = n \)
        \item \( f(n) = n^2 \)
        \item \( f(n) = 2^n \)
    \end{itemize}
    What are \( A f \) and \( A^{2} f \) for each?
\end{frame}

\begin{frame}
    \frametitle{Write the Fibonacci sequence with advancement operators}
    
    The Fibonacci sequence recursion
    \[
        f(n + 2) - f(n + 1) - f(n) = 0
    \]
    can be rewritten as
    \[
        A^{2} f(n) - A f(n) - A^{0} f(n) = 0
    \]
    or equivalently
    \[
        (A^{2} - A - 1) f = 0
    \]
\end{frame}

\begin{frame}
    \frametitle{Write linear recurrences via advancement operators}

    Consider the general \( k \)-th order linear recurrence:
    \[
        c_{0} f(n+k) + c_{1} f(n+k-1) + \cdots + c_{k} f(n) = g(n),
    \]
    
    This can be succinctly expressed using the Advancement operator \( A \):
    \[
        p(A) f = (c_{0} A^{k} + c_{1} A^{k-1} + \cdots + c_{k}) f = g(n).
    \]
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Consider the following linear recurrences:
    \begin{enumerate}
        \item \( f(n+3) = 2f(n+2) + 3f(n+1) - f(n) \)
        \item \( g(n+1) = -g(n) \)
        \item \( h(n+2) = h(n+1) + 2h(n) \)
    \end{enumerate}

    Can you rewrite these equations using the Advancement operator \( A \)?
\end{frame}

\begin{frame}
    \frametitle{A property advancement operator}

    If $p(A) = q(A)$ holds as \emph{polynomials} of the variable $A$,
    then $p(A)f = q(A)f$ as \emph{functions}.

    \begin{example}
        Let us verify this:
        \begin{equation*}
            (A-2)(A + 3) f = (A^{2} + A - 6)f.
        \end{equation*}
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Another property advancement operator}
    
    We also have $p(A)(f + g) = p(A) f + p(A) g$.
    \begin{example}[]
        Let us verify this:
        \begin{equation*}
            (A-2)(f + g) = (A-2) f + (A-2) g
        \end{equation*}
    \end{example}
\end{frame}

\section{\acs{ac} 9.4.1 Solving Advancement Operator Equations -- Homogeneous Case}

\subsection{The Fundamental Theorem of Algebra}

\begin{frame}[t]{A trivial example}
    Find \alert{all} solutions for
    \[
        (A-2) f(n) = 0
    \]

    \pause{}

    Find \alert{all} the solutions for 
    \[ 
        (A+3) f(n) = 0
    \]
\end{frame}

\begin{frame}[t]{Example 9.9}
    Can you show that the function
    \begin{equation*}
        c_{1} 2^{n} + c_{2} (-3)^{n}
    \end{equation*}
    with any $c_1, c_2 \in \dsR$ is a solution of
    \begin{equation}
        \label{eq:homogeneous:1}
        (A^{2}+A-6) f 
        = 0
        .
    \end{equation}

    \think{} Are \emph{all} solutions of this form?
\end{frame}

\begin{frame}
    \frametitle{Polynomial roots and the advancement operator}

    Given a polynomial \( p(A) \) of degree \( k \), if there exist roots \( r_1, \dots, r_k \) in \( \mathbb{C} \),
    \[
        p(A) = (A - r_{1}) \dots (A - r_{k}),
    \]
    then these numbers \( r_1, \dots, r_k \) are called the \alert{roots} of \( p(A) \).

    \begin{block}{The fundamental theorem of algebra}
        A polynomial of degree \( n \) has \( n \) roots in \( \mathbb{C} \).
        \href{https://en.wikipedia.org/wiki/Fundamental\_theorem\_of\_algebra}{Proof}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Examples of polynomial roots}

    Some polynomials have \emph{distinct} roots:
    \begin{itemize}
        \item \( x^2 + x - 6 = (x + 3)(x - 2) \)
        \item \( x^2 + 1 = (x + \mathbb{i})(x - \mathbb{i}) \)
    \end{itemize}

    Some have \emph{multiple} roots:
    \begin{itemize}
        \item \( x^2 + 2x + 1 = (x + 1)^{2} \)
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{High school math}

    Recall that the two roots of
    \begin{equation*}
        x^2 + b x+c
    \end{equation*}
    are
    \begin{equation*}
        \frac{-b \pm \sqrt{b^2-4 c}}{2}
    \end{equation*}

    \cake{} What are the roots of
    \begin{equation*}
        x^2 + 10 x + 21
    \end{equation*}
\end{frame}

\subsection{When $p(A)$ has \emph{distinct} roots}

\begin{frame}
    \frametitle{Theorem 9.21 (AC)}

    Let \( p(A) \) be a polynomial with \emph{distinct} roots \( r_{1}, \dots,
    r_{n} \). Then, for any constants \( c_1, \dots, c_k \) in \( \mathbb{R} \),
    the expression
    \begin{equation*}
        c_{1} r_{1}^{n} + c_{2} r_{2}^{n} + \cdots + c_{k} r_{k}^{n}
    \end{equation*}
    is a solution to \( p(A) f = 0 \).

    In other words, 
    \begin{equation*}
        p(A) (c_{1} r_{1}^{n} + c_{2} r_{2}^{n} + \cdots + c_{k} r_{k}^{n}) = 0, 
        \qquad (\text{for all } n \in \mathbb{N})
        .
    \end{equation*}

    Moreover, \alert{every solution} to \( p(A) f = 0 \) can be written in this form.
\end{frame}

\begin{frame}
    \frametitle{Example 9.10}
    
    Can you find the solution for
    \begin{equation*}
        t_{n+2} = 3 t_{n+1} - t_n, \qquad \text{ for } n \ge 1,
    \end{equation*}
    which satisfies both the initial conditions
    \begin{equation*}
        t_1 = 3, \qquad t_2 = 8.
    \end{equation*}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\zany{} \emoji{robot} to help}
    
    Such problem can (of course) be solved with WolframAlpha with 
    the \href{https://www.wolframalpha.com/input?i=solve+t\%5Bn+\%2B+2\%5D+\%3D\%3D+3+t\%5Bn+\%2B+1\%5D+-+t\%5Bn\%5D\%2C+t\%5B1\%5D\%3D\%3D3\%2C+t\%5B2\%5D\%3D\%3D8}{query}
    \begin{small}
\begin{verbatim}
solve t[n+2]==3*t[n+1] - t[n], t[1]==3, t[2]==8
\end{verbatim}
    \end{small}

    And the result is
    \begin{equation*}
    t(n) = -\frac{2 \sqrt{5} \left(\frac{3}{2} - \frac{\sqrt{5}}{2}\right)^n - 15 \left(\frac{3}{2} + \frac{\sqrt{5}}{2}\right)^n - 7 \sqrt{5} \left(\frac{3}{2} + \frac{\sqrt{5}}{2}\right)^n}{5 (3 + \sqrt{5})}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}

    Consider the following recurrence
    \begin{equation*}
        t_n = t_{n-1} + t_{n-2} \qquad (n \ge 2).
    \end{equation*}

    Write this recurrence with advanced operator $A$ and
    find all solutions for this recurrence equation.
\end{frame}

\begin{frame}[t]
    \frametitle{Initial conditions}

    $t_n$ can be seen as the number of ways to tile a $2 \times n$ chessboard
    using only dominoes \tikz[scale=0.3]{\drawtile{1}{2}} (rotation allowed).
    \begin{figure}[htpb]
    \begin{center}
        \begin{tikzpicture}[scale=1, transform shape]
            \drawchessboard{2}{5}
            \end{tikzpicture}
    \end{center}
    \caption*{Example of tiling a chessboard with \tikz[scale=0.3]{\drawtile{1}{2}}}
    \end{figure}

    In other words, we have the following initial conditions:
    \begin{equation*}
        t_{0}=1, \qquad t_{1}=1.
    \end{equation*}

    \puzzle{} Find $(t_{n})_{n \ge 0}$ which that satisfies both these conditions
    and the recurrence $t_n = t_{n-1} + t_{n-2}$?
\end{frame}

\subsection{When $p(A)$ has \emph{multiple} roots}

\begin{frame}
    \frametitle{Example 9.12}
    
    Can we find all solutions to
    \begin{equation*}
        (A − 2)^2 f = 0.
    \end{equation*}

    \cake{} Why $c_1 2^n + c_2 2^n$ \emph{probably} does not give all the solutions?
    
    \think{} What if we try $c_2 n 2^n$?

    \pause{}

    \cake{} Can you guess the general solution for
    \begin{equation*}
        (A-2)^3 f = 0
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Dealing with multiple roots}
    
    \cake{} Observing that
    \begin{equation*}
        (A-r) (r^n) = 0, \quad
        (A-r)^2 (n r^n) = 0, \quad
        (A-r)^3 (n^{2} r^n) = 0, \dots
    \end{equation*}
    can you guess that the general solution of
    \begin{equation*}
        (A - r)^{p} f = 0
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.13}

    Can you find the solution for
    \begin{equation*}
        (A+5)(A-1)^3 f = 0
    \end{equation*}
    which satisfies both the initial conditions
    \begin{equation*}
        f_{0} = 1, f_{1} = 2, f_{2} = 4, f_{3} = 4.
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Let $d_{n}$ be the number of strings of length $n$ on the alphabet 
%    $\{\temoji{apple}, \temoji{banana}, \temoji{cherries}\}$ with even numbers of
%    \emoji{cherries}.
%
%    Can you explain why
%    \begin{equation*}
%        d_{n + 1} = 2 d_n + (3^{n-1} - d_{n}),
%        \qquad (n \ge 1)
%    \end{equation*}
%
%    \hint{} We will learn how to deal with nonhomogeneous linear recurrence in
%    the next lecture.
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 9.9: 1, 3, 5, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
