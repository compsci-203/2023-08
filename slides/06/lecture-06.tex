\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 2.4 Combinatorial Proofs}

\subsection{Summation formulas}

\begin{frame}
    \frametitle{Sum of the first \(n\) positive integers}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            How to prove that for $n \ge 1$
            \[
                1+2+\dots+n = \frac{n (n+1)}{2}
            \]
            using a combinatorial argument?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{sum-1.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Sum of the first \(n\) positive odd integers}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            How to prove that for $n \ge 1$
            \[
                1+3+\cdots+2 n -1 = n^2
            \]
            using a combinatorial argument?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{sum-2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Fibonacci numbers}

\begin{frame}
    \frametitle{Fibonacci numbers and tiling a board}
    
    Let $f_n$ be the number of ways to tile a $1 \times n$ grid like this
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \drawchessboard{1}{8} % Draws a 1x8 chessboard
        \end{tikzpicture}
    \end{figure}
    using only dominoes \tikz[scale=0.3]{\drawtile{1}{2}} (of size $1 \times 2$) and
    squares \tikz[scale=0.3]{\drawtile{1}{1}} (of size $1 \times 1$).

    How can we show that
    \begin{equation*}
        f_{n+2} = f_{n+1} + f_{n} \qquad (n \ge 1)
    \end{equation*}

    The number $f_{n-1}$ is also known as the $n$-th
    \href{https://oeis.org/wiki/Fibonacci\_numbers}{Fibonacci Number}.
\end{frame}

\begin{frame}
    \frametitle{\zany{} What is $f_0$?}
    
    Given a recursive equation like
    \begin{equation*}
        f_{n+2} = f_{n+1} + f_{n} \qquad (n \ge 1)
    \end{equation*}
    and initial values $f_1 = 1, f_2 = 2$,
    we often say that $f_0 = 1$ so that the recursive equation becomes
    \begin{equation*}
        f_{n+2} = f_{n+1} + f_{n} \qquad (\alert{n \ge 0})
    \end{equation*}

    From a combinatorial perspective, there is one way to tile a $1 \times 0$
    grid --- do nothing at all.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Can you find a combinatorial proof that
    \begin{equation*}
        f_{2n} = f_{n}^2 + f_{n-1}^{2} \qquad (n \ge 1)
    \end{equation*}
    where $f_{n}$'s are the Fibonacci numbers?

    \hint{} Consider the following two cases.
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}
                \drawchessboard{1}{8} % Draws a 1x8 chessboard
                \begin{scope}[xshift=3cm]
                    \drawtile{1}{2}
                \end{scope}
            \end{tikzpicture}
        \end{center}
        \begin{center}
            \begin{tikzpicture}
                \begin{scope}
                    \drawchessboard{1}{4} % Draws a 1x4 chessboard
                \end{scope}
                \begin{scope}[xshift=4.2cm]
                    \drawchessboard{1}{4} % Draws a 1x4 chessboard
                \end{scope}
            \end{tikzpicture}
        \end{center}
    \end{figure}
\end{frame}

\subsection{Combinatorial identities}

\begin{frame}[t]
    \frametitle{Example 2.17}

    Prove that
    \[
        \binom{n}{0}+\binom{n}{1}+\dots+\binom{n}{n}=2^n.
    \]
\end{frame}

\begin{frame}[t]
    \frametitle{Example 2.19}
    Prove that
    \[
        \binom{n}{0}2^{0}+\binom{n}{1}2^{1}+\dots+\binom{n}{n}2^{n}=3^n
    \]
    %\hint{} Both sides count choosing the number of binary and ternary strings of length
    %$n$ respectively.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 2.18}
    Find a combinatorial proof for
    \[
        \binom{n}{k+1}
        =
        \binom{k}{k}
        +
        \binom{k+1}{k}
        +
        \binom{k+2}{k}
        +
        \dots
        +
        \binom{n-1}{k}
        .
    \]
    \begin{block}{Proof}
        \begin{overlayarea}{\textwidth}{5cm} % Width and height specified
            \only<1>{
                The \ac{lhs} is the number of strings consisting of $k+1$ \emoji{apple}
                and $n-(k+1)$ \emoji{banana}.
            }
            \only<2>{
                The \ac{rhs} is also the number of strings consisting of $k+1$ \emoji{apple}
                and $n-(k+1)$ \emoji{banana}.
            }
        \end{overlayarea}
    \end{block}
\end{frame}

%\begin{frame}[t]
%    \frametitle{\acs{ac} Example 2.2.0 (\acs{dm} Example 1.4.7)}
%    Prove
%    \[
%        \binom{2n}{n} = \binom{n}{0}^{2} + \binom{n}{1}^{2} + \dots \binom{n}{n}^{2}
%    \]
%    we can first look at the example of $n = 3$
%    \begin{equation*}
%        \binom{2 \times 3}{3} 
%        = 
%        \binom{3}{0}^{2} + \binom{3}{1}^{2} + \binom{3}{2}^{2} + \binom{3}{3}^{2}
%    \end{equation*}
%
%    \hint{} The \ac{lhs} is the number of ways to line up three \emoji{apple} and three \emoji{banana}.
%\end{frame}

%\begin{frame}[t]
%    \frametitle{Combinatorial identities 3}
%    Prove that for \(k > 0\),
%    \[
%        \binom{n}{k}
%        =
%        \frac{n}{k}
%        \binom{n-1}{k-1}
%    \]
%    This is equivalent to
%    \[
%        \binom{k}{1} \binom{n}{k}
%        =
%        \binom{n}{1}
%        \binom{n-1}{k-1}
%    \]
%    \hint{} Both sides count ways of choosing one \emoji{crown} and $k-1$ ministers from $n$ people.
%
%    %\practice{}
%\end{frame}

%\begin{frame}[t]
%    \frametitle{Combinatorial identities 5}
%    Prove
%    \[
%        \sum^{n}_{i=1}\binom{n}{i}i=n2^{n-1}
%    \]
%
%    \hint{}  Both sides are the sum of the sizes of subsets of \([n]\).
%
%\end{frame}
%
%\begin{frame}
%    \frametitle{Combinatorial identities 6}
%
%    \begin{alertblock}{Quiz --- sum of squares}
%        \[
%            \sum_{k=1}^n k^2 
%            =\binom{n+1}{2} + 2 \binom{n+1}{3}
%        \]
%    \end{alertblock}
%
%
%    \alert{Hint} Both sides count the number of ordered triples $(i,j,k)$ with condition (what to
%    put here?).
%
%    %
%    \begin{block}{Answer}
%        Both sides count the number of ordered triples $(i,j,k)$ with condition $0 \le i,j < k \le n$.
%        See \href{https://math.stackexchange.com/q/95047/1618}{here}.
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Combinatorial identities 7}
%
%    \begin{alertblock}{Quiz --- sum of squares}
%        \[
%            \sum_{k=1}^n k^3 = \binom{n+1}{2}^2
%        \]
%    \end{alertblock}
%
%
%    LHS counts the number of ordered 4-tuple $(h, i,j,k)$ with condition $0 \leq h,i,j < k
%    \leq n$.  
%
%    RHS counts $(x_1,x_2), (x_3,x_4)$ with $0 \leq x_1 < x_2 \leq n$ and $0 \leq x_3 < x_4 \leq n$.  
%    
%    Need a bijection between both sides.
%\end{frame}

%\begin{frame}
%    \frametitle{Bijections in combinatorics}
%
%    If there is a bijection between two sets, then they must have the same number of elements.
%
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.4\textwidth]{bijection.png}
%    \end{figure}
%
%    \hint{}  If all you left/right \emoji{shoe} is pair with exactly one right/left \emoji{shoe}, then you
%    must have the same number of left and right \emoji{shoe}s.
%\end{frame}

%\begin{frame}
%    \frametitle{Combinatorial identities 7 continued}
%
%    A bijection between \( (h,i,j,k)\) and \( (x_{1},x_{2}), (x_{3},x_{4}) \)
%    \begin{itemize}
%        \item If \(h<i\), then \( (h,i), (j,k) \Leftrightarrow x_{2} < x_{4}\).
%        \item If \(h>i\), then \( (j,k), (i,h) \Leftrightarrow x_{2} > x_{4}\).
%        \item If \(h=i\), then \( (h,k), (j,k) \Leftrightarrow x_{2} = x_{4}\).
%    \end{itemize}
%
%    See \href{https://math.stackexchange.com/q/95047/1618}{here}.
%\end{frame}

\begin{frame}[c]
    \frametitle{Computer assisted proof}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Many combinatorial identities can be \emph{proved} by
            \href{https://www.wolframalpha.com/input/?i=sum\_(k\%3D0)\%5En+binomial(n,+k)\%5E2+binomial(3+n+\%2B+k,+2+n)}{computers}.

            \medskip{}

            \href{http://sites.math.rutgers.edu/~zeilberg/}{Doron Zeilberger} made great
            contributions.

            \medskip{}

            \laughing{} Check his blog \href{http://sites.math.rutgers.edu/~zeilberg/}{here}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Zeilberger.jpg}
                \caption*{Doron Zeilberger}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 2.5 The Ubiquitous Nature of Binomial Coefficients}

\subsection{Integer Composition}

\begin{frame}
    \frametitle{Amanda's \carrot{}}

    Amanda wants to give 3 \bunny{} 10 \carrot{}, 
    so that every \bunny{} has \(>0\) \carrot{}.

    In how many ways can she do it?

    We can make the problem more interesting by adding constraints
    \begin{itemize}
        \item Identical/distinct \carrot{}
        \item Identical/distinct \bunny{} 
        \item At least $a$ \carrot{} each \bunny{}
        \item At most $b$ \carrot{} each \bunny{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The argument}

    Put \(10\) \alert{identical} \carrot{} in a line.

    Insert \(2\) \pepper{} into the gaps in the line, e.g.,
    \begin{itemize}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \pepper{}  \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{}  \carrot{} \carrot{} \carrot{}
    \end{itemize}

    \cake{} In how many ways can we use this method to separate $10$ \carrot{}
    into \(3\) non-empty piles? 

    \hint{} We need to avoid the following cases
    \begin{itemize}
        \item \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \pepper{} \carrot{} \carrot{} \carrot{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The formula}
    Amanda want to give \(m\) \alert{identical} \carrot{} and \(n\) \alert{distinct}
    \bunny{}.

    To do this, she puts \(m\) \alert{identical} \carrot{} in a line.

    By inserting \blankshort{} \pepper{} into the \blankshort{} gaps in the line,
    she gets \(n\) non-empty piles of \carrot{}.

    \cake{} Thus, the number of ways to do this is
    \[
        \binom{\blankshort{}}{\blankshort{}}
    \]
\end{frame}

\begin{frame}
    \frametitle{Integer composition}

    This type of \carrot{} problems is called \alert{integer composition} .

    It is equivalent to ask for the number of integer solutions of
    \[
        x_{1}+x_{2}+\dots + x_{n} = m
    \]
    with $x_{i} > 0$ for all $i \in [n]$.
\end{frame}

\begin{frame}[t]
    \frametitle{When \bunny{} is allowed to be empty-handed}

    In how many ways can Amanda distribute her 10 \carrot{} to 3 \bunny{},
    if she is \emph{allowed} to give no \carrot{} to any \bunny{}.

    We can count the ways of inserting two \pepper{} 
    in $11$ gaps and allowing that the two \pepper{}
    is at the same gap. 

    Consider two cases ---

    \begin{itemize}
        \item \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \pepper{} \pepper{}  \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{}  \carrot{} \carrot{} \carrot{}
    \end{itemize}

    So the answer is
    \begin{equation*}
        \binom{11}{2} + 11
        =
        66
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Taking one \carrot{} back}
    
    Now Amanda wants to give her 3 \bunny{} 10 + 3 \carrot{}, 
    so that everyone has \(>0\) \carrot{}.

    \think{} In how many ways can she do it? Does it ring a \emoji{bell}?
\end{frame}

\begin{frame}
    \frametitle{Proof by bijection}

    How to use a bijection to prove that 
    the number of integer solutions for
    \[
        x_{1}+x_{2}+\dots + x_{n} = m
    \]
    with $x_{i} \ge 0$ for all $i \in [n]$,
    is the same as the number of integer solutions for
    \[
        x_{1}'+x_{2}'+\dots + x_{n}' = m+n
    \]
    with $x_{i}' > 0$ for all $i \in [n]$?

    \cake{} What is this number?
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}
    
    Find the number of integer solutions to the inequality
    \begin{equation*}
        x_{1}+x_{2}+x_{3}+x_{4} + x_{5} + x_{6} \le 38
    \end{equation*}
    with $x_{i} > 0$ for $i \in [6]$.

    \hint{} This is the same as the number of integer solutions to
    \begin{equation*}
        x_{1}+x_{2}+x_{3}+x_{4} + x_{5} + x_{6} + x_{7} = \blankshort{}
    \end{equation*}
    with $x_{i} > 0$ for $i \in [7]$.
\end{frame}

\subsection{Integer Partition}

\begin{frame}
    \frametitle{Amanda's money again}

    Amanda wants to divide her 10 \carrot{} into non-empty piles.

    She does not care the number of piles and their order.

    For example, the following are counted only once ---

    \begin{itemize}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{}\pepper{}  \carrot{} \carrot{}
    \end{itemize}
    In how many way can Amanda do this?
\end{frame}

\begin{frame}
    \frametitle{Partitions of an integer}
    For a fixed integer \(m\),
    an equation in the form of
    \[
        m = a_{1}+a_{2}+\dots a_{n}
    \]
    such that \(a_{1} \ge a_{2} \ge \dots a_{n} \ge 1\) are integers
    is called a \alert{partition} of $m$.

    So the previous problem is call the \alert{partitions} of $10$.
    
    \bomb{} Unfortunately, there does not exist a simple formula for partition
    numbers. 
\end{frame}

\begin{frame}
    \frametitle{Example --- partitions of $4$}

    \cake{} Can you find all the partitions of $4$?
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Two giants who studied integer partitions}
    \begin{columns}[c]
        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]{hardy.jpg}
            \caption*{G. H. Hardy (1877-1947)}
        \end{figure}

        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]{Ramanujan.jpg}

            \caption*{Srinivasa Ramanujan (1887-1920)}
        \end{figure}

    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A film about Ramanujan}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The story of Ramanujan and Hardy is 
            told in the film
            \href{https://youtube.com/watch?v=QqRnkFXuMmo}{The Man Who Knew Infinity}.

            \medskip{}

            In one scene from the film, 
            Hardy explained $4$ can be partitioned in five ways.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\linewidth]{the-man-who-knew-infity.jpg}
            \end{figure}
        \end{column}
    \end{columns}
    
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 2.9: 15, 17, 19, 21.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
