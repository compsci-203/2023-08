\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 2.1 Strings}

\begin{frame}
    \frametitle{Strings}
    
    A sequence of length \(n\) like \( (a_{1},a_{2},\dots,a_{n})\) is called a
    \alert{string}.

    The entries in a string are called \alert{characters} or \alert{letters}.

    The set of possible entries is called the \alert{alphabet}.

    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{words.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Examples of strings}

    \cake{} What are some possible alphabets of the following strings?
    \begin{itemize}
        \item $010010100010110011101$ --- a bit/binary string.

        \item $201002211001020$ --- a ternary string.

        \item
            \emoji{apple}\emoji{banana}\emoji{apple}\emoji{banana}\emoji{cherries}\emoji{banana}\emoji{dumpling} --- my breakfast of a week

        \item ababcbd

        \item \texttt{KSF762} --- an European vehicle license plate

        \item $(34, 53, 3, 43, 54, 64, 7)$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Strings are functions}
    
    A string of length \(n\) on the alphabet \(\mathcal A\) is a function from \([n]\) to \(\mathcal A\).

    \begin{example}
        The string ababcbd can be seen as the function $f:[7]\mapsto\{\text{a, b, c, d}\}$
        where
        \begin{equation*}
            f = 
            \begin{pmatrix}
                1 & 2 & 3 & 4 & 5 & 6 & 7 \\
                  &   &   &   &   &   &  
            \end{pmatrix}
        \end{equation*}
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Notations for strings}

    The string \(\left( 2,5,8,11 \right) \) can be seen as a function \(f:[4]\to[12]\) defined by \(f(n)=3n-1\).

    Such a string is often written as \( (a_1,a_{2},a_{3},a_{4}, \dots, a_{m})\) with
    \(a_{n}=3n-1\) for $n \in [m]$.

    \cake{} The following string
    \begin{equation*}
        (2, 5, 10, 17)
    \end{equation*}
    is defined by $a_{n} = \blankshort{}$ for $n \in [4]$.
\end{frame}

\begin{frame}
    \frametitle{Strings and CPU}
    
    A machine instruction for a $64$-bit CPU is a bit string of length $64$.

    \cake{} What is the number of such strings?

    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{cpu.jpg}
    \end{figure}

\end{frame}

\begin{frame}[c]
    \frametitle{My breakfast of a week}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            I eat one type of fruit among $\{\temoji{apple}, \temoji{banana},
            \temoji{cherries}\}$ for breakfast.

            \medskip{}

            \cake{} How many different ways can I arrange breakfast for a week?

            \medskip{}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{apple-banana-cherry.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The number of strings}

    There is a bijection between the set of strings of length
    $7$ and my breakfast of a week, e.g.,
    \begin{itemize}
        \item 
            \emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}
            --- Having \emoji{apple} everyday.
        \item 
            \emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{cherries}\emoji{cherries}
            --- \emoji{apple} for weekdays and \emoji{cherries} for weekends.
        \item 
            \emoji{apple}\emoji{banana}\emoji{cherries}\emoji{apple}\emoji{banana}\emoji{cherries}\emoji{apple}
            --- Rotating \emoji{apple} and \emoji{banana} and \emoji{cherries}.
    \end{itemize}

    \cake{} What is the number of words of length $7$ from an $3$ letter
    alphabet?

    \cake{} What is the number of words of length $n$ from an $m$ letter
    alphabet?
\end{frame}

\begin{frame}[c]
    \frametitle{My lunch}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            I pack a sandwich for lunch.

            My sandwich consists of
            \begin{enumerate}
                \item One type of bread: \emoji{croissant} \emoji{bread}
                    \emoji{bagel}
                \item One type of fillings: \emoji{falafel} \emoji{eggplant}
                    \emoji{mushroom} \emoji{avocado}
                \item One type of sauce: ketchup, mayonnaise, mustard
            \end{enumerate}

            \cake{} How many different types of sandwiches can I make?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bagel.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{The number of strings with restrictions}
    The \emoji{sandwich} problem is equivalent to ask how many strings of length $3$ are
    there, such that
    \begin{itemize}
        \item the first position has $3$ options,
        \item the second position has $4$ options,
        \item the last position has $3$ options.
    \end{itemize}

    \cake{} What is the answer then?

    \cake{} In general, if we restrict the number of possible letters for position $i$ to
    \(m_{i}\),
    what is the total number of such strings?
\end{frame}

%\begin{frame}
%    \frametitle{Strings with restrictions}
%
%    In \emoji{sweden}, 
%    a vehicle plate has three letters, 
%    followed by three digits.
%    The letters I, Q, V are not allowed.
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.4\textwidth]{license.jpg}
%    \end{figure}
%
%    \cake{} How many such vehicle plates can there be?
%\end{frame}

\begin{frame}
    \frametitle{The number of passwords}

    How many passwords satisfy
    \begin{itemize}
        \item The first letter is an upper-case letter
        \item The second to the sixth character must be a letter or a digit
        \item The seventh must be either @ or .
        \item The eighth through twelfth positions allow lower-case letters, *, \%, and \#
        \item The thirteenth position must be a digit
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The number of passwords --- solution}

    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{password.jpg}
    \end{figure}


    So the number of possible password is
    \[
        26 \times 62^5 \times 2 \times 29^{5} \times 10
        =
        9771287250890863360
        \approx
        9.77 \times 10^{18}.
    \]
\end{frame}

%\begin{frame}
%    \frametitle{How good is your intuition with large numbers?}
%    
%    If a \emoji{robot} can guess $10^3$ password per second, 
%    how many years will it take to try all 
%    $9.77 \times 10^{18}$ passwords?
%
%    \begin{enumerate}
%        \item Less than $10^3$.
%        \item Between than $10^3$ to $10^6$.
%        \item Between than $10^6$ to $10^9$.
%        \item More than $10^9$.
%    \end{enumerate}
%\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A bad password}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            People often choose a password by substituting, e.g.,
            \begin{equation*}
                \text{Trobador}
                \Rightarrow
                \text{Tr0b4dor}
            \end{equation*}
            The number of such password is only $(26+10)^{8} \approx 2.8 \times 10^{12}$.

            \medskip{}

            \cake{} If a \emoji{robot} can guess $10^6$ password per second, 
            how many days will it take to guess this password?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{troubadour.jpg}
                \caption*{A trobador}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A good password}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            But if you choose four random English words, e.g.,
            \begin{center}
                correct horse battery staple
            \end{center}
            then you have about $20,000^4 \approx 1.6 \times 10^{17}$.

            \medskip{}

            It takes $10^7$ times more time for a \emoji{robot} to guess this password.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{correct-horse-battery-staple.jpg}
                \caption*{correct \emoji{horse-face} \emoji{battery} staple}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{\laughing{} My WiFi Password}
%
%    Can you guess what is my mom's WiFi password?
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{wifi-password.jpg}
%    \end{figure}
%\end{frame}

\section{\acs{ac} 2.2 Permutations}

\begin{frame}[c]
    \frametitle{Letters from a bag}

    \begin{columns}[c]
        \column{0.5\textwidth}

        \setlength{\parskip}{10pt}

        Put the 26 letters of English alphabet in a bag.

        Take six letters out one by one, without replacement.

        This makes a string of length six.

        \cake{} Could this string be \alert{yellow}?

        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{letter.jpg}
        \end{figure}


    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{What is a permutation?}

    A \alert{permutation} is a string without \alert{repetition} of letters.

    \cake{} Which of the following strings are permutations?

    \begin{itemize}
        \item $(12, 7, 8, 6, 4, 9, 11)$
        \item $(X, y, a, A, D, 7, B, E, 9)$
        \item $(5, b, 7, 2, 4, 9, A, 7, 6, X)$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Factorials}
    
    For $m \in \dsN$, we define 
    \begin{equation*}
        m!
        =
        \begin{cases}
            1 & m = 0 \\
            m (m-1)! & m > 0
        \end{cases}
    \end{equation*}

    We call $m!$ the \alert{factorial of $m$}.

    \cake{} Thus we have
    \begin{align*}
        & 0! = \blankshort{} \\
        & 1! = \blankshort{} \\
        & 2! = \blankshort{} \\
        & 3! = \blankshort{} \\
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{The number of permutations}

    Let $P(m, n)$ denote the number of permutations of length $n$ for an \(m\)-letter
    alphabet.
    
    How can we write $P(m, n)$ with factorials?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    In \emoji{sweden}, 
    a vehicle plate has three letters, 
    followed by three digits.
    The letters I, Q, V are not allowed.

    \alert{In addition, the three letters must be distinct.}
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{license.jpg}
    \end{figure}

    \cake{} How many such vehicle plates can there be?
\end{frame}

%\begin{frame}
%    \frametitle{An election}
%
%    \begin{columns}
%        \column{0.5\textwidth}
%
%        A group of 40 students holds elections to select a prime minister,
%        a deputy prime minister,
%        and a minister for finance. 
%
%        Assuming that a student \alert{cannot} hold more than one position, how many different outcomes are possible?
%
%        \column{0.5\textwidth}
%
%        \begin{figure}
%            \centering
%            \includegraphics[width=0.5\textwidth]{vote.jpg}
%        \end{figure}
%    \end{columns}
%\end{frame}

\section{\acs{ac} 2.3 Combinations}

\begin{frame}
    \frametitle{Why it is hard to choose food?}

    A restaurant offers
    \emoji{hamburger}\emoji{pizza}\emoji{fries}.

    We want to order 2 different foods.

    \cake{} If we care about the \emph{order} of the food, in how many ways
    can we do this?

    \pause{}

    What if we do \alert{not} care about the order?
\end{frame}

\begin{frame}
    \frametitle{The number of combinations}

    The restaurant problem is the same as asking if we can choose \( n \) different
    letters from an \( m \)-letter alphabet, regardless of their order, in how many ways can we do it.
    
    This is called the number of \alert{combinations} of \( n \) letters taken from
    an \( m \)-letter alphabet,
    denoted by \( C(m,n) \).

    How can we compute \( C(m,n) \)?
\end{frame}

\begin{frame}[t]
    \frametitle{Proposition 2.9}

    For \(0 \le n \le m\)
    \[
        C(m,n)
        = 
        \frac{P(m,n)}{n!}
        =
        \frac{m(m-1)\dotsm(m-n+1)}{n!}
        =
        \frac{m!}{n!(m-n)!}
    \]

    \begin{exampleblock}{\zany{} An assuming corollary}
        The proposition implies that
        \[
            \frac{m!}{n!(m-n)!}
        \] 
        is an integer for any integers $m \ge n \ge 0$.

        This is not easy to prove without proposition 2.9.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Binomial Coefficients}

    Another way to write $C(m,n)$ is
    \[
        \binom{m}{n},
    \]
    which reads ``\(m\) choose \(n\)''.

    This number is called a \alert{binomial coefficient}. 

    We will see where the name comes soon!
\end{frame}

\begin{frame}{Proposition 2.10 --- A combinatorial identity}
    For all integers $m, n$ with \(0 \le n \le m\)
    \[
        \binom{m}{n}=\binom{m}{m-n}.
    \]

    \begin{block}{\only<1>{Proof with Proposition 2.9}\only<2>{Proof with a combinatorial argument}}
        \vspace{11em}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Another Identity}
    
    How to show that for 
    for all integers $m, n$ with 
    \(0 < n < m\)
    \begin{equation*}
        n \binom{m}{n}= m \binom{m - 1}{n-1}
    \end{equation*}
    using a combinatorial argument?
\end{frame}

\begin{frame}
    \frametitle{A Third Identity}
    
    How to show that for 
    for all integers $m, n$ with 
    \(0 < n < m\)
    \begin{equation*}
        (m-n) \binom{m}{n}= m \binom{m - 1}{n}.
    \end{equation*}
    using a combinatorial argument?
\end{frame}

\begin{frame}
    \frametitle{A fourth identity}

    How to show that for 
    all integers $m, n$ with
    \(0 < n < m\)
    \begin{equation*}
        \binom{m}{n}=\binom{m-1}{n-1} + \binom{m-1}{n}.
    \end{equation*}
    using a combinatorial argument?
\end{frame}

\begin{frame}
    \frametitle{Pascal's triangle}

    A simple way to compute binomial coefficients is 
    using \href{https://en.wikipedia.org/wiki/Pascal\%27s\_triangle}{Pascal's
    triangle}.

    \begin{center}
        \begin{tabular}{ccccccccc}
            & & & & $\binom{0}{0}$ & & & & \\
            & & & $\binom{1}{0}$ & & $\binom{1}{1}$ & & & \\
            & & $\binom{2}{0}$ & & $\binom{2}{1}$ & & $\binom{2}{2}$ & & \\
            & $\binom{3}{0}$ & & $\binom{3}{1}$ & & $\binom{3}{2}$ & & $\binom{3}{3}$ & \\
            $\binom{4}{0}$ & & $\binom{4}{1}$ & & $\binom{4}{2}$ & & $\binom{4}{3}$ & & $\binom{4}{4}$ \\
        \end{tabular}
    \end{center}
\end{frame}

%\begin{frame}
%    \frametitle{Election --- revisited}
%
%    \begin{columns}
%        \column{0.5\textwidth}
%
%        A group of 40 students holds elections to select a parliament of three members. 
%
%        Assuming that there is \alert{no} difference among the roles of each
%        member of parliament, how many different outcomes are possible?
%
%        \column{0.5\textwidth}
%
%        \begin{figure}
%            \centering
%            \includegraphics[width=0.5\textwidth]{vote.jpg}
%        \end{figure}
%    \end{columns}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item[\emoji{pencil}] Section 2.9: 1, 3, 5, 7, 9, 11, 13, 21.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
