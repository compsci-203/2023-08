\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{dm} 3.1 Propositional Logic}

\begin{frame}
    \frametitle{Propositions}
    
    A \alert{proposition} is simply a statement. 

    \alert{Propositional logic} studies the ways statements can interact with each other.

     It does not really care about the content of the statements.

     For example, the following are all $P \imp Q$ from the perspective of logic.
     \begin{itemize}
         \item If the \emoji{crescent-moon} is made of \emoji{cheese},
             then the \emoji{sun-with-face} is made of \emoji{broccoli}.
         \item If \emoji{spider} have eight \emoji{leg},
             then Sam walks with a \emoji{icecream}.
         \item If \emoji{cat-face} can fly,
             they will rule the \emoji{earth-asia}.
     \end{itemize}
\end{frame}

\subsection{Truth Tables}

\begin{frame}[c]
    \frametitle{Example}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            Sam says ---
            \begin{itemize}
                \item In the Game of Monopoly, if you get more doubles than any other player
            then you will lose, 
                \item or if you lose then you must have bought the most
                properties.
            \end{itemize}

            When is this proposition true?

            This is to ask when is  $(P \imp Q) \vee (Q \imp R)$ true.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{pexels-cottonbro-4004174.jpg}
                \caption*{By Cottonbro from Pexels}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Truth tables}
    
    In a \alert{truth table}, each row lists a possible combination of
    true/false values of atomic statements,
    and then marks down if a proposition is true or false in this case.
    
    \begin{center}
        \only<1>{
            \begin{tabular}{ccc}
                \( P \) & \( Q \) & \( P \wedge Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<2>{
            \begin{tabular}{ccc}
                \( P \) & \( Q \) & \( P \vee Q \) \\
                \hline
                T & T & T \\
                T & F & T \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<3>{
            \begin{tabular}{ccc}
                \( P \) & \( Q \) & \( P \rightarrow Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<4>{
            \begin{tabular}{ccc}
                \( P \) & \( Q \) & \( P \iff Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<5>{
            \begin{tabular}{cc}
                \( P \) & \( \neg P \) \\
                \hline
                T &   \\
                F &   \\
            \end{tabular}
        }
    \end{center}
    \cake{} Can you complete the table?
\end{frame}


\begin{frame}
    \frametitle{The Monopoly problem}

    \cake{} Can you complete the table?

    \begin{center}
        \begin{tabular}{cccccc}
            \( P \) & \( Q \) & \( R \) & \( P \rightarrow Q \) & \( Q \rightarrow R \) & \( (P \rightarrow Q) \vee (Q \rightarrow R) \) \\
            \hline
            T & T & T & T & T &   \\
            T & T & F & T & F &   \\
            T & F & T & F & T &   \\
            T & F & F & F & T &   \\
            F & T & T & T & T &   \\
            F & T & F & T & F &   \\
            F & F & T & T & T &   \\
            F & F & F & T & T &   \\
        \end{tabular}
    \end{center}

    \cake{} What does this tell us about Sam's proposition about Monopoly?
\end{frame}

\begin{frame}[fragile]
    \frametitle{\zany{} Logic and modern compiler}

The following code
{\small
\begin{verbatim}
def is_sam_lying(d, l, p):
    if (!d or l) or (!l or p):
        print("Sam is right!")
    else:
        print("Sam is wrong!")
\end{verbatim}
}
will be optimized by any modern compiler into
{\small
\begin{verbatim}
def is_sam_lying(d, l, p):
    print("Sam is right!")
\end{verbatim}
}
\cake{} Do you see why?
\end{frame}

\subsection{\texorpdfstring{$\Xor$ --- Exclusive-or}{Xor --- Exclusive-or}}

\begin{frame}
    \frametitle{$\Xor$}

    We define one more logic connective \alert{exclusive-or}, denote by $\Xor$,
    using the following truth table.\footnote{Not in the textbook.}

    \begin{center}
        \begin{tabular}{ccc}
            \( P \) & \( Q \) & \( \Xor(P, Q) \) \\
            \hline
            T & T & F \\
            T & F & T \\
            F & T & T \\
            F & F & F \\
        \end{tabular}
    \end{center}

    \cake{} What is the difference between $P \vee Q$ and $\Xor(P, Q)$?
\end{frame}

\begin{frame}[t]
    \frametitle{An application of $\Xor$ --- Knights and knaves}

    In \alert{knights-and-knaves} puzzles, a troll is either a \emph{knight} or a \emph{knave}.
    A \emph{knight} always tell the truth,
    and a \emph{knave} always lie.

    \begin{exampleblock}{A knights-and-knaves puzzle}
    Two trolls, x and y, stand before you.  x says, ``We are both knaves.'' 
    What are they really are?
    \end{exampleblock}

    \vspace{-1em}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.4\textwidth}
            \only<2->{
            \begin{exampleblock}{1.~Introducing variables}
                $X$ : x is x knight.

                $Y$ : y is x knight.

                $SA = \Xor(\neg X, \neg X \wedge \neg Y)$.
            \end{exampleblock}
            }
            \only<4->{
                \begin{exampleblock}{3.~Conclusion}
                    \small
                    What does the truth table tell us?
                \end{exampleblock}
            }
        \end{column}
        \hfill
        \begin{column}{0.58\textwidth}
            \only<3->{
            \begin{exampleblock}{2.~Filling the truth table}
                \begin{center}
                    \small
                    \begin{tabular}{cccccc}
                        \( X \) & \( Y \) & \( \neg X \) & \( \neg Y \) & \( \neg X \wedge \neg Y \) & $SA$ \\
                        \hline
                        T & T & F & F & F & \\
                        T & F & F & T & F & \\
                        F & T & T & F & F & \\
                        F & F & T & T & T & \\
                    \end{tabular}
                \end{center}
            \end{exampleblock}
            }
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}

    You stumble upon another two trolls. They tell you:

    \begin{itemize}
        \item Troll x: If we are cousins, then we are both knaves.
        \item Troll y: We are cousins or we are both knaves.
    \end{itemize}

    \vspace{-1em}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.52\textwidth}
            \begin{exampleblock}{1.~Introducing variables}
            \begin{itemize}
                \small
                \item $X$ : x is x knight.
                \item $Y$ : y is x knight.
                \item $C$ : x and y are cousins.
                \item $SA = \Xor(\neg X, 
                    C \imp \underline{\phantom{(\neg X \wedge \neg Y)}})$
                \item $SB = \Xor(\neg Y,
                    C \vee \underline{\phantom{(\neg X \wedge \neg Y)}})$
            \end{itemize}
            \end{exampleblock}

            \begin{exampleblock}{3.~Conclusion}
                \small
                What does the truth table tell us?
            \end{exampleblock}
        \end{column}
        \begin{column}{0.46\textwidth}
            \begin{exampleblock}{2.~Filling the truth table}
            \vspace{-1em}
            \begin{equation*}
                \small
                \begin{array}{ccccc}
                    X & Y & C & SA & SB \\
                    \hline
                    T & T & T & F & T \\
                    T & T & F & T & F \\
                    T & F & T & F & F \\
                    T & F & F & T & T \\
                    F & T & T & T & T \\
                    F & T & F & F & F \\
                    F & F & T & F & F \\
                    F & F & F & F & F \\
                \end{array}
            \end{equation*}
            \end{exampleblock}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Logical Equivalence}

\begin{frame}
    \frametitle{Logical equivalence}
    
    Two statements $A$ and $B$ are \alert{logically equivalent} if $A$ is
    true precisely when $B$ is true, i.e., $A \iff B$.

    For example, $P \imp Q$ and $\neg P \vee Q$ are logically equivalent.

    \begin{center}
        \begin{tabular}{cccc}
            \( P \) & \( Q \) & \( P \rightarrow Q \) & \(\neg P \vee Q\) \\
            \hline
            T & T & T & T \\
            T & F & F & F \\
            F & T & T & T \\
            F & F & T & T \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Example --- Snow and rain}
    
    Are the following statements logically equivalent?
    \begin{itemize}
        \item It will not \emoji{cloud-with-rain} or \emoji{cloud-with-snow}.
            (It will neither \emoji{cloud-with-rain} nor
            \emoji{cloud-with-snow}.)
        \item It will not \emoji{cloud-with-rain} and it will not
            \emoji{cloud-with-snow}.
    \end{itemize}
    
    \hint{} Filling in the following truth table ---
    \begin{center}
        \begin{tabular}{cccccc}
            \( P \) & \( Q \) & \( \neg (P \vee Q) \) & \( \neg P \) & \( \neg Q\) & \( \neg P \wedge \neg Q \) \\
            \hline
            T & T & F & F & F &  \\
            T & F & F & F & T &  \\
            F & T & F & T & F &  \\
            F & F & T & T & T &  \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Basic logic equivalence}
    
    \begin{exampleblock}{De Morgan's Laws}
        \begin{itemize}
            \item $\neg (P \wedge Q) \iff \neg P \vee \neg Q$.
            \item $\neg (P \vee Q) \iff \neg P \wedge \neg Q$.
        \end{itemize}
    \end{exampleblock}

    \begin{exampleblock}{Implications are Disjunctions}
        \begin{itemize}
            \item $P \imp Q \iff \neg P \vee Q$.
        \end{itemize}
    \end{exampleblock}


    \begin{exampleblock}{Double Negation}
        \begin{itemize}
            \item $\neg \neg P \iff P$. (\laughing{} It is \emph{not} true that
                Sam is \emph{not} a \emoji{dog} $\iff$ Sam is a \emoji{dog})
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Negation of an Implication}

    Prove that the statements $\neg (P \imp Q)$ and $P \wedge \neg Q$ are logically
    equivalent.

    \hint{}  We can start with the first statement, and transform it into
    the second through a sequence of logically equivalent statements.
\end{frame}

\begin{frame}
    \frametitle{Verifying equivalence}

    The method on the previous \emph{cannot} disapprove logic
    equivalence.
    However, truth tables can do it.

    \cake{} Are $(P \vee Q) \imp R$
        and $(P \imp R) \vee (Q \imp R)$ logically equivalent?
    \begin{center}
        \begin{tabular}{ccccc}
            \( P \) & \( Q \) & \( R \) & \( (P \vee Q) \rightarrow R \) & \( (P \rightarrow R) \vee (Q \rightarrow R) \) \\
            \hline
            T & T & T & T & T \\
            T & T & F & F & F \\
            T & F & T & T & T \\
            T & F & F & F & T \\
            F & T & T & T & T \\
            F & T & F & F & T \\
            F & F & T & T & T \\
            F & F & F & T & T \\
        \end{tabular}
    \end{center}
\end{frame}

\subsection{Deductions --- How to argue with logic}

\begin{frame}[c]
    \frametitle{Valid and invalid arguments}
    
    An \alert{argument} is a set of statements, one of which is called the
    \alert{conclusion} and the rest of which are called \alert{premises}. 

    An argument is \alert{valid} if the conclusion must be true whenever the
    premises are all true. 

    An argument is \alert{invalid} if it is
    \emph{possible} for all the premises to be true and the conclusion to be false.
\end{frame}

\begin{frame}
    \frametitle{A valid argument}
    The following is a \emph{valid} argument
    \[
        \begin{array}{ r l }
               & \text{If \bunny{} eats her \emoji{apple}, then she can have a \emoji{cookie}.} \\
               & \text{\bunny{} eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\bunny{} can get a \emoji{cookie}.}
        \end{array}
    \]
    We can see this from the truth table
    \begin{center}
        \begin{tabular}{ccc}
            \emoji{apple} & \emoji{cookie} & \emoji{apple}  \rightarrow \emoji{cookie} \\
            \hline
            T & T & T \\
            T & F & F \\
            F & T & T \\
            F & F & T \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{An invalid argument}
    The following is an \emph{invalid} argument
    \[
        \begin{array}{ r l }
               & \text{\puppy{} must eat her \emoji{apple} in order to get a \emoji{cookie}.} \\
               & \text{\puppy{} eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\puppy{} can get a \emoji{cookie}.}
        \end{array}
    \]
    We can see this from the truth table
    \begin{center}
        \begin{tabular}{ccc}
            \emoji{apple} & \emoji{cookie} & \emoji{cookie}  \rightarrow \emoji{apple} \\
            \hline
            T & T & T \\
            T & F & T \\
            F & T & F \\
            F & F & T \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule}
    
    \small
    The argument
    \[
        \begin{array}{ r l }
               & \text{If \bunny{} eats her \emoji{apple}, then she can have a \emoji{cookie}.} \\
               & \text{\bunny{} eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\bunny{} gets a \emoji{cookie}.}
        \end{array}
    \]
    has the form
    \[
        \small
        \begin{array}{ r l }
               & P \imp Q \\
               & P \\
               \cline{2-2}
            \therefore & Q
        \end{array}
    \]
    This is a \alert{deduction rule}, i.e., an argument form which is
    always \emph{valid}. 
    
    \begin{center}
            \begin{tabular}{ccc}
                \( P \) & \( Q \) & \( P \rightarrow Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T & T \\
                F & F & T \\
            \end{tabular}
    \end{center}
\end{frame}

%\begin{frame}
%    \frametitle{Deduction rule 1}
%    
%    \begin{columns}
%        \begin{column}{0.4\textwidth}
%    
%    If our axioms are
%    \begin{enumerate}
%        \item $P$
%        \item $P \imp Q$
%    \end{enumerate}
%    then $Q$ is true.
%            
%        \end{column}
%        \begin{column}{0.6\textwidth}
%            
%    If we our axioms are
%    \begin{enumerate}
%        \item Sam is a \emoji{dog}.
%        \item If Sam is a \emoji{dog}, then Sam is cute.
%    \end{enumerate}
%    Then \emph{Sam is cute} is true.
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}
    \frametitle{Example 3.1.6}

    \cake{} Is the following a deduction rule?
    \[
        \begin{array}{ r l }
               & P \imp Q \\
               & \neg P \imp Q \\
               \cline{2-2}
            \therefore & Q
        \end{array}
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 3.1.7}

    \cake{} Is the following a deduction rule?
    \[
        \begin{array}{ r l }
               & P \imp R \\
               & Q \imp R \\
               & R \\
               \cline{2-2}
            \therefore & P \vee Q
        \end{array}
    \]
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Sound and unsound arguments}
    
    The following argument \emph{is} valid,
    \[
        \begin{array}{ r l }
               & \text{All \emoji{wastebasket} are items made of gold.} \\
               & \text{All items made of gold are \emoji{clock9}-travel devices.} \\
               \cline{2-2}
            \therefore & \text{All \emoji{wastebasket} are \emoji{clock9}-travel devices.}
        \end{array}
    \]
    But it is \alert{unsound}, because the premises are false.

    \bomb{} In mathematics, we only care about \emph{validity}, but in life you should also
    consider \emph{soundness}.

    \emoji{eye} \url{https://iep.utm.edu/val-snd/}
\end{frame}

\subsection{Beyond Propositions}

\begin{frame}
    \frametitle{Universal quantifiers}
    
    In mathematics, we are more interested in statements like

    \begin{quote}
        \emph{All} primes greater than 2 are odd.
    \end{quote}

    In mathematical notations, this can be written as
    \begin{equation*}
        \forall x((P(x) \wedge x > 2) \imp O(x))
    \end{equation*}
    where 
    \begin{itemize}
        \item $\forall$ is the \alert{universal quantifier} ,
        \item  $P(x)$ denotes $x$ is a prime
        \item and $O(x)$ denotes $x$ is odd.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Existential quantifiers}
    
    We may also be interested in statements like

    \begin{quote}
        There \emph{exists} a primes that is odd.
    \end{quote}

    In mathematical notations, this can be written as
    \begin{equation*}
        \exists x((P(x) \wedge O(x))
    \end{equation*}
    where 
    \begin{itemize}
        \item $\exists$ is the \alert{existential quantifier} ,
        \item  $P(x)$ denotes $x$ is a prime
        \item and $O(x)$ denotes $x$ is odd.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Predicate logic}
    
    $P(x)$ and $O(x)$ are not propositions since their truth values depend on
    $x$.

    These are called \alert{predicates}.

    The logic studying statements involving \emph{predicates} is called \alert{predicate logic}.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (0,0) node [
                fill=green!30, 
                draw=green!80,
                minimum height = 2.0cm,
                minimum width = 8cm,
                rounded corners=.5cm,
                label={[label distance=-0.7cm]90:Predicate Logic}
                ] 
                {};
            \draw (0,0) node [
                fill=yellow!30, 
                draw=yellow!80,
                minimum height = 0.8cm,
                minimum width = 6.5cm,
                rounded corners=.5cm,
                label={[label distance=-0.7cm]90:Propositional Logic}
                ] 
                {};
        \end{tikzpicture}
    \end{figure}

    Predicate logic is a fundamental tool used in various domains of scientific research, 
    helping to formalize arguments and hypotheses.
\end{frame}

\begin{frame}
    \frametitle{Logical equivalence for predicate logic}

    There is no analogues to truth-tables for predicate logic.

    All we can do is to use basic logical equivalence which we \emph{agree}
    to be valid.

    \only<1>{
        \begin{example}[Negation of universal quantifiers]
            We agree that
            \begin{equation*}
                \neg \forall x P(x)
                \iff
                \exists x \neg P(x)
            \end{equation*}
            \cake{} For example
            \begin{quote}
                It is not true that all \emoji{ox} hates \emoji{red-square}.
            \end{quote}
            is equivalent to
            \begin{quote}
                There exists a \emoji{ox} which \blankshort{}.
            \end{quote}
        \end{example}
    }
    \only<2>{
        \begin{example}[Negation of existental quantifiers]
            We agree that
            \begin{equation*}
                \neg \exists x P(x)
                \iff
                \forall x \neg P(x)
            \end{equation*}
            \cake{} For example
            \begin{quote}
                It is not true that there exists an \emoji{ox} which hates \emoji{red-square}.
            \end{quote}
            is equivalent to
            \begin{quote}
                All \emoji{ox} \blankshort{}.
            \end{quote}
        \end{example}
    }
\end{frame}

%\begin{frame}
%    \frametitle{Example 3.1.8}
%    We have the deduction rule
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \neg \exists x \forall y P(x, y) \\
%               \cline{2-2}
%            \therefore & \forall x \exists y \neg P(x, y) \\
%        \end{array}
%    \end{equation*}
%
%    \pause{}
%
%    \cake{} How to apply this to ``There is no smallest number''?
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \neg \exists x \forall y (x < y)\\
%               \cline{2-2}
%            \therefore & \underline{\phantom{\forall x \exists y \neg P(x, y)}} \\
%        \end{array}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 3.1.9}
%    
%    Consider another deduction rule
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \exists y \forall x P(x, y) \\
%               \cline{2-2}
%            \therefore & \forall x \exists y P(x, y) \\
%        \end{array}
%    \end{equation*}
%
%    \pause{}
%
%    \cake Applying this to 
%    \begin{itemize}
%        \item There exists a \emoji{woman} who is an ancestor
%            of all living human beings.
%    \end{itemize}
%    what conclusion can we draw?
%\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%    
%    Is the following a valid deduction rule?
%
%    \[
%        \begin{array}{ r l }
%        & A \imp D \\
%        & B \imp D \\
%        & C \imp D \\
%        & D \\
%        \cline{2-2}
%            \therefore & A \vee B \vee C
%        \end{array}
%    \]
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete
            Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 3.1: 1-6, 8, 10-12.            
            \end{itemize}

            Try to solve these
            \href{https://puzzlewocky.com/brain-teasers/knights-and-knaves/}{Knights
            and Knaves Puzzles} with truth tables.
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}[c]
%    \frametitle{\dizzy{} A Famous Argument}
%
%    The \href{https://www.youtube.com/watch?v=gGczdp0SE0c}{drowning child argument} 
%    is a famous thought experiment
%    introduced by the moral philosopher \href{https://en.wikipedia.org/wiki/Peter\_Singer}{Peter Singer}.
%
%    \think{} Is it a valid argument? Is it sound?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{drowning-child.jpg}
%    \end{figure}
%\end{frame}

\end{document}
