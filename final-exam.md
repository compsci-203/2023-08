---
title: Final Exam for COMPSCI 203
author: Xing Shi Cai
date: 2021-08 session
geometry:
- margin=1.5in
...

# Date, Time and Format

The exam will be carried out through [GradeScope](https://www.gradescope.com/) during 
Tue 10/19/2021 12:00 PM-3:00 PM.

There will be six problems with equal weights.

Please keep an eye on our [Microsoft Teams channel](https://teams.microsoft.com/l/channel/19%3arzW1CJtgHBKamvQb_0uXG9SZZDqmphSTFOtx1-5Qads1%40thread.tacv2/General?groupId=85f158e0-ce88-41a9-a711-493bdbf9f9a2&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c).
Corrections or clarifications of the problems will be posted there.
You can also send me a message or call me on MS teams to ask questions.

# What is allowed in an exam?

1. Consulting our textbooks, slides, and your own notes
2. Using the following websites
    1. [OEIS](https://oeis.org/)
    2. [WolframAlpha](https://wolframalpha.com/)
    3. [MathWorld](https://mathworld.wolfram.com/)
    4. [Wikipedia](https://en.wikipedia.org/)
3. Using a computer for calculations
4. Using pencils and papers (of course!)

# What is not allowed?

1. Discussing the exam with **anyone** (human or non-human, online or offline)
2. Search the answer on the Internet or in any books other than the two textbooks

# Scope

The exam will cover everything we have talked about in class.

# LaTeX

You are *recommended* to use the GradeScope's [LaTeX features](https://help.gradescope.com/article/3vm6obxcyf-latex-guide) when you write mathematical
equations.
But that is *not* mandatory.
