(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     18958,        595]
NotebookOptionsPosition[     16008,        532]
NotebookOutlinePosition[     16397,        548]
CellTagsIndexPosition[     16354,        545]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Lower bound of Ramsey number", "Section",
 CellChangeTimes->{{3.904852976062916*^9, 
  3.9048529811981707`*^9}},ExpressionUUID->"fbdf2bea-0808-4486-8f22-\
37ff50e68cfa"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ie", "[", "1", "]"}], "=", 
  RowBox[{
   RowBox[{
    RowBox[{"2", 
     RowBox[{"Binomial", "[", 
      RowBox[{"t", ",", "n"}], "]"}], 
     SuperscriptBox["2", 
      RowBox[{"n", 
       RowBox[{"(", 
        RowBox[{"t", "-", "n"}], ")"}]}]], 
     SuperscriptBox["2", 
      RowBox[{"Binomial", "[", 
       RowBox[{
        RowBox[{"t", "-", "n"}], ",", "2"}], "]"}]]}], "<", 
    SuperscriptBox["2", 
     RowBox[{"Binomial", "[", 
      RowBox[{"t", ",", "2"}], "]"}]]}], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.9048529838898087`*^9, 3.904852986991536*^9}, {
  3.904853022863742*^9, 3.9048530732790203`*^9}, {3.9048531366578617`*^9, 
  3.904853137730976*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"638ddaaa-8b64-49f7-8232-0b37ec29550d"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"2", "+", "n", "+", 
       SuperscriptBox["n", "2"], "+", "t", "+", 
       SuperscriptBox["t", "2"]}], ")"}]}]], " ", 
   RowBox[{"Binomial", "[", 
    RowBox[{"t", ",", "n"}], "]"}]}], "<", 
  SuperscriptBox["2", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       SuperscriptBox["n", "2"]}], "+", "t", "+", 
      SuperscriptBox["t", "2"]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{{3.9048530590643806`*^9, 3.9048530749888153`*^9}, 
   3.904853138204013*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"0848d2b7-9b09-4d53-bcc5-1b356f2bef54"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ie", "[", "2", "]"}], "=", 
  RowBox[{
   RowBox[{"ie", "[", "1", "]"}], "/.", 
   RowBox[{
    RowBox[{"Binomial", "[", 
     RowBox[{"t", ",", "n"}], "]"}], ":>", 
    FractionBox[
     SuperscriptBox["t", "n"], 
     RowBox[{"n", "!"}]]}]}]}]], "Input",
 CellChangeTimes->{{3.904853099008668*^9, 3.904853124975759*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"a3a272e0-8faa-4a58-933f-bfbd693d8caf"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    SuperscriptBox["2", 
     RowBox[{
      FractionBox["1", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"2", "+", "n", "+", 
        SuperscriptBox["n", "2"], "+", "t", "+", 
        SuperscriptBox["t", "2"]}], ")"}]}]], " ", 
    SuperscriptBox["t", "n"]}], 
   RowBox[{"n", "!"}]], "<", 
  SuperscriptBox["2", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       SuperscriptBox["n", "2"]}], "+", "t", "+", 
      SuperscriptBox["t", "2"]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{{3.904853126212961*^9, 3.904853141937258*^9}},
 CellLabel->"Out[5]=",ExpressionUUID->"8c6d1ded-f603-4e2e-b75b-ce16857b222f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ie", "[", "3", "]"}], "=", 
  RowBox[{
   RowBox[{"ie", "[", "2", "]"}], "/.", "\[VeryThinSpace]", 
   RowBox[{
    RowBox[{"n", "!"}], "\[Rule]", 
    RowBox[{
     SqrtBox[
      RowBox[{"2", " ", "\[Pi]", " ", "n"}]], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox["n", "\[ExponentialE]"], ")"}], "n"]}]}]}]}]], "Input",
 CellChangeTimes->{{3.90485314849751*^9, 3.9048531974635973`*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"e926af33-bc5b-4c6c-a64d-86a9353f6a77"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    SuperscriptBox["2", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], "+", 
      RowBox[{
       FractionBox["1", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"2", "+", "n", "+", 
         SuperscriptBox["n", "2"], "+", "t", "+", 
         SuperscriptBox["t", "2"]}], ")"}]}]}]], " ", 
    SuperscriptBox["\[ExponentialE]", "n"], " ", 
    SuperscriptBox["n", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], "-", "n"}]], " ", 
    SuperscriptBox["t", "n"]}], 
   SqrtBox["\[Pi]"]], "<", 
  SuperscriptBox["2", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       SuperscriptBox["n", "2"]}], "+", "t", "+", 
      SuperscriptBox["t", "2"]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.904853198853251*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"6a042432-aa1b-47ae-85cd-dc188df8a35d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ie", "[", "4", "]"}], "=", 
  RowBox[{
   RowBox[{"DivideSides", "[", 
    RowBox[{
     RowBox[{"ie", "[", "3", "]"}], ",", 
     RowBox[{
      RowBox[{"ie", "[", "3", "]"}], "[", 
      RowBox[{"[", "2", "]"}], "]"}]}], "]"}], "//", 
   RowBox[{
    RowBox[{"Map", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Simplify", "[", 
        RowBox[{"#", ",", 
         RowBox[{
          RowBox[{"n", ">", "0"}], "&&", 
          RowBox[{"t", ">", "0"}]}]}], "]"}], "&"}], ",", "#", ",", "1"}], 
     "]"}], "&"}]}]}]], "Input",
 CellChangeTimes->{{3.904853203665077*^9, 3.904853285936598*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"acd19c37-c22c-4d5c-a7d2-3691b3999dff"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["2", 
    FractionBox[
     RowBox[{"1", "+", "n"}], "2"]], " ", 
   SuperscriptBox["\[ExponentialE]", "n"], " ", 
   SuperscriptBox["t", "n"]}], "<", 
  RowBox[{
   SuperscriptBox["2", 
    FractionBox[
     SuperscriptBox["n", "2"], "2"]], " ", 
   SuperscriptBox["n", 
    RowBox[{
     FractionBox["1", "2"], "+", "n"}]], " ", 
   SqrtBox["\[Pi]"]}]}]], "Output",
 CellChangeTimes->{{3.904853221355036*^9, 3.904853286522441*^9}},
 CellLabel->"Out[11]=",ExpressionUUID->"8be0b48b-d92b-42e8-84e7-8a6524e974d7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ie", "[", "5", "]"}], "=", 
  RowBox[{
   RowBox[{"ApplySides", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["#", 
       RowBox[{"1", "/", "n"}]], "&"}], ",", 
     RowBox[{"ie", "[", "4", "]"}]}], "]"}], "//", "PowerExpand"}]}]], "Input",\

 CellChangeTimes->{{3.904853335713799*^9, 3.904853433715963*^9}, {
  3.904853480836706*^9, 3.904853486097102*^9}, {3.904853550513775*^9, 
  3.9048535521311293`*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"2bf10fe1-79d8-4caf-b9bf-8097dce9e062"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["2", 
    FractionBox[
     RowBox[{"1", "+", "n"}], 
     RowBox[{"2", " ", "n"}]]], " ", "\[ExponentialE]", " ", "t"}], "<", 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"n", "/", "2"}]], " ", 
   SuperscriptBox["n", 
    FractionBox[
     RowBox[{
      FractionBox["1", "2"], "+", "n"}], "n"]], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{
     FractionBox["1", "2"], "/", "n"}]]}]}]], "Output",
 CellChangeTimes->{{3.904853350702046*^9, 3.904853434025257*^9}, 
   3.904853487456448*^9, 3.904853552478601*^9},
 CellLabel->"Out[22]=",ExpressionUUID->"433c1b35-28c9-4ed4-b187-1c006d52174a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Reduce", "[", 
    RowBox[{
     RowBox[{"ie", "[", "5", "]"}], ",", "t", ",", "Reals"}], "]"}], "//", 
   RowBox[{
    RowBox[{"FullSimplify", "[", 
     RowBox[{"#", ",", 
      RowBox[{"t", ">", "0"}]}], "]"}], "&"}]}], "//", 
  RowBox[{
   RowBox[{"PowerExpand", "[", 
    RowBox[{"#", ",", 
     RowBox[{"n", ">", "1000"}]}], "]"}], "&"}]}]], "Input",
 CellChangeTimes->{{3.904853491276367*^9, 3.904853495904784*^9}, {
  3.90485356160187*^9, 3.904853608338632*^9}, {3.9048536423863773`*^9, 
  3.904853723217865*^9}, {3.9048537698423433`*^9, 3.904853798177443*^9}},
 CellLabel->"In[41]:=",ExpressionUUID->"00e18963-eb3d-47c2-8a34-18918cb8fe03"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"t", ">", 
     FractionBox[
      RowBox[{
       SuperscriptBox["2", 
        FractionBox[
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"-", "1"}], "+", "n"}], ")"}], " ", "n"}]}], 
         RowBox[{"2", " ", "n"}]]], " ", 
       SuperscriptBox["n", 
        RowBox[{"1", "+", 
         FractionBox["1", 
          RowBox[{"2", " ", "n"}]]}]], " ", 
       SuperscriptBox["\[Pi]", 
        RowBox[{
         FractionBox["1", "2"], "/", "n"}]]}], "\[ExponentialE]"]}], "&&", 
    RowBox[{
     SuperscriptBox["2", 
      RowBox[{
       FractionBox["1", "2"], "/", "n"}]], "<", "0"}]}], ")"}], "||", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     SuperscriptBox["2", 
      RowBox[{
       FractionBox["1", "2"], "/", "n"}]], ">", "0"}], "&&", 
    RowBox[{"t", "<", 
     FractionBox[
      RowBox[{
       SuperscriptBox["2", 
        FractionBox[
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"-", "1"}], "+", "n"}], ")"}], " ", "n"}]}], 
         RowBox[{"2", " ", "n"}]]], " ", 
       SuperscriptBox["n", 
        RowBox[{"1", "+", 
         FractionBox["1", 
          RowBox[{"2", " ", "n"}]]}]], " ", 
       SuperscriptBox["\[Pi]", 
        RowBox[{
         FractionBox["1", "2"], "/", "n"}]]}], "\[ExponentialE]"]}]}], 
   ")"}]}]], "Output",
 CellChangeTimes->{
  3.904853515961422*^9, {3.90485355786755*^9, 3.90485360875445*^9}, {
   3.904853654468938*^9, 3.904853723615439*^9}, {3.9048537717789*^9, 
   3.904853798705081*^9}},
 CellLabel->"Out[41]=",ExpressionUUID->"63ee6653-aa28-4c7a-8cf3-a438447f7a26"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["The Tournament", "Section",
 CellChangeTimes->{{3.9044503262483*^9, 3.90445033735198*^9}, {
  3.904453455358782*^9, 
  3.904453464957624*^9}},ExpressionUUID->"f4da5ac8-63f1-4603-a52b-\
70b93908f1b7"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "1", "]"}], "=", 
  RowBox[{
   RowBox[{"Binomial", "[", 
    RowBox[{"n", ",", "k"}], "]"}], "*", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "-", 
      SuperscriptBox["2", 
       RowBox[{"-", "k"}]]}], ")"}], 
    RowBox[{"n", "-", "k"}]]}]}]], "Input",
 CellChangeTimes->{{3.904450145200884*^9, 3.904450175754634*^9}, {
  3.90445353012754*^9, 3.904453531630587*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"d276a837-5d27-45b3-a0c0-ea25fbf90ec9"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"1", "-", 
     SuperscriptBox["2", 
      RowBox[{"-", "k"}]]}], ")"}], 
   RowBox[{
    RowBox[{"-", "k"}], "+", "n"}]], " ", 
  RowBox[{"Binomial", "[", 
   RowBox[{"n", ",", "k"}], "]"}]}]], "Output",
 CellChangeTimes->{3.904453532222271*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"12a20d85-76a5-42a6-a1ed-46d6956ecad5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "2", "]"}], "=", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"\[ExponentialE]", " ", "n"}], "k"], ")"}], "k"], " ", 
   RowBox[{"Exp", "[", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"n", "-", "k"}], 
      SuperscriptBox["2", "k"]]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.90445347168235*^9, 3.904453536782847*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"213568a4-a7d5-4ba7-8d91-72480704c14f"],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"k", "-", 
    RowBox[{
     SuperscriptBox["2", 
      RowBox[{"-", "k"}]], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "k"}], "+", "n"}], ")"}]}]}]], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    FractionBox["n", "k"], ")"}], "k"]}]], "Output",
 CellChangeTimes->{3.904453537315535*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"71c7a22b-0164-4251-9462-3c65fcee5a5f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "3", "]"}], "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"ex", "[", "2", "]"}], "//", "Log"}], "//", 
      RowBox[{
       RowBox[{"FullSimplify", "[", 
        RowBox[{"#", ",", 
         RowBox[{
          RowBox[{"n", ">", "1000"}], "&&", 
          RowBox[{"k", ">", "1000"}], "&&", 
          RowBox[{"k", "\[Element]", 
           TemplateBox[{},
            "Integers"]}], "&&", 
          RowBox[{"n", "\[Element]", 
           TemplateBox[{},
            "Integers"]}]}]}], "]"}], "&"}]}], "//", "Expand"}], "//", 
    RowBox[{
     RowBox[{"#", "/.", 
      RowBox[{
       RowBox[{"Log", "[", 
        RowBox[{"n", "/", "k"}], "]"}], "->", 
       RowBox[{
        RowBox[{"Log", "[", "n", "]"}], "-", 
        RowBox[{"Log", "[", "k", "]"}]}]}]}], "&"}]}], "//", 
   "Expand"}]}]], "Input",
 CellChangeTimes->{{3.9044535260793877`*^9, 3.90445358604563*^9}, {
  3.9044542991771507`*^9, 3.9044543619767437`*^9}},
 CellLabel->"In[37]:=",ExpressionUUID->"4ce61ffe-9968-4814-a83d-3cf2affd2d5e"],

Cell[BoxData[
 RowBox[{"k", "+", 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"-", "k"}]], " ", "k"}], "-", 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"-", "k"}]], " ", "n"}], "-", 
  RowBox[{"k", " ", 
   RowBox[{"Log", "[", "k", "]"}]}], "+", 
  RowBox[{"k", " ", 
   RowBox[{"Log", "[", "n", "]"}]}]}]], "Output",
 CellChangeTimes->{{3.904453548591221*^9, 3.904453586553731*^9}, {
  3.9044543020450687`*^9, 3.904454362493641*^9}},
 CellLabel->"Out[37]=",ExpressionUUID->"409aafd0-4c7b-4b82-b4f7-e4cfa699bc6e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ex", "[", "4", "]"}], "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"ex", "[", "3", "]"}], "/.", 
       RowBox[{"n", "->", 
        RowBox[{
         SuperscriptBox["k", "2"], 
         RowBox[{"Log", "[", "2", "]"}], 
         SuperscriptBox["2", "k"]}]}]}], "/.", 
      RowBox[{"k", "->", 
       RowBox[{"Exp", "[", "m", "]"}]}]}], "//", 
     RowBox[{
      RowBox[{"FullSimplify", "[", 
       RowBox[{"#", ",", 
        RowBox[{"m", ">", "1000"}]}], "]"}], "&"}]}], "//", "Expand"}], "//", 
   
   RowBox[{
    RowBox[{"#", "/.", 
     RowBox[{"m", "->", 
      RowBox[{"Log", "[", "k", "]"}]}]}], "&"}]}]}]], "Input",
 CellChangeTimes->{{3.904453589822296*^9, 3.904453785276409*^9}, {
  3.904454163580208*^9, 3.9044542200403957`*^9}, {3.904454257418894*^9, 
  3.904454277913013*^9}},
 CellLabel->"In[38]:=",ExpressionUUID->"b01d2050-63fd-4903-82ae-d83cbf46287f"],

Cell[BoxData[
 RowBox[{"k", "+", 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"-", "k"}]], " ", "k"}], "+", 
  RowBox[{"k", " ", 
   RowBox[{"Log", "[", "k", "]"}]}], "+", 
  RowBox[{"k", " ", 
   RowBox[{"Log", "[", 
    RowBox[{"Log", "[", "2", "]"}], "]"}]}]}]], "Output",
 CellChangeTimes->{{3.904453616678132*^9, 3.904453650163373*^9}, {
   3.904453682612096*^9, 3.904453785825541*^9}, {3.904454166049906*^9, 
   3.904454222224606*^9}, 3.904454278528103*^9, 3.904454363757554*^9},
 CellLabel->"Out[38]=",ExpressionUUID->"9ccbc14d-edce-4bce-b58e-f0cb2a709463"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{"ex", "[", "4", "]"}], ",", 
   RowBox[{"k", "->", "\[Infinity]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.904453827534041*^9, 3.904453834259664*^9}},
 CellLabel->"In[30]:=",ExpressionUUID->"1b1d9434-f7e8-4df8-8559-fe204a40542c"],

Cell[BoxData[
 RowBox[{"-", "\[Infinity]"}]], "Output",
 CellChangeTimes->{3.904453834840185*^9, 3.904454197496695*^9, 
  3.904454234168027*^9},
 CellLabel->"Out[30]=",ExpressionUUID->"32abe8fa-828f-49e0-939c-425e1a1145d4"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1440, 810},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"3b16fc4d-691a-4fe9-a502-2e9f9d39590d"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 175, 3, 68, "Section",ExpressionUUID->"fbdf2bea-0808-4486-8f22-37ff50e68cfa"],
Cell[CellGroupData[{
Cell[780, 29, 796, 22, 33, "Input",ExpressionUUID->"638ddaaa-8b64-49f7-8232-0b37ec29550d"],
Cell[1579, 53, 739, 22, 41, "Output",ExpressionUUID->"0848d2b7-9b09-4d53-bcc5-1b356f2bef54"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2355, 80, 435, 12, 47, "Input",ExpressionUUID->"a3a272e0-8faa-4a58-933f-bfbd693d8caf"],
Cell[2793, 94, 722, 22, 59, "Output",ExpressionUUID->"8c6d1ded-f603-4e2e-b75b-ce16857b222f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3552, 121, 522, 14, 44, "Input",ExpressionUUID->"e926af33-bc5b-4c6c-a64d-86a9353f6a77"],
Cell[4077, 137, 936, 30, 62, "Output",ExpressionUUID->"6a042432-aa1b-47ae-85cd-dc188df8a35d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5050, 172, 711, 21, 29, "Input",ExpressionUUID->"acd19c37-c22c-4d5c-a7d2-3691b3999dff"],
Cell[5764, 195, 565, 17, 44, "Output",ExpressionUUID->"8be0b48b-d92b-42e8-84e7-8a6524e974d7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6366, 217, 531, 14, 32, "Input",ExpressionUUID->"2bf10fe1-79d8-4caf-b9bf-8097dce9e062"],
Cell[6900, 233, 650, 19, 50, "Output",ExpressionUUID->"433c1b35-28c9-4ed4-b187-1c006d52174a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7587, 257, 695, 17, 29, "Input",ExpressionUUID->"00e18963-eb3d-47c2-8a34-18918cb8fe03"],
Cell[8285, 276, 1775, 57, 90, "Output",ExpressionUUID->"63ee6653-aa28-4c7a-8cf3-a438447f7a26"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10109, 339, 205, 4, 68, "Section",ExpressionUUID->"f4da5ac8-63f1-4603-a52b-70b93908f1b7"],
Cell[CellGroupData[{
Cell[10339, 347, 506, 14, 33, "Input",ExpressionUUID->"d276a837-5d27-45b3-a0c0-ea25fbf90ec9"],
Cell[10848, 363, 395, 12, 37, "Output",ExpressionUUID->"12a20d85-76a5-42a6-a1ed-46d6956ecad5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11280, 380, 489, 14, 56, "Input",ExpressionUUID->"213568a4-a7d5-4ba7-8d91-72480704c14f"],
Cell[11772, 396, 453, 14, 47, "Output",ExpressionUUID->"71c7a22b-0164-4251-9462-3c65fcee5a5f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12262, 415, 1088, 32, 29, "Input",ExpressionUUID->"4ce61ffe-9968-4814-a83d-3cf2affd2d5e"],
Cell[13353, 449, 520, 14, 36, "Output",ExpressionUUID->"409aafd0-4c7b-4b82-b4f7-e4cfa699bc6e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13910, 468, 946, 28, 32, "Input",ExpressionUUID->"b01d2050-63fd-4903-82ae-d83cbf46287f"],
Cell[14859, 498, 567, 13, 36, "Output",ExpressionUUID->"9ccbc14d-edce-4bce-b58e-f0cb2a709463"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15463, 516, 291, 6, 29, "Input",ExpressionUUID->"1b1d9434-f7e8-4df8-8559-fe204a40542c"],
Cell[15757, 524, 223, 4, 33, "Output",ExpressionUUID->"32abe8fa-828f-49e0-939c-425e1a1145d4"]
}, Open  ]]
}, Open  ]]
}
]
*)

