import networkx as nx
import matplotlib.pyplot as plt

# Construct the tree
edges = [(0, 4), (4, 1), (1, 2), (2, 6), (2, 3), (5, 1)]
tree = nx.Graph(edges)

# Convert to Prufer code
sequence = nx.to_prufer_sequence(tree)
print(sequence)

# Define drawing options
options = {
    "font_size": 20,
    "node_size": 1500,
    "node_color": "orange",
    "edgecolors": "black",  # Add black borders
    "linewidths": 3,
    "width": 3,
}

# Generate layout and draw the tree
pos = nx.spring_layout(tree, seed=42)  # Added seed for reproducibility
nx.draw_networkx(tree, pos, **options)

# Save the figure
plt.savefig("../slides/images/graphs/prufer-code-example.pdf")
