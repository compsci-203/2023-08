import networkx as nx
import matplotlib.pyplot as plt

# Construct the tree from the Prufer code
code = [7, 5, 5, 3, 1]
code = [i-1 for i in code]
tree = nx.from_prufer_sequence(code)
print(tree)

# Define drawing options
options = {
    "font_size": 20,
    "node_size": 1500,
    "node_color": "orange",
    "edgecolors": "black",  # Add black borders
    "linewidths": 3,
    "width": 3,
}

# Generate layout and draw the tree
pos = nx.spring_layout(tree, seed=42)  # Added seed for reproducibility

# Create a label mapping to add 1 to each label
label_mapping = {node: node + 1 for node in tree.nodes()}

# Draw the tree with updated labels
nx.draw_networkx(tree, pos, labels=label_mapping, **options)

# Save the figure
plt.savefig("../slides/images/graphs/prufer-code-to-tree-example.pdf")
